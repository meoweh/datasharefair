const {deleteFairAuthorization} = require("../sparql-wrappers/authorization");
const {deleteFairDataset} = require("../sparql-wrappers/dataset");
const {getFairCategories} = require("../sparql-wrappers/dataset");
const {getFairDatasets} = require("../sparql-wrappers/dataset");
const {getFairDatasetsForUser} = require("../sparql-wrappers/dataset");
const {getFairAuthorizationsWithId} = require("../sparql-wrappers/authorization");
const {getFairAuthorizationsForUserWriteControl} = require("../sparql-wrappers/authorization");
const {createFairDataset} = require("../sparql-wrappers/dataset");
const {makeAuthorizationsFair, convertDatasetFair} = require("../utils/makeDataFair");
const {makeDatasetFair} = require("../utils/makeDataFair");
const {createFairAuthorizations} = require("../sparql-wrappers/authorization");
const SPARQLCatalog = require('../sparql-wrappers/SPARQLCatalog');
const SPARQLAccessPolicy = require('../sparql-wrappers/SPARQLAccessPolicy');

$$.swarm.describe("dataset", {
    updateDat: async function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const sessionData = session.get();

        const accountData = transaction.lookup('global.Account', sessionData.company_id).get();

        const oldAccessRights = req.accessRights;
        const datasetData = convertDatasetFair(req);

        const authorizations = makeAuthorizationsFair(req, req.status);
        const accessPolicyData = SPARQLAccessPolicy.toSPARQL(datasetData.accessRights, authorizations);

        let toCommitDeletedAuth = [];
        for (const accessRight of oldAccessRights) {
            await deleteFairAuthorization(accessRight.authorization_id);
            const authorization = transaction.lookup('global.Authorization', accessRight.authorization_id);
            authorization.remove();
            toCommitDeletedAuth.push(authorization);
            await SPARQLAccessPolicy.delete(accessRight.policy_id);
        }

        await deleteFairDataset(datasetData.dataset_id);

        /*save new assets for audit and consensus purposes*/
        const dataset = transaction.lookup('global.Dataset', datasetData.dataset_id);
        dataset.create(datasetData);

        /*create authorizations*/
        let toCommitAuthorizations = [];
        authorizations.forEach((auth) => {
            const authorization = transaction.lookup('global.Authorization', auth.authorization_id);
            authorization.create(auth);
            toCommitAuthorizations.push(authorization);
        });

        /*create access policy */
        const accessPolicy = transaction.lookup('global.AccessPolicy', accessPolicyData.policy_id);
        accessPolicy.create(accessPolicy);

        try {
            toCommitDeletedAuth.forEach(authorization => transaction.add(authorization));
            toCommitAuthorizations.forEach(authorization => transaction.add(authorization));
            transaction.add(dataset);
            transaction.add(accessPolicy);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot create catalog!'});
            return;
        }

        try {
            /*add fair data dataset */
            await createFairDataset(datasetData);
            /*add fair data authorizations*/
            await createFairAuthorizations(authorizations);
            /*add fair data access policy */
            await SPARQLAccessPolicy.create(accessPolicyData);
        } catch (err) {
            console.log(err);
            this.return({"status": 1, "message": "Cannot save semantic data for catalog on update!"});
            return;
        }

        this.return(null, {"status": 0});

    },

    createDataset: async function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const sessionData = session.get();

        const accountData = transaction.lookup('global.Account', sessionData.company_id).get();

        const datasetData = makeDatasetFair(req, accountData);
        const authorizations = makeAuthorizationsFair(req, req.status);
        const accessPolicyData = SPARQLAccessPolicy.toSPARQL(datasetData.accessRights, authorizations);

        /*save new assets for audit and consensus purposes*/
        const dataset = transaction.lookup('global.Dataset', datasetData.dataset_id);
        dataset.create(datasetData);

        /*create authorizations*/
        let toCommitAuthorizations = [];
        authorizations.forEach((auth) => {
            const authorization = transaction.lookup('global.Authorization', auth.authorization_id);
            authorization.create(auth);
            toCommitAuthorizations.push(authorization);
        });

        /*create access policy */
        const accessPolicy = transaction.lookup('global.AccessPolicy', accessPolicyData.policy_id);
        accessPolicy.create(accessPolicy);

        try {
            toCommitAuthorizations.forEach(authorization => transaction.add(authorization));
            transaction.add(dataset);
            transaction.add(accessPolicy);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot create dataset!'});
            return;
        }

        try {
            /*add fair data dataset */
            await createFairDataset(datasetData);
            /*add fair data authorizations*/
            await createFairAuthorizations(authorizations);
            /*add fair data access policy */
            await SPARQLAccessPolicy.create(accessPolicyData);
            /*update fair catalog to contain the new dataset */
            await SPARQLCatalog.updateCatalogDatasets(datasetData);
        } catch (err) {
            console.log(err);
            this.return({"status": 1, "message": "Cannot save semantic data for catalog!"});
            return;
        }

        this.return(null, {"status": 0});

    },
    getDatsForUser: async function (req) {

        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const sessionData = session.get();

        const accountData = transaction.lookup('global.Account', sessionData.company_id).get();

        // ia toate autorizatiile cu drepturi write/control pt user (sau ia-le pe toate si filtreaza-le pe cele cu write/control)
        const {authList} = await getFairAuthorizationsForUserWriteControl(accountData.resource_id);
        if (!authList || !authList.length) {
            this.return(null, {"status": 0, "datasets": []});
            return;
        }
        // ia toate accessPolicy-urile care au in isPartOf authorization_id-rile gasite mai sus
        const {policiesList, authorizationIds, policyAuthBind} = await SPARQLAccessPolicy.getFairAccessPoliciesForUser(authList);


        if (!policiesList || !policiesList.length) {
            this.return(null, {"status": 0, "datasets": []});
            return;
        }
        //cauta toate autorizatiile cu id-uri in politele gasite mai sus si adauga-le si policy_id
        let authDataReloaded = await getFairAuthorizationsWithId(authorizationIds, policyAuthBind);

        authDataReloaded = authDataReloaded.map((item) => {
            const item_id = item['http://www.w3.org/ns/auth/acl#agent'];
            const idx = item_id.lastIndexOf('/');
            const accData = transaction.lookup('global.Account', item_id.slice(idx + 1)).get();
            item.entityName = accData.name;
            return item;
        });

        // ia dataseturile care au in accessRights id-ul accessPoxlicy-urile gasite mai sus
        const fairDatasets = await getFairDatasetsForUser(policiesList, authDataReloaded);

        this.return(null, {"status": 0, "datasets": fairDatasets});
    },
    getAllDatasets: async function (req) {

        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        let {fairDatasets, authorizationIds} = await getFairDatasets();

        //cauta toate autorizatiile cu id-uri in politele gasite mai sus si adauga-le si policy_id
        let authDataReloaded = await getFairAuthorizationsWithId(authorizationIds, {});
        authDataReloaded = authDataReloaded.map((item) => {
            const item_id = item['http://www.w3.org/ns/auth/acl#agent'];
            const idx = item_id.lastIndexOf('/');
            const accData = transaction.lookup('global.Account', item_id.slice(idx + 1)).get();
            item.entityName = accData.name;
            return item;
        });

        fairDatasets.forEach((propValue) => {
            propValue['http://purl.org/dc/terms/accessRights'] = authDataReloaded.filter(el => el.policy_id === propValue['http://purl.org/dc/terms/accessRights']);
            if (!propValue.hasOwnProperty('http://www.w3.org/ns/dcat#distribution')) {
                propValue['http://www.w3.org/ns/dcat#distribution'] = [];
            }
        });

        this.return(null, {"status": 0, "datasets": fairDatasets});
    },
    getCategoriesForDats: async function (req) {
        try {
            const res = await getFairCategories();
            this.return(null, res);
        } catch (err) {
            this.return({status: 1, message: 'Problem retrieving categories'});
        }
    }
});
