const SPARQLQueryDispatcher = require('../utils/SPARQLQueryDispatcher');
const constants = require('../../config/constants');
const client = new SPARQLQueryDispatcher(constants.fusekiOrganizationUpdateEndpoint);

$$.swarm.describe("account", {
    create: async function (req) {
        const that = this;
        let transaction = $$.blockchain.beginTransaction({});
        let account = transaction.lookup('global.Account', req.company_id);

        account.create(req);

        const accountData = account.get();

        const query = `PREFIX foaf: <http://xmlns.com/foaf/0.1#> 
            PREFIX schema: <http://schema.org/Thing#>
            INSERT DATA { 
            <${accountData.resource_id}> a foaf:Organization; 
            foaf:name '${accountData.name}' ; 
            schema:url '${accountData.address}' ; 
            schema:identifier '${accountData.company_id}' 
            }`;

        try{
            await client.query(query)
        } catch(err){
            console.log(err);
            that.return({"status": 1, "message": "Cannot add semantic data!"});
            return;
        }

        try {
            transaction.add(account);
            $$.blockchain.commit(transaction);
        } catch (err) {
            that.return({"status": 1, "message": "Account creation failed!"});
            return;
        }

        that.return(null, {"status": 0});

    },
    update: function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if(!session || !session.isValid()){
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const sessionInfo = session.get();
        const account = transaction.lookup('global.Account', sessionInfo.company_id);

        console.log('Updating account');
        const updatedData = account.updateAcc(req);
        console.log(`Updated account : ${JSON.stringify(updatedData)}`);

        try {
            transaction.add(account);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot update account data!'});
            return;
        }
        this.return(null, {"status": 0});
    },
    delete: function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if(!session || !session.isValid()){
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const sessionInfo = session.get();
        const account = transaction.lookup('global.Account', sessionInfo.company_id);
        account.deleteAcc(req);
        session.remove();

        try {
            transaction.add(account);
            transaction.add(session);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot delete account data!'});
            return;
        }
        this.return(null, {"status": 0});
    },
    get: function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if(!session || !session.isValid()){
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const sessionInfo = session.get();
        const account = transaction.lookup('global.Account', sessionInfo.company_id);

        if(!account){
            this.return({'status': 1, 'message': 'Cannot retrieve account data!'});
            return;
        }

        try {
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
        }

        this.return(null, {"status": 0, data: account.get()});
    },
    getAll: function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if(!session || !session.isValid()){
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        let allAccounts = transaction.loadAssets('global.Account');
        allAccounts = allAccounts.map((el) => {
            const accData = el.get();
            return {
                resource_id: accData.resource_id,
                company_id: accData.company_id,
                name: accData.name
            }
        });

        allAccounts = allAccounts.filter(el => el.name !== '');

        if(!allAccounts || !allAccounts.length){
            this.return({'status': 1, 'message': 'Cannot retrieve all accounts!'});
            return;
        }

        try {
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
        }

        this.return(null, {"status": 0, data: allAccounts});
    },
    getPendingAuthorizations: async () => {
        // ia din fuseki toate distributiile pe care userul are write sau control
        // ia toate autorizatiile din blockchain cu status pending si care au un distribution_id unul din cele de mai sus;
    }
});
