$$.swarm.describe('session', {
    login: function (req) {
        console.log('validating credentials for account: ' + JSON.stringify(req));

        let transaction = $$.blockchain.beginTransaction({});

        let uid = $$.uidGenerator.safe_uuid();
        let session = transaction.lookup('global.Session', uid);

        let account = transaction.lookup('global.Account', req.company_id);

        if(!account.validateCredentials(req))
            this.return({status: 1, message: 'Wrong credentials'});

        session.add(req.company_id, uid);

        try {
            transaction.add(session);
            $$.blockchain.commit(transaction);
        } catch (err) {
            this.return({'status': 1, 'message': 'Session creation failed!'});
            return;
        }

        this.return(null, {'status': 0, 'user_token': uid});
    },
    logout: function(req) {
        let transaction = $$.blockchain.beginTransaction({});
        let session = transaction.lookup('global.Session', req.user_token);
        session.remove();

        try {
            transaction.add(session);
            $$.blockchain.commit(transaction);
        } catch (err) {
            this.return({'status': 1, 'message': 'Invalidating session failed!'});
            return;
        }
        this.return(null, {'status': 0});

    }
});
