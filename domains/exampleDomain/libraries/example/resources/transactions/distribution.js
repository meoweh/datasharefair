const {updateDatasetWithDistribution, getFairCatalogIdForDataset} = require("../sparql-wrappers/dataset");
const {createFairDistribution, deleteFairDistribution} = require("../sparql-wrappers/distribution");
const {createFairAuthorizations, deleteFairAuthorization} = require("../sparql-wrappers/authorization");
const {makeAuthorizationsFair} = require("../utils/makeDataFair");
const {makeDistributionFair, convertDistributionFair} = require("../utils/makeDataFair");
const SPARQLAccessPolicy = require('../sparql-wrappers/SPARQLAccessPolicy');
const {decrypt} = require("../utils/encryptor");
const {getFairAccessPolicyForDistribution} = require("../sparql-wrappers/distribution");
const {getDistForAccessPolicy} = require("../sparql-wrappers/distribution");
const {getFairAuthorizationsForUserWriteControl} = require("../sparql-wrappers/authorization");
const {getFairAuthorizationsWithId} = require("../sparql-wrappers/authorization");
const {getFairDistributions} = require("../sparql-wrappers/distribution");

/**
 * @typedef {Object} AccessRightDistributionUI
 * @property {string} authorization_id - "http://127.0.0.1:8087/fdp/ce4c2899-8696-43d6-baa8-d7be24dc4e8f"
 * @property {string} policy_id - "http://127.0.0.1:8087/fdp/distribution-emi-1#accessRights"
 * @property {string} status - "Approved"
 * @property {string} type - "http://www.w3.org/ns/auth/acl#Authorization"
 * @property {string} accessLevel - "Control"
 * @property {string} entityId - "http://127.0.0.1/fdp/23456789"
 * @property {string} entityName - "Pingulin Bombolin"
 */

/**
 * @typedef {Object} DistributionUIDelete
 * @property {string} distribution_id - "http://127.0.0.1:8087/fdp/distribution-emi-1"
 * @property {string} type - "http://www.w3.org/ns/dcat#Distribution"
 * @property {string} conformsTo - "https://www.purl.org/fairtools/fdp/schema/0.1/distributionMetadata"
 * @property {string} description - "distribution-emi-1"
 * @property {string} version - "distribution-emi-1"
 * @property {string} dataset - "http://127.0.0.1:8087/fdp/dataset-emi-1"
 * @property {string} language - "aa"
 * @property {string} license - "https://opensource.org/licenses/AAL"
 * @property {string} publisher - "http://127.0.0.1/fdp/23456789"
 * @property {string} title - "distribution-emi-1"
 * @property {string} metadataIdentifier - "http://127.0.0.1:8087/fdp/distribution-emi-1"
 * @property {string} created - "2020-04-08T19:05:54.391Z"
 * @property {string} modified - "2020-04-08T19:05:54.391Z"
 * @property {string} label - "distribution-emi-1"
 * @property {string} url - "http://server/unset-base/"
 * @property {string} mediaType - "application/gzip"
 * @property {Array<AccessRightDistributionUI>} accessRights
 * @property {string} user_token
 */

/**
 * @typedef {Object} DistributionUIUpdate
 * @property {string} distribution_id - "http://127.0.0.1:8087/fdp/distribution-emi-1"
 * @property {string} dataset - "http://127.0.0.1:8087/fdp/dataset-emi-1"
 * @property {string} title - "distribution-emi-1"
 * @property {string} publisher - "http://127.0.0.1/fdp/23456789"
 * @property {string} description - "distribution-emi-1"
 * @property {string} version - "distribution-emi-1"
 * @property {string} language - "aa"
 * @property {string} license - "https://opensource.org/licenses/AAL"
 * @property {string} url - "http://server/unset-base/"
 * @property {string} mediaType - "application/gzip"
 * @property {string} label - "distribution-emi-1"
 * @property {string} created - "2020-04-08T19:05:54.391Z"
 * @property {string} modified - "2020-04-08T19:05:54.391Z"
 * @property {Array<AccessRightDistributionUI>} accessRights
 * @property {string} user_token
 */

$$.swarm.describe("distribution", {
    updateDist: async function (/*DistributionUIUpdate*/ req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const sessionData = session.get();

        const accountData = transaction.lookup('global.Account', sessionData.company_id).get();

        const oldAccessRights = req.accessRights;
        console.log(req);
        const distributionData = convertDistributionFair(req);
        console.log(distributionData);

        const authorizations = makeAuthorizationsFair(req, req.status);
        const accessPolicyData = SPARQLAccessPolicy.toSPARQL(distributionData.accessRights, authorizations);

        let toCommitDeletedAuth = [];
        for (const accessRight of oldAccessRights) {
            await deleteFairAuthorization(accessRight.authorization_id);
            const authorization = transaction.lookup('global.Authorization', accessRight.authorization_id);
            authorization.remove();
            toCommitDeletedAuth.push(authorization);
            await SPARQLAccessPolicy.delete(accessRight.policy_id);
        }

        await deleteFairDistribution(distributionData.distribution_id);

        /*save new assets for audit and consensus purposes*/
        const distribution = transaction.lookup('global.Distribution', distributionData.distribution_id);
        distribution.create(distributionData);

        /*create authorizations*/
        let toCommitAuthorizations = [];
        authorizations.forEach((auth) => {
            const authorization = transaction.lookup('global.Authorization', auth.authorization_id);
            authorization.create(auth);
            toCommitAuthorizations.push(authorization);
        });

        /*create access policy */
        const accessPolicy = transaction.lookup('global.AccessPolicy', accessPolicyData.policy_id);
        accessPolicy.create(accessPolicy);

        try {
            toCommitDeletedAuth.forEach(authorization => transaction.add(authorization));
            toCommitAuthorizations.forEach(authorization => transaction.add(authorization));
            transaction.add(distribution);
            transaction.add(accessPolicy);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot create distribution!'});
            return;
        }

        try {
            /*add fair data dataset */
            await createFairDistribution(distributionData);
            /*add fair data authorizations*/
            await createFairAuthorizations(authorizations);
            /*add fair data access policy */
            await SPARQLAccessPolicy.create(accessPolicyData);
        } catch (err) {
            console.log(err);
            this.return({"status": 1, "message": "Cannot save semantic data for distribution on update!"});
            return;
        }

        this.return(null, {"status": 0});
    },
    createDistribution: async function (req) {
        // creare distributie
        // creare polita de acces
        // creare autorizari
        // asociere distributie la dataset
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const sessionData = session.get();

        const accountData = transaction.lookup('global.Account', sessionData.company_id).get();

        const distributionData = makeDistributionFair(req, accountData);
        const authorizations = makeAuthorizationsFair(req, req.status);
        const accessPolicyData = SPARQLAccessPolicy.toSPARQL(distributionData.accessRights, authorizations);

        /*save new assets for audit and consensus purposes*/
        const distribution = transaction.lookup('global.Distribution', distributionData.distribution_id);
        distribution.create(distributionData);

        /*create authorizations*/
        let toCommitAuthorizations = [];
        authorizations.forEach((auth) => {
            const authorization = transaction.lookup('global.Authorization', auth.authorization_id);
            authorization.create(auth);
            toCommitAuthorizations.push(authorization);
        });

        /*create access policy */
        const accessPolicy = transaction.lookup('global.AccessPolicy', accessPolicyData.policy_id);
        accessPolicy.create(accessPolicy);

        try {
            toCommitAuthorizations.forEach(authorization => transaction.add(authorization));
            transaction.add(distribution);
            transaction.add(accessPolicy);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot create distribution!'});
            return;
        }

        try {
            /*add fair data catalog */
            await createFairDistribution(distributionData);
            /*add fair data authorizations*/
            await createFairAuthorizations(authorizations);
            /*add fair data access policy */
            await SPARQLAccessPolicy.create(accessPolicyData);
            /*update fair dataset to contain the new distribution */
            await updateDatasetWithDistribution(distributionData);
        } catch (err) {
            console.log(err);
            this.return({"status": 1, "message": "Cannot save semantic data for distribution!"});
            return;
        }

        this.return(null, {"status": 0, "distribution_id": distributionData.distribution_id});
    },
    getDistForDatasets: async function (req) {
        // ia toate distributiile care au in isPartOf datasetul din request
        //cauta autorizatiile asociate si construieste array-ul de accessRights
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        for (let dataset of req.datasets) {
            try {
                // const catalog_id = await getFairCatalogIdForDataset(dataset.dataset_id);
                let {fairDistributions, policyIds} = await getFairDistributions(dataset);
                const {policiesList, authorizationIds, policyAuthBind} = await SPARQLAccessPolicy.getAuthorizationByPolicy(policyIds);
                let authDataReloaded = await getFairAuthorizationsWithId(authorizationIds, policyAuthBind);
                authDataReloaded = authDataReloaded.map((item) => {
                    const item_id = item['http://www.w3.org/ns/auth/acl#agent'];
                    const idx = item_id.lastIndexOf('/');
                    const accData = transaction.lookup('global.Account', item_id.slice(idx + 1)).get();
                    item.entityName = accData.name;
                    return item;
                });

                fairDistributions.forEach((propValue) => {
                    propValue['http://purl.org/dc/terms/accessRights'] = authDataReloaded.filter(el => el.policy_id === propValue['http://purl.org/dc/terms/accessRights']);
                    // propValue['http://www.w3.org/ns/dcat#catalog'] = catalog_id;
                });
                console.log(JSON.stringify(fairDistributions));
                dataset['http://www.w3.org/ns/dcat#distribution'] = fairDistributions;
            } catch (e) {
                this.return({status: 1, message: 'Error retrieving distributions'});
                return;
            }
        }

        this.return(null, {"status": 0, "datasets": req.datasets});

    },
    deleteDist: async function (/*DistributionUIDelete*/ req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const sessionData = session.get();

        const accountData = transaction.lookup('global.Account', sessionData.company_id).get();

        req.accessRights.forEach(accessRight => {
            const Bauthorization = transaction.lookup('global.Authorization', accessRight.authorization_id);
            Bauthorization.remove();
            transaction.add(Bauthorization);
        });

        const Bpolicy = transaction.lookup('global.AccessPolicy', req.accessRights[0].policy_id);
        Bpolicy.remove();
        transaction.add(Bpolicy);

        const Bdistribution = transaction.lookup('global.Distribution', req.distribution_id);
        Bdistribution.remove();
        transaction.add(Bdistribution);

        try {
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot delete distribution!'});
            return;
        }

        try {
            /*delete fair data distribution */
            await deleteFairDistribution(req.distribution_id);
            /*delete fair data authorizations*/
            await Promise.all(req.accessRights.map(accessRight => deleteFairAuthorization(accessRight.authorization_id)));
            /*delete fair data access policy */
            await SPARQLAccessPolicy.delete(req.accessRights[0].policy_id);
        } catch (err) {
            console.log(err);
            this.return({"status": 1, "message": "Cannot delete semantic data for distribution!"});
            return;
        }

        this.return(null, {"status": 0});
        // sterge distributia si trigger stergere si la arhiva asociata la downloadURL + curata autorizatiile
        // NU STERGE ARHIVA
    },
    requestAccess: async function (req) {
        // butonul de request access din marketplace triggers it => se adauga un authorization pending in blockchain cu tot cu distribution_id (fara fuseki!)
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const authData = makeAuthorizationsFair(req, 'pending');
        authData[0].distribution_id = req.distribution.distribution_id;
        const authorization = transaction.lookup('global.Authorization', authData.authorization_id);
        authorization.create(authData[0]);

        try {
            transaction.add(authorization);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot create distribution request access!'});
            return;
        }

        this.return(null, {"status": 0});

    },
    approveAccess: async function (req) {
        // authorization-ul pending se updateaza sa fie approved si se insereaza in fuseki noua autorizatie + se adauga legatura cu noua autorizarie in accessPolicy-ul distributiei
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        let allAuthorizations = transaction.loadAssets('global.Authorization');
        const allPendingAuthorizations = allAuthorizations.map(el => el.get());
        let authToApprove = allPendingAuthorizations.filter(el => el.agent === req.accessEntityId && el.distribution_id === req.resourceId);

        const authorization = transaction.lookup('global.Authorization', authToApprove[0].authorization_id);

        console.log('GET AUTH ==================');
        console.log(JSON.stringify(authorization.get()));
        console.log('GET AUTH ==================');

        const authData = authorization.get();
        authData.status = 'approved';

        let accessPolicyId = await getFairAccessPolicyForDistribution(authData.distribution_id);

        let accessPolicy = await SPARQLAccessPolicy.getPolicyById(accessPolicyId);

        accessPolicy[0].isPartOf = accessPolicy[0].isPartOf + `, <${authData.authorization_id}>`;

        authorization.updateAut(authData);

        try{
            await createFairAuthorizations([authData]);

            await SPARQLAccessPolicy.create(accessPolicy[0]);
        }catch(err){
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot save request access!'});
            return;
        }

        try {
            transaction.add(authorization);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot commit request access!'});
            return;
        }

        this.return(null, {"status": 0});

    },
    retrieveAccessToApproveForUser: async function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const sessionData = session.get();
        const accountData = transaction.lookup('global.Account', sessionData.company_id).get();

        let allAuthorizations = transaction.loadAssets('global.Authorization');

        const allPendingAuthorizations = allAuthorizations.map(el => el.get());

        let {authList, authData} = await getFairAuthorizationsForUserWriteControl(accountData.resource_id);
        authData = authData.filter(item => item['http://www.w3.org/ns/auth/acl#mode'] === 'http://www.w3.org/ns/auth/acl#Control');
        let allUserControlAuth = authData.map(item => item.authorization_id);
        const {policiesList, authorizationIds, policyAuthBind} = await SPARQLAccessPolicy.getFairAccessPoliciesForUser(allUserControlAuth);

        //array de distribution ids
        let distributions = await getDistForAccessPolicy(policiesList);
        distributions = distributions.map(el => el.distribution);

        let resp = allPendingAuthorizations.filter(el => el.hasOwnProperty('removed') && !el.removed && el.status === 'pending' && distributions.includes(el.distribution_id));
        resp.map((item) => {
            item.resource_type = 'distribution';
            const acc = transaction.lookup('global.Account', item.agent.slice(item.agent.lastIndexOf('/')));
            if (acc) {
                item.entity_name = acc.get().name;
                item.access_type = 'User';
            }
            const group = transaction.lookup('global.Group', item.agent.slice(item.agent.lastIndexOf('/')));
            if (group) {
                item.entity_name = group.get().group_name;
                item.access_type = 'Group';
            }
            const dist = transaction.lookup('global.Distribution', item.distribution_id);
            item.resource_title = dist.get().title;
        });
        // get all authorization policies where user has Control for distributions from Jena
        this.return(null, resp);

    },
    getDecryptedDistribution: async function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const Bdistribution = transaction.lookup('global.Distribution', req.distribution_id);
        const encKey = Bdistribution.getEncryptionKey();

        console.log(encKey);

        console.log(`MY FILE: ${req.file}`);
        const decryptedContent = decrypt(req.file, encKey);

        try {
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot retrieve encryption key!'});
            return;
        }

        // base64 so bytes arent lost on encoding?
        this.return(null, {file: decryptedContent.toString('base64')});
    },
    saveEncryptionKeySwarm: function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }
        const Bdistribution = transaction.lookup('global.Distribution', req.distribution_id);
        Bdistribution.addEncryptionKey(req.key);

        try {
            transaction.add(Bdistribution);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot retrieve encryption key!'});
            return;
        }

        this.return(null, {status: 0});
    }
});
