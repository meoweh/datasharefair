$$.swarm.describe('group', {
    create: function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        let group = transaction.lookup('global.Group', req.group_id);

        req.group_id = req.group_name.replace(/\s/g, '_') + '_' + $$.uidGenerator.safe_uuid();
        group.create(req);

        console.log('adding to blockchain: ' + req);

        try {
            transaction.add(group);
            $$.blockchain.commit(transaction);
        } catch (err) {
            this.return({'status': 1, 'message': 'Group creation failed!'});
            return;
        }

        this.return(null, {'status': 0});
    },
    removeGroupMember: function(req){
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if(!session || !session.isValid()){
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const group = transaction.lookup('global.Group', req.group_id);

        group.removeMember(req.company_id);

        try {
            transaction.add(group);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot add member to group!'});
            return;
        }
        this.return(null, {'status': 0});
    },
    addGroupMember: function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if(!session || !session.isValid()){
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const group = transaction.lookup('global.Group', req.group_id);

        group.addMember(req.company_id);

        try {
            transaction.add(group);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot add member to group!'});
            return;
        }
        this.return(null, {'status': 0});
    },
    delete: function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if(!session || !session.isValid()){
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const group = transaction.lookup('global.Group', req.group_id);
        group.deleteGroup();

        try {
            transaction.add(group);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot delete group data!'});
            return;
        }
        this.return(null, {'status': 0});
    },
    getAll: function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if(!session || !session.isValid()){
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }
        const sessionInfo = session.get();
        const currentAcc = transaction.lookup('global.Account', sessionInfo.company_id);

        let allGroups = transaction.loadAssets('global.Group');
        allGroups = allGroups.filter(el => el.group_name !== '' && el.members.includes(currentAcc.company_id));

        allGroups = allGroups.map((el) => {
            let groupData = el.get();
            groupData.members = groupData.members.map((member) => {
                const account = transaction.lookup('global.Account', member);
                return {
                    resource_id: account.resource_id,
                    company_id: account.company_id,
                    category: account.category,
                    name: account.name,
                    region: account.region
                }
            });
            return groupData;
        });


        if(!allGroups || !allGroups.length){
            this.return(null, []);
            return;
        }

        try {
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
        }

        this.return(null, {'status': 0, data: allGroups});
    }
});
