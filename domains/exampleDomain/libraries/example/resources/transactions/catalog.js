const {makeAuthorizationsFair} = require('../utils/makeDataFair');
const {createFairAuthorizations, getFairAuthorizationsForUserWriteControl, getFairAuthorizationsWithId, deleteFairAuthorization} = require('../sparql-wrappers/authorization');
const SPARQLAccessPolicy = require('../sparql-wrappers/SPARQLAccessPolicy');
const SPARQLCatalog = require('../sparql-wrappers/SPARQLCatalog');

$$.swarm.describe("catalog", {
    createCat: async function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const sessionData = session.get();

        const accountData = transaction.lookup('global.Account', sessionData.company_id).get();

        const catalogData = SPARQLCatalog.toSPARQLCreate(req, accountData);
        const authorizations = makeAuthorizationsFair(req, req.status);
        const accessPolicyData = SPARQLAccessPolicy.toSPARQL(catalogData.accessRights, authorizations);

        /*save new assets for audit and consensus purposes*/
        const catalog = transaction.lookup('global.Catalog', catalogData.catalog_id);
        catalog.create(catalogData);

        /*create authorizations*/
        let toCommitAuthorizations = [];
        authorizations.forEach((auth) => {
            const authorization = transaction.lookup('global.Authorization', auth.authorization_id);
            authorization.create(auth);
            toCommitAuthorizations.push(authorization);
        });

        /*create access policy */
        const accessPolicy = transaction.lookup('global.AccessPolicy', accessPolicyData.policy_id);
        accessPolicy.create(accessPolicy);

        try {
            toCommitAuthorizations.forEach(authorization => transaction.add(authorization));
            transaction.add(catalog);
            transaction.add(accessPolicy);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot create catalog!'});
            return;
        }

        try {
            /*add fair data catalog */
            await SPARQLCatalog.create(catalogData);
            /*add fair data authorizations*/
            await createFairAuthorizations(authorizations);
            /*add fair data access policy */
            await SPARQLAccessPolicy.create(accessPolicyData);
        } catch (err) {
            console.log(err);
            this.return({"status": 1, "message": "Cannot save semantic data for catalog!"});
            return;
        }

        this.return(null, {"status": 0});

    },
    updateCat: async function (req) {
        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }


        const oldAccessRights = req.accessRights;
        const catalogData = SPARQLCatalog.toSPARQL(req);

        const authorizations = makeAuthorizationsFair(req, req.status);
        const accessPolicyData = SPARQLAccessPolicy.toSPARQL(catalogData.accessRights, authorizations);

        let toCommitDeletedAuth = [];
        for (const accessRight of oldAccessRights) {
            await deleteFairAuthorization(accessRight.authorization_id);
            const authorization = transaction.lookup('global.Authorization', accessRight.authorization_id);
            authorization.remove();
            toCommitDeletedAuth.push(authorization);
            await SPARQLAccessPolicy.delete(accessRight.policy_id);
        }

        await SPARQLCatalog.delete(catalogData.catalog_id);

        /*save new assets for audit and consensus purposes*/
        const catalog = transaction.lookup('global.Catalog', catalogData.catalog_id);
        catalog.create(catalogData);

        /*create authorizations*/
        let toCommitAuthorizations = [];
        authorizations.forEach((auth) => {
            const authorization = transaction.lookup('global.Authorization', auth.authorization_id);
            authorization.create(auth);
            toCommitAuthorizations.push(authorization);
        });

        /*create access policy */
        const accessPolicy = transaction.lookup('global.AccessPolicy', accessPolicyData.policy_id);
        accessPolicy.create(accessPolicy);

        try {
            toCommitDeletedAuth.forEach(authorization => transaction.add(authorization));
            toCommitAuthorizations.forEach(authorization => transaction.add(authorization));
            transaction.add(catalog);
            transaction.add(accessPolicy);
            $$.blockchain.commit(transaction);
        } catch (err) {
            console.error(err);
            this.return({'status': 1, 'message': 'Cannot create catalog!'});
            return;
        }

        try {
            /*add fair data catalog */
            await SPARQLCatalog.create(catalogData);
            /*add fair data authorizations*/
            await createFairAuthorizations(authorizations);
            /*add fair data access policy */
            await SPARQLAccessPolicy.create(accessPolicyData);
        } catch (err) {
            console.log(err);
            this.return({"status": 1, "message": "Cannot save semantic data for catalog on update!"});
            return;
        }

        this.return(null, {"status": 0});
    },
    getCatsForUser: async function (req) {

        let transaction = $$.blockchain.beginTransaction({});
        const session = transaction.lookup('global.Session', req.user_token);

        if (!session || !session.isValid()) {
            this.return({'status': 1, 'message': 'Expired Token!'});
            return;
        }

        const sessionData = session.get();

        const accountData = transaction.lookup('global.Account', sessionData.company_id).get();

        // ia toate autorizatiile cu drepturi write/control pt user (sau ia-le pe toate si filtreaza-le pe cele cu write/control)
        const {authList} = await getFairAuthorizationsForUserWriteControl(accountData.resource_id);
        if (!authList || !authList.length) {
            this.return(null, {"status": 0, "catalogs": []});
            return;
        }
        // ia toate accessPolicy-urile care au in isPartOf authorization_id-rile gasite mai sus
        const {policiesList, authorizationIds, policyAuthBind} = await SPARQLAccessPolicy.getFairAccessPoliciesForUser(authList);


        if (!policiesList || !policiesList.length) {
            this.return(null, {"status": 0, "catalogs": []});
            return;
        }
        //cauta toate autorizatiile cu id-uri in politele gasite mai sus si adauga-le si policy_id
        let authDataReloaded = await getFairAuthorizationsWithId(authorizationIds, policyAuthBind);

        authDataReloaded = authDataReloaded.map((item) => {
            const item_id = item['http://www.w3.org/ns/auth/acl#agent'];
            const idx = item_id.lastIndexOf('/');
            const accData = transaction.lookup('global.Account', item_id.slice(idx + 1)).get();
            item.entityName = accData.name;
            return item;
        });

        // ia cataloagele care au in accessRights id-ul accessPoxlicy-urile gasite mai sus
        const fairCatalogs = await SPARQLCatalog.getForUser(policiesList, authDataReloaded);

        this.return(null, {"status": 0, "catalogs": fairCatalogs});
    },
    getAllCatalogsNameId: async function (req) {
        try {
            const res = await SPARQLCatalog.getSimple();
            this.return(null, {status: 0, catalogs: res});
        } catch (err) {
            this.return({status: 1, message: 'Error retrieving catalogs'});
        }

    }
});
