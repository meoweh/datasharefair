const constants = require('../../config/constants');
const axios = require('axios');
const FuzzySearch = require('fuzzy-search');
const SPARQLQueryDispatcher = require('../utils/SPARQLQueryDispatcher');


$$.swarm.describe("getFairResources", {
    getSemanticCategories: async function (req) {
        //retrieve data from wikidata and dbpedia to query concepts

        //search dbpedia + apply fuzzy search on results
        const self = this;
        let semanticCat = [];
        try {
            semanticCat = (await axios.get(`http://lookup.dbpedia.org/api/search/KeywordSearch?QueryString=${req.category}`)).data.results;
        } catch (error) {
            console.error(error);
        }
        if (semanticCat && semanticCat.length) {
            this.swarm('local/agent/gofair', 'fuzzyCategories', 'dbpedia', semanticCat, req.category);
        }
        //search wikidata
        const query = `SELECT distinct ?item ?itemLabel ?itemDescription WHERE{  
                  ?item ?label "${req.category}"@en.  
                  ?article schema:about ?item .
                  ?article schema:inLanguage "en" .
                  ?article schema:isPartOf <https://en.wikipedia.org/>.	
                  SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }    
                }`;

        const queryDispatcher = new SPARQLQueryDispatcher(constants.wikidataEndpoint);
        try {
            semanticCat = (await queryDispatcher.query(query)).results.bindings;
        } catch (err) {
            console.log(err);
            self.return({"status": 1, "message": "Cannot find categories on either DBPedia or Wikidata!"});
            return;
        }

        if (!semanticCat || !semanticCat.length) {
            self.return(null, []);
        }

        self.swarm('local/agent/getFairResources', 'fuzzyCategories', 'wikidata', semanticCat, req.category);

    },
    fuzzyCategories: function (searchSource, searchResults, category) {
        let keys = [];
        if (searchSource === 'dbpedia') {
            keys = ['label', 'categories.label']
        } else {
            keys = ['itemLabel.value']
        }

        const searcher = new FuzzySearch(searchResults, keys, {
            caseSensitive: false,
            sort: true
        });

        const sanitized = category.replace(/[\W_]+/g, " ");

        const result = searcher.search(sanitized);

        this.return(null, result);

    }
});
