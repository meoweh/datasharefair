const keyword_extractor = require("keyword-extractor");
const constants = require('../../config/constants');
const uuid = require('uuid');

// needs {catalog_id: string, title: string, publisher: string, description:string
// version: string, language: string, license: string, created: string, modified: string
// accessRights: string, datasets: null, categories: Array<string>
exports.convertCatalogFair = function (req) {
    let catalogData = {};

    catalogData.catalog_id = req.catalog_id;
    catalogData.description = req.description;
    catalogData.hasVersion = req.version;
    catalogData.language = `lang:${req.language}`;
    catalogData.license = req.license;
    catalogData.publisher = req.publisher;
    catalogData.title = req.title;
    catalogData.metadataIdentifier = catalogData.catalog_id;
    const now = new Date().toISOString();
    catalogData.metadataIssued = req.created;
    catalogData.metadataModified = now;
    catalogData.label = req.title;
    catalogData.themeTaxonomy = req.categories.map(el => `<${el}>`).join(', ');
    catalogData.accessRights = `${catalogData.catalog_id}#accessRights`;
    catalogData.conformsTo = `https://www.purl.org/fairtools/fdp/schema/0.1/catalogMetadata`;

    return catalogData;
};

exports.makeCatalogFair = function (req, account) {

    let catalogData = {};

    let extraction_result = keyword_extractor.extract(req.catalogName, {
        language: "english",
        remove_digits: true,
        return_changed_case: true,
        remove_duplicates: false
    });
    const keywords_title = `${extraction_result.join('_')}`;
    catalogData.catalog_id = `${constants.fdp_address}` + `catalogs/` + `${keywords_title}`;
    catalogData.description = req.catalogDescription;
    catalogData.hasVersion = req.catalogVersion;
    catalogData.language = `lang:${req.catalogLanguage}`;
    catalogData.license = req.catalogLicense;
    catalogData.publisher = account.resource_id;
    catalogData.title = req.catalogName;
    catalogData.metadataIdentifier = catalogData.catalog_id;
    const now = new Date().toISOString();
    catalogData.metadataIssued = now;
    catalogData.metadataModified = now;
    catalogData.label = req.catalogName;
    catalogData.themeTaxonomy = req.catalogTheme.map(el => `<${el}>`).join(', ');
    catalogData.accessRights = `${catalogData.catalog_id}#accessRights`;
    catalogData.conformsTo = `https://www.purl.org/fairtools/fdp/schema/0.1/catalogMetadata`;

    return catalogData;
};

// needs {accessRights:string}, Array<{authorization_id: string}>
exports.makeAccessPolicyFair = function (catalogData, authorizations) {
    return {
        policy_id: catalogData.accessRights,
        description: 'This resource has access restriction',
        isPartOf: authorizations.map(auth => `<${auth.authorization_id}>`).join(', ')
    }
};

//needs {accessRights: Array<{accessLevel, entityId}>, status?}
exports.makeAuthorizationsFair = function (req, status) {

    let authorizations = [];
    req.accessRights.forEach((access) => {
        let authorization = {};
        authorization.authorization_id = constants.fdp_address + `authorizations/` + uuid.v4();
        authorization.mode = `acl:${access.accessLevel}`;
        authorization.agent = access.entityId;
        authorization.status = 'approved';
        //pentru cazul de request si status pending
        if(status){
            authorization.status = status;
        }
        if(access.accessType){
            authorization.agent_type = access.accessType;
        }
        authorizations.push(authorization);
    });

    return authorizations;
};

exports.convertDatasetFair = function (req) {
    let datasetData = {};
    datasetData.catalog = req.catalog;
    datasetData.dataset_id = req.dataset_id;
    datasetData.description = req.description;
    datasetData.hasVersion = req.version;
    datasetData.language = `lang:${req.language}`;
    datasetData.license = req.license;
    datasetData.publisher = req.resource_id;
    datasetData.title = req.title;
    datasetData.metadataIdentifier = datasetData.dataset_id;
    const now = new Date().toISOString();
    datasetData.metadataIssued = req.created;
    datasetData.metadataModified = now;
    datasetData.label = req.title;
    datasetData.themeTaxonomy = req.categories.map(el => `<${el}>`).join(', ');
    datasetData.keyword = req.keywords.map(el => `"${el}"`).join(', ');
    datasetData.accessRights = `${datasetData.dataset_id}#accessRights`;
    datasetData.conformsTo = `https://www.purl.org/fairtools/fdp/schema/0.1/datasetMetadata`;

    return datasetData;
};

exports.makeDatasetFair = function (req, account) {
    /* datasetCatalog: formValue.catalog,
      datasetName: formValue.title,
      datasetDescription: formValue.description,
      datasetVersion: formValue.version,
      datasetLanguage: formValue.language,
      datasetLicense: formValue.license,
      datasetTheme: formValue.categories,
      datasetKeywords: formValue.keywords,*/
    let datasetData = {};
    datasetData.catalog = req.datasetCatalog;
    let extraction_result = keyword_extractor.extract(req.datasetName, {
        language: "english",
        remove_digits: true,
        return_changed_case: true,
        remove_duplicates: false
    });
    const keywords_title = `${extraction_result.join('_')}`;
    datasetData.dataset_id = `${constants.fdp_address}` + `datasets/` + `${keywords_title}`;
    datasetData.description = req.datasetDescription;
    datasetData.hasVersion = req.datasetVersion;
    datasetData.language = `lang:${req.datasetLanguage}`;
    datasetData.license = req.datasetLicense;
    datasetData.publisher = account.resource_id;
    datasetData.title = req.datasetName;
    datasetData.metadataIdentifier = datasetData.dataset_id;
    const now = new Date().toISOString();
    datasetData.metadataIssued = now;
    datasetData.metadataModified = now;
    datasetData.label = req.datasetName;
    datasetData.themeTaxonomy = req.datasetTheme.map(el => `<${el}>`).join(', ');
    datasetData.keyword = req.datasetKeywords.map(el => `"${el}"`).join(', ');
    datasetData.accessRights = `${datasetData.dataset_id}#accessRights`;
    datasetData.conformsTo = `https://www.purl.org/fairtools/fdp/schema/0.1/datasetMetadata`;

    return datasetData;

};

exports.makeDistributionFair = function (req, account) {
    /*title: '',
    description: '',
    version: '',
    language: this.countries[0].alpha2,
    license: this.licenses[0].name,
    dataset: '',
    catalog: '',
    */
    let distributionData = {};
    let extraction_result = keyword_extractor.extract(req.title, {
        language: "english",
        remove_digits: true,
        return_changed_case: true,
        remove_duplicates: false
    });
    const keywords_title = `${extraction_result.join('_')}`;
    distributionData.dataset = req.dataset;
    // distributionData.catalog = req.catalog; - is this really needed here? or just remaining bug from C/P?
    distributionData.distribution_id = `${constants.fdp_address}` + `distributions/` + `${keywords_title}`;
    distributionData.accessRights = distributionData.distribution_id + '#accessRights';
    distributionData.conformsTo = 'https://www.purl.org/fairtools/fdp/schema/0.1/distributionMetadata';
    distributionData.description = req.description;
    distributionData.hasVersion = req.version;
    distributionData.isPartOf = req.dataset;
    distributionData.language = `lang:${req.language}`;
    distributionData.license = req.license;
    distributionData.publisher = account.resource_id;
    distributionData.title = req.title;
    distributionData.metadataIdentifier = distributionData.distribution_id;
    const now = new Date().toISOString();
    distributionData.metadataIssued = now;
    distributionData.metadataModified = now;
    distributionData.label = req.title;
    distributionData.downloadURL = req.downloadURL;
    distributionData.mediaType = 'application/gzip';

    return distributionData;

};

exports.convertDistributionFair = function (/*DistributionUIUpdate*/ req) {
    let distributionData = {};
    distributionData.dataset = req.dataset;
    // distributionData.catalog = req.catalog; - is this really needed here? or just remaining bug from C/P?
    distributionData.distribution_id = req.distribution_id;
    distributionData.accessRights = `${distributionData.distribution_id}#accessRights`;
    distributionData.conformsTo = `https://www.purl.org/fairtools/fdp/schema/0.1/datasetMetadata`;
    distributionData.description = req.description;
    distributionData.hasVersion = req.version;
    distributionData.isPartOf = req.dataset;
    distributionData.language = `lang:${req.language}`;
    distributionData.license = req.license;
    distributionData.publisher = req.publisher;
    distributionData.title = req.title;
    distributionData.metadataIdentifier = distributionData.distribution_id;
    const now = new Date().toISOString();
    distributionData.metadataIssued = req.created;
    distributionData.metadataModified = now;
    distributionData.label = req.title;
    distributionData.downloadURL = req.url;
    distributionData.mediaType = req.mediaType;

    return distributionData;
};
