const crypto = require('crypto');
const algorithm = 'aes-256-ctr';

exports.decrypt = function (encrypted, key) {
    // Get the iv: the first 16 bytes
    const buf = Buffer.from(encrypted, 'base64');
    const iv = buf.slice(0, 16);
    console.log(iv);
    // Get the rest
    const encr = buf.slice(16);
    // Create a decipher
    const decipher = crypto.createDecipheriv(algorithm, key, iv);
    // Actually decrypt it
    return Buffer.concat([decipher.update(encr), decipher.final()]);
};
