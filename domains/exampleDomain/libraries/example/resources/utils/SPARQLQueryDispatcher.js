const axios = require('axios');

class SPARQLQueryDispatcher {
    /**
     * @typedef {Object} Binding
     * @property {string} type
     * @property {string} value
     */

    /**
     * @typedef {Object<string,string|Array<string>>} JSONResultObject
     */

    /**
     * @typedef {Array<JSONResultObject>} JSONResult
     */

    /**
     * @typedef {Object} SPARQLResult
     * @property {Object} head
     * @property {Array<string>} head.vars
     * @property {Object} results
     * @property {Array<Object<string, Binding>>} results.bindings - keys in items of bindings represent bindings used in query and present in head
     */


    /**
     *
     * @param {string} endpoint - sparql endpoint to query
     */
    constructor(endpoint) {
        this.endpoint = endpoint;
    }

    /**
     *
     * @param {string|Array<string>} value
     * @returns {Array<string>}
     */
    coerceToArray(value){
        return Array.isArray(value) ? value : [value];
    }

    /**
     *
     * @param {string} sparqlQuery
     * @param {boolean} [update=false]
     * @returns {Promise<SPARQLResult>}
     */
    async query(sparqlQuery, update = false) {
        let fullUrl = this.endpoint;
        let method = 'GET';
        if (this.endpoint.includes('update') || update) {
            if (!this.endpoint.includes('update')) {
                fullUrl += '/update';
            }
            fullUrl += '?update=' + encodeURIComponent(sparqlQuery);
            method = 'POST';
        } else {
            fullUrl += '?query=' + encodeURIComponent(sparqlQuery);
        }
        const headers = {'Accept': 'application/sparql-results+json'};

        let response = '';
        try {
            response = await axios({
                url: fullUrl,
                method: method,
                headers: headers
            })
        } catch (err) {
            console.error(err);
            throw err;
        }
        console.log(`sparql response: ${JSON.stringify(response.data)}`);
        return response.data;
    }

    async delete(entityId) {
        const query = `DELETE {<${entityId}> ?p ?o}
                   WHERE {<${entityId}> ?p ?o}`;
        await this.query(query, true);
    }

    /**
     * @param {string} sparqlQuery
     * @returns {Promise<JSONResult>}
     */
    async queryJSON(sparqlQuery) {
        return this.toJSON(await this.query(sparqlQuery));
    }

    /**
     * Returns JSON representation of SPARQL object; assumes bindings are of type ?subject ?property ?property_value
     * @param {SPARQLResult} responseData
     * @returns {JSONResult} - Array with one or multiple items depending on query
     */
    toJSONSPPV(responseData) {
        const returnJSONMap = {};
        const subject = responseData.head.vars[0];
        const subjectIdProp = `${subject}_id`;
        const sparqlData = responseData.results.bindings;

        sparqlData.forEach(data => {
            const subjectId = data[subject].value;
            if (!returnJSONMap[subjectId]) {
                returnJSONMap[subjectId] = {
                    [subjectIdProp]: subjectId
                };
            }
            const returnJSON = returnJSONMap[subjectId];
            const prop = data['property'].value;
            const propValue = data['property_value'].value;
            if (returnJSON[prop]) {
                if (!Array.isArray(returnJSON[prop])) {
                    returnJSON[prop] = [returnJSON[prop]];
                }
                returnJSON[prop].push(propValue);
                return;
            }
            returnJSON[prop] = propValue;
        });

        return Object.values(returnJSONMap);
    }

    /**
     * Returns JSON representation of SPARQL object; assumes each item from bindings is JSON object
     * @param {SPARQLResult} responseData
     * @returns {JSONResult} - Array with one or multiple items depending on query
     */
    toJSONGeneric(responseData) {
        const returnData = [];
        const sparqlData = responseData.results.bindings;

        sparqlData.forEach(data => {
            const returnO = {};
            for (const key in data) {
                returnO[key] = data[key].value;
            }
            returnData.push(returnO);
        });

        return returnData;
    }

    /**
     * Returns JSON representation of SPARQL object
     * @param {SPARQLResult} responseData
     * @returns {JSONResult} - Array with one or multiple items depending on query
     */
    toJSON(responseData) {
        // if subject property property_value, then we handle it by merging everything
        // otherwise put properties on object as found
        const isSPPVQueryType = responseData.head.vars.length === 3 &&
            responseData.head.vars[1] === 'property' && responseData.head.vars[2] === 'property_value';
        if (isSPPVQueryType) {
            return this.toJSONSPPV(responseData);
        }
        return this.toJSONGeneric(responseData);
    }
}

module.exports = SPARQLQueryDispatcher;
