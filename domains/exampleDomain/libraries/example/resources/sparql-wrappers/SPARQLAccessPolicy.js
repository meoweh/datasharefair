const SPARQLQueryDispatcher = require('../utils/SPARQLQueryDispatcher');
const constants = require('../../config/constants');

class SPARQLAccessPolicy extends SPARQLQueryDispatcher {
    constructor() {
        super(constants.fusekiAccessPoliciesEndpoint);
    }

    /**
     * @typedef {Object} JSONAccessPolicy
     * @property {string} policy_id
     * @property {string} description
     * @property {string} isPartOf
     */

    /**
     * @typedef {Object} SPARQLAccessPolicyO
     * @property {string} policy_id
     * @property {string} description
     * @property {string} isPartOf
     */

    /**
     * @param {JSONResultObject} sparqlJSON
     * @return {JSONAccessPolicy}
     */
    toAccessPolicy(sparqlJSON) {
        console.log(sparqlJSON);
        return {
            policy_id: sparqlJSON['policy_id'],
            description: sparqlJSON['http://purl.org/dc/terms/description'],
            isPartOf: sparqlJSON['http://purl.org/dc/terms/isPartOf']
        };
    }

    /**
     *
     * @param {string} policyId
     * @param {Array<Object<{authorization_id:string}>>} authorizations
     * @returns {SPARQLAccessPolicyO}
     */
    toSPARQL(policyId, authorizations) {
        return {
            policy_id: policyId,
            description: 'This resource has access restriction',
            isPartOf: authorizations.map(auth => `<${auth.authorization_id}>`).join(', ')
        }
    }

    /**
     * @param {SPARQLAccessPolicyO} accessPolicy
     * @return {Promise<void>}
     */
    async create(accessPolicy) {
        const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
                    PREFIX dct: <http://purl.org/dc/terms/>
                    PREFIX acl: <http://www.w3.org/ns/auth/acl#>
                    
                    INSERT DATA {
                        <${accessPolicy.policy_id}> a dct:RightsStatement ;
                        dct:description "${accessPolicy.description}";
                        dct:isPartOf ${accessPolicy.isPartOf} .
                    }`;
        await this.query(query, true);
    }

    /**
     *
     * @param authList
     * @returns {Promise<{policyAuthBind: Object<string, Array<string>>, policiesList: Array<string>, authorizationIds: Array<string>}>}
     */
    async getFairAccessPoliciesForUser(authList) {
        const regexData = authList.join('|');
        // get all partOfs for a policy
        const query = `PREFIX dct: <http://purl.org/dc/terms/>
              SELECT ?policy_id (GROUP_CONCAT(?_partOf; separator=", ") as ?partOf)
              WHERE{
                ?policy_id dct:isPartOf ?_partOf;
                {
                  SELECT ?policy_id WHERE {
                    ?policy_id a dct:RightsStatement;
                    dct:isPartOf ?_partOf ; 
                    FILTER(regex(str(?_partOf), "^.*(${regexData})(, (.)*)*$" ) )
                  }
                }
              }
              GROUP BY ?policy_id`;
        const policies = (await this.queryJSON(query)).map(d => ({
            policy_id: d['policy_id'],
            isPartOf: d['partOf']
        }));
        let allAuthorizationIds = [];
        let policyAuthBind = {};
        policies.forEach((policy) => {
            policyAuthBind[policy.policy_id] = policy.isPartOf.split(', ');
            allAuthorizationIds = allAuthorizationIds.concat(policy.isPartOf.split(', '));
        });
        return {
            policiesList: policies.map(p => p.policy_id),
            authorizationIds: allAuthorizationIds,
            policyAuthBind: policyAuthBind
        };
    }

    async getAuthorizationByPolicy(policyIds) {
        const regexData = policyIds.join('|');

        const query = `PREFIX dct: <http://purl.org/dc/terms/>
              SELECT ?policy_id (GROUP_CONCAT(?_partOf; separator=", ") as ?partOf)
              WHERE{
                ?policy_id dct:isPartOf ?_partOf;
                {
                  SELECT ?policy_id WHERE {
                    ?policy_id a dct:RightsStatement;
                    dct:isPartOf ?_partOf ; 
                    FILTER(regex(str(?policy_id), "^.*(${regexData})(, (.)*)*$" ) )
                  }
                }
              }
              GROUP BY ?policy_id`;

        const policies = (await this.queryJSON(query)).map(d => ({
            policy_id: d['policy_id'],
            isPartOf: d['partOf']
        }));
        let allAuthorizationIds = [];
        let policyAuthBind = {};
        policies.forEach((policy) => {
            policyAuthBind[policy.policy_id] = policy.isPartOf.split(', ');
            allAuthorizationIds = allAuthorizationIds.concat(policy.isPartOf.split(', '));
        });
        return {
            policiesList: policies.map(p => p.policy_id),
            authorizationIds: allAuthorizationIds,
            policyAuthBind: policyAuthBind
        };
    }

    async getPolicyById(policyId){
        const query = `PREFIX dct: <http://purl.org/dc/terms/>
              SELECT ?policy_id (GROUP_CONCAT(?_partOf; separator=", ") as ?partOf)
              WHERE{
                ?policy_id dct:isPartOf ?_partOf;
                {
                  SELECT ?policy_id WHERE {
                    ?policy_id a dct:RightsStatement;
                    dct:isPartOf ?_partOf ; 
                    FILTER(regex(str(?policy_id), "^.*(${policyId})(, (.)*)*$" ) )
                  }
                }
              }
              GROUP BY ?policy_id`;

        return (await this.queryJSON(query)).map(d => ({
            policy_id: d['policy_id'],
            isPartOf: d['partOf'].split(',').map(i => `<${i.trim()}>`).join(','),
            description: 'This resource has access restrictions'
        }));
    }

}

module.exports = exports = new SPARQLAccessPolicy();
