const SPARQLQueryDispatcher = require('../utils/SPARQLQueryDispatcher');
const constants = require('../../config/constants');
const keyword_extractor = require("keyword-extractor");

class SPARQLCatalog extends SPARQLQueryDispatcher {
    constructor() {
        super(constants.fusekiCatalogEndpoint);
    }

    /**
     * @typedef {Object} JSONCatalog
     * @property {string} catalog_id
     * @property {string} title
     * @property {string} description
     * @property {string} language - without lang?
     * @property {string} license
     * @property {string} version
     * @property {string} accessRights
     * @property {Array<string>} categories
     * @property {Array<string>} catalog_keyword
     * @property {Array<string>} datasets
     * @property {string} conformsTo
     * @property {string} publisher
     * @property {string} created - timestamp
     * @property {string} modified - timestamp
     */

    /**
     * @typedef {Object} SPARQLCatalogO
     * @property {string} catalog_id
     * @property {string} accessRights
     * @property {string} conformsTo
     * @property {string} description
     * @property {string} hasVersion
     * @property {string} language - with lang?
     * @property {string} license
     * @property {string} publisher
     * @property {string} title
     * @property {string} metadataIssued - timestamp
     * @property {string} metadataModified - timestamp
     * @property {string} catalog_keyword
     * @property {string} themeTaxonomy
     * @property {Array<string>} dataset
     */

    /**
     * @param {JSONResultObject} sparqlJSON
     * @return {JSONCatalog}
     */
    toCatalog(sparqlJSON) {
        return {
            catalog_id: sparqlJSON['catalog_id'],
            title: sparqlJSON['http://purl.org/dc/terms/title'],
            description: sparqlJSON['http://purl.org/dc/terms/description'],
            language: sparqlJSON['http://purl.org/dc/terms/language'],
            license: sparqlJSON['http://purl.org/dc/terms/license'],
            version: sparqlJSON['http://purl.org/dc/terms/hasVersion'],
            accessRights: sparqlJSON['http://purl.org/dc/terms/accessRights'],
            categories: this.coerceToArray(sparqlJSON['http://www.w3.org/ns/dcat#themeTaxonomy']),
            catalog_keyword: sparqlJSON['http://purl.org/dc/terms/identifier'],
            datasets: this.coerceToArray(sparqlJSON['http://www.w3.org/ns/dcat#dataset'] || []),
            conformsTo: sparqlJSON['http://purl.org/dc/terms/conformsTo'],
            publisher: sparqlJSON['http://purl.org/dc/terms/publisher'],
            created: sparqlJSON['http://rdf.biosemantics.org/ontologies/fdp-o#metadataIssued'],
            modified: sparqlJSON['http://rdf.biosemantics.org/ontologies/fdp-o#metadataModified'],
        };
    }

    /**
     *
     * @param {Object<{catalogName:string, catalogDescription:string, catalogVersion: string, catalogLanguage:string, catalogLicense:string, catalogTheme: Array<string>}>} req
     * @param {Object<{resource_id:string}>} account
     * @returns {SPARQLCatalogO}
     */
    toSPARQLCreate(req, account) {
        let catalogData = {};

        let extraction_result = keyword_extractor.extract(req.catalogName, {
            language: "english",
            remove_digits: true,
            return_changed_case: true,
            remove_duplicates: false
        });
        catalogData.catalog_keyword = `${extraction_result.join('_')}`;
        catalogData.catalog_id = `${constants.fdp_address}` + `catalogs/` + `${catalogData.catalog_keyword}`;
        catalogData.description = req.catalogDescription;
        catalogData.hasVersion = req.catalogVersion;
        catalogData.language = `lang:${req.catalogLanguage}`;
        catalogData.license = req.catalogLicense;
        catalogData.publisher = account.resource_id;
        catalogData.title = req.catalogName;
        catalogData.metadataIdentifier = catalogData.catalog_id;
        const now = Date.now();
        catalogData.metadataIssued = now;
        catalogData.metadataModified = now;
        catalogData.label = req.catalogName;
        catalogData.themeTaxonomy = req.catalogTheme.map(el => `<${el}>`).join(', ');
        catalogData.accessRights = `${catalogData.catalog_id}#accessRights`;
        catalogData.conformsTo = `https://www.purl.org/fairtools/fdp/schema/0.1/catalogMetadata`;
        catalogData.dataset = req.datasets || [];

        return catalogData;
    }

    /**
     *
     * @param {JSONCatalog} req
     * @returns {SPARQLCatalogO}
     */
    toSPARQL(req) {
        let catalogData = {};

        let extraction_result = keyword_extractor.extract(req.title, {
            language: "english",
            remove_digits: true,
            return_changed_case: true,
            remove_duplicates: false
        });
        catalogData.catalog_keyword = `${extraction_result.join('_')}`;
        catalogData.catalog_id = req.catalog_id;
        catalogData.description = req.description;
        catalogData.hasVersion = req.version;
        catalogData.language = `lang:${req.language}`;
        catalogData.license = req.license;
        catalogData.publisher = req.publisher;
        catalogData.title = req.title;
        catalogData.metadataIdentifier = catalogData.catalog_id;
        const now = Date.now();
        catalogData.metadataIssued = req.created;
        catalogData.metadataModified = now;
        catalogData.label = req.title;
        catalogData.themeTaxonomy = req.categories.map(el => `<${el}>`).join(', ');
        catalogData.accessRights = `${catalogData.catalog_id}#accessRights`;
        catalogData.conformsTo = `https://www.purl.org/fairtools/fdp/schema/0.1/catalogMetadata`;
        catalogData.dataset = req.datasets || [];

        return catalogData;
    }

    /**
     * @param {SPARQLCatalogO} catalogData
     * @return {Promise<void>}
     */
    async create(catalogData) {
        const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX dcterms: <http://purl.org/dc/terms/>
                PREFIX fdp: <http://rdf.biosemantics.org/ontologies/fdp-o#>
                PREFIX lang: <http://id.loc.gov/vocabulary/iso639-1/>
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                
                INSERT DATA {
                    <${catalogData.catalog_id}> a dcat:Catalog;
                  dcterms:accessRights <${catalogData.accessRights}>;
                  dcterms:conformsTo <${catalogData.conformsTo}>;
                  dcterms:description "${catalogData.description}";
                  dcterms:hasVersion "${catalogData.hasVersion}";
                  dcterms:isPartOf <${constants.fdp_address}>;
                  dcterms:language ${catalogData.language};
                  dcterms:license <${catalogData.license}>;
                  dcterms:publisher <${catalogData.publisher}>;
                  dcterms:title "${catalogData.title}";
                  fdp:metadataIdentifier <${catalogData.catalog_id}>;
                  fdp:metadataIssued "${catalogData.metadataIssued}"^^xsd:dateTime;
                  fdp:metadataModified "${catalogData.metadataModified}"^^xsd:dateTime;
                  rdfs:label "${catalogData.title}";
                  dcterms:identifier "${catalogData.catalog_keyword}";
                  dcat:themeTaxonomy ${catalogData.themeTaxonomy};
                  ${catalogData.dataset.length ? `dcat:dataset ${catalogData.dataset.map(d => `<${d}>`).join(', ')}` : ''} .                
                  
                <${catalogData.accessRights}> a dcterms:RightsStatement;
                  dcterms:description "This resource has access restriction" .
                }`;

        await this.query(query, true);
    }

    /**
     * @typedef {JSONCatalog} JSONCatalogUI
     * @property {Array<*>} accessRights
     * /

     /**
     * type, mode, agent are in sparql format
     * @param {Array<string>} targetedAccessRights
     * @param {Array<Object<{policy_id: string, authorization_id: string, status: string, entityName: string, type: string, mode: string, agent: string}>>} authData
     * @returns {Promise<Array<JSONCatalogUI>>}
     */
    async getForUser(targetedAccessRights, authData) {
        const regexData = targetedAccessRights.join('|');


        const queryCatalog = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
                        PREFIX dcterms: <http://purl.org/dc/terms/>
                                    
                        SELECT ?catalog ?property ?property_value
                        WHERE {
                           ?catalog a dcat:Catalog ;
                           dcterms:accessRights ?accessRights ;
                           FILTER(regex(str(?accessRights), "^.*(${regexData})(, (.)*)*$" ) ) .
                           ?catalog ?property ?property_value .
                        }`;

        const fusekiData = await this.queryJSON(queryCatalog);
        const catalogs = fusekiData.map(d => this.toCatalog(d));
        return catalogs.map((/*JSONCatalog*/c) => ({
            ...c,
            accessRights: authData.filter(el => el.policy_id === c.accessRights)
        }));
    }

    /**
     * @returns {Promise<Array<{catalog_id: string, title: string}>>}
     */
    async getSimple() {
        const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            SELECT *
            WHERE {
               ?catalog a dcat:Catalog ;
               dcterms:title ?title.
            }`;
        const result = await this.queryJSON(query);
        return result.map(r => ({
            catalog_id: r['catalog'],
            title: r['title']
        }));
    }

    /**
     *
     * @param {Object<{catalog: string, dataset_id: string}>} datasetData
     * @returns {Promise<void>}
     */
    async updateCatalogDatasets(datasetData) {
        const getCatalogDatasetsQuery = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
            SELECT ?dataset
            WHERE {
                <${datasetData.catalog}> <http://www.w3.org/ns/dcat#dataset> ?dataset;
                dcat:dataset ?dataset.
            }`;


        const currentDatasets = (await this.queryJSON(getCatalogDatasetsQuery)).map(d => d.dataset);
        const newDatasets = [...currentDatasets, datasetData.dataset_id]
            .map(d => `<${d}>`).join(', ');

        const updateQuery = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
            INSERT DATA
            {
               <${datasetData.catalog}> a dcat:Catalog ;
               dcat:dataset ${newDatasets} .
            }`;


        await this.query(updateQuery, true);
    }

}

module.exports = exports = new SPARQLCatalog();
