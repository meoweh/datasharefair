const SPARQLQueryDispatcher = require('../utils/SPARQLQueryDispatcher');
const constants = require('../../config/constants');

exports.getFairDistributions = async (dataset) => {

        const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
            PREFIX dcterms: <http://purl.org/dc/terms/>
                                                
            SELECT ?distribution ?property ?property_value
            WHERE {
                   ?distribution a dcat:Distribution ;
                   dcterms:isPartOf <${dataset.dataset_id}> .
                   ?distribution ?property ?property_value .
            }`;
        const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiDistributionQueryEndpoint);
        const distributions = await queryDispatcher.queryJSON(query);
        let policyIds = [];
        for(let distribution of distributions){
            policyIds.push(distribution['http://purl.org/dc/terms/accessRights']);
        }
        return {fairDistributions: distributions, policyIds}

};

exports.getFairAccessPolicyForDistribution = async function(distribution_id) {

    const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
            PREFIX dcterms: <http://purl.org/dc/terms/>
                                                
            SELECT ?distribution ?accessRights
            WHERE {
                   ?distribution a dcat:Distribution ;
                   dcterms:accessRights ?accessRights .
                   FILTER(regex(str(?distribution), "^.*(${distribution_id})(, (.)*)*$" ) )

            }`;

    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiDistributionQueryEndpoint);
    const resp = await queryDispatcher.query(query);
    return resp.results.bindings[0].accessRights.value;
};

exports.createFairDistribution = async (distributionData) => {
    const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#> 
                PREFIX dcterms: <http://purl.org/dc/terms/> 
                PREFIX fdp: <http://rdf.biosemantics.org/ontologies/fdp-o#> 
                PREFIX lang: <http://id.loc.gov/vocabulary/iso639-1/> 
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
                
                INSERT DATA {
                  
                <${distributionData.distribution_id}> a dcat:Distribution;
                  dcterms:accessRights <${distributionData.accessRights}>;
                  dcterms:conformsTo <${distributionData.conformsTo}>;
                  dcterms:description "${distributionData.description}";
                  dcterms:hasVersion "${distributionData.hasVersion}";
                  dcterms:isPartOf <${distributionData.isPartOf}>;
                  dcterms:language ${distributionData.language};
                  dcterms:license <${distributionData.license}>;
                  dcterms:publisher <${distributionData.publisher}>;
                  dcterms:title "${distributionData.title}";
                  fdp:metadataIdentifier <${distributionData.metadataIdentifier}>;
                  fdp:metadataIssued "${distributionData.metadataIssued}"^^xsd:dateTime;
                  fdp:metadataModified "${distributionData.metadataModified}"^^xsd:dateTime;
                  rdfs:label "${distributionData.label}";
                  dcat:downloadURL <${distributionData.downloadURL}>;
                  dcat:mediaType "${distributionData.mediaType}" .
                
                }`;

    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiDistributionUpdateEndpoint);
    await queryDispatcher.query(query);
};

exports.getDistForAccessPolicy = async (targetedAccessRights) => {
    const regexData = targetedAccessRights.join('|');


    const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
                        PREFIX dcterms: <http://purl.org/dc/terms/>
                                    
                        SELECT ?distribution
                        WHERE {
                           ?distribution a dcat:Distribution ;
                            dcterms:accessRights ?accessRights .
                            FILTER(regex(str(?accessRights), "^.*(${regexData})(, (.)*)*$" ) ) .

                        }`;
    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiDistributionQueryEndpoint);

    return await queryDispatcher.queryJSON(query);

};

exports.deleteFairDistribution = async function (distributionId){
    const query = `DELETE {<${distributionId}> ?p ?o}
                   WHERE {<${distributionId}> ?p ?o}`;
    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiDistributionUpdateEndpoint);
    await queryDispatcher.query(query);
};
