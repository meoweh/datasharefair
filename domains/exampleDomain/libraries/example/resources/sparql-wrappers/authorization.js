const SPARQLQueryDispatcher = require('../utils/SPARQLQueryDispatcher');
const constants = require('../../config/constants');


async function createFairAuthorization(authorization) {

    const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
        PREFIX dct: <http://purl.org/dc/terms/>
        PREFIX acl: <http://www.w3.org/ns/auth/acl#>
        
        INSERT DATA {
             <${authorization.authorization_id}> a acl:Authorization;
                acl:mode ${authorization.mode};
                acl:agent <${authorization.agent}> ;
        }`;

    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiAuthorizationsUpdateEndpoint);
    await queryDispatcher.query(query);
}

exports.deleteFairAuthorization = async function (authorizationId){
    const query = `DELETE {<${authorizationId}> ?p ?o}
                   WHERE {<${authorizationId}> ?p ?o}`;
    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiAuthorizationsUpdateEndpoint);
    await queryDispatcher.query(query);
};


exports.createFairAuthorizations = async function (authorizations) {
    for(const auth of authorizations){
        await createFairAuthorization(auth);
    }
};


exports.getFairAuthorizationsForUserWriteControl = async function(user){

    const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
            PREFIX dct: <http://purl.org/dc/terms/>
            PREFIX acl: <http://www.w3.org/ns/auth/acl#>
                    
            SELECT * WHERE {
              {
                ?authorization_id a acl:Authorization;
                  acl:mode acl:Control;
                  acl:agent <${user}> .
                  ?authorization_id ?property ?property_value . 
              }
              UNION
              {
                ?authorization_id a acl:Authorization;
                  acl:mode acl:Write;
                  acl:agent <${user}> . 
                  ?authorization_id ?property ?property_value .
              }
            }`;

    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiAuthorizationsQueryEndpoint);
    const authorizations = await queryDispatcher.query(query.split('\n').join(''));

    let authGroupedData = {};

    authorizations.results.bindings.forEach((propValue) => {
        if(!authGroupedData.hasOwnProperty(propValue.authorization_id.value)){
            authGroupedData[propValue.authorization_id.value] = {};
            authGroupedData[propValue.authorization_id.value]['authorization_id'] = propValue.authorization_id.value;
        }
        authGroupedData[propValue.authorization_id.value][propValue.property.value] = propValue.property_value.value;
    });

    return {authList: authorizations.results.bindings.map(auth => auth.authorization_id.value), authData: Object.values(authGroupedData)};
};

exports.getFairAuthorizationsWithId = async function(authorizationIds, policyAuthBind){
    const regexData = authorizationIds.join('|');

    const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
            PREFIX dct: <http://purl.org/dc/terms/>
            PREFIX acl: <http://www.w3.org/ns/auth/acl#>
                    
            SELECT * WHERE {
                ?authorization_id a acl:Authorization .
                 ?authorization_id ?property ?property_value . 
            FILTER(regex(str(?authorization_id), "^.*(${regexData})(, (.)*)*$") )
            }`;

    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiAuthorizationsQueryEndpoint);
    const authorizations = await queryDispatcher.query(query.split('\n').join(''));

    let authGroupedData = {};

    authorizations.results.bindings.forEach((propValue) => {
        if(!authGroupedData.hasOwnProperty(propValue.authorization_id.value)){
            authGroupedData[propValue.authorization_id.value] = {};
            authGroupedData[propValue.authorization_id.value]['authorization_id'] = propValue.authorization_id.value;
        }
        authGroupedData[propValue.authorization_id.value]['policy_id'] = '';
        for(const key of Object.keys(policyAuthBind)){
            if(policyAuthBind[key].includes(propValue.authorization_id.value)){
                authGroupedData[propValue.authorization_id.value]['policy_id'] = key;
            }
        }
        authGroupedData[propValue.authorization_id.value]['status'] = 'Approved';
        authGroupedData[propValue.authorization_id.value][propValue.property.value] = propValue.property_value.value;
    });
    return Object.values(authGroupedData);
};
