const SPARQLQueryDispatcher = require('../utils/SPARQLQueryDispatcher');
const constants = require('../../config/constants');

// exports.createFairAccessPolicy = async function (accessPolicy) {
//     const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
//                     PREFIX dct: <http://purl.org/dc/terms/>
//                     PREFIX acl: <http://www.w3.org/ns/auth/acl#>
//
//                     INSERT DATA {
//                         <${accessPolicy.policy_id}> a dct:RightsStatement ;
//                         dct:description "${accessPolicy.description}";
//                         dct:isPartOf ${accessPolicy.isPartOf} .
//                     }`;
//     const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiAccessPoliciesUpdateEndpoint);
//     await queryDispatcher.query(query);
// };

// exports.deleteFairAccessPolicy = async function (accessPolicyId){
//     const query = `DELETE {<${accessPolicyId}> ?p ?o}
//                    WHERE {<${accessPolicyId}> ?p ?o}`;
//     const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiAccessPoliciesUpdateEndpoint);
//     await queryDispatcher.query(query);
// };

// exports.getFairAccessPoliciesForUser = async function(authList){
//     const regexData = authList.join('|');
//     // get all partOfs for a policy
//     const query = `PREFIX dct: <http://purl.org/dc/terms/>
//               SELECT ?policy_id (GROUP_CONCAT(?_partOf; separator=", ") as ?partOf)
//               WHERE{
//                 ?policy_id dct:isPartOf ?_partOf;
//                 {
//                   SELECT ?policy_id WHERE {
//                     ?policy_id a dct:RightsStatement;
//                     dct:isPartOf ?_partOf ;
//                     FILTER(regex(str(?_partOf), "^.*(${regexData})(, (.)*)*$" ) )
//                   }
//                 }
//               }
//               GROUP BY ?policy_id`;
//     // const query = `PREFIX dct: <http://purl.org/dc/terms/>
//     //         SELECT * WHERE {
//     //           ?policy_id a dct:RightsStatement;
//     //            dct:isPartOf ?partOf ;
//     //            FILTER(regex(str(?partOf), "^.*(${regexData})(, (.)*)*$" ) )
//     //         }`;
//     const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiAccessPoliciesQueryEndpoint);
//     const policies = await queryDispatcher.query(query);
//     let allAuthorizationIds = [];
//     let policyAuthBind = {};
//     policies.results.bindings.forEach((policy) => {
//         policyAuthBind[policy.policy_id.value] = policy.partOf.value.split(', ');
//         allAuthorizationIds.concat(policy.partOf.value.split(', '));
//     });
//     return {policiesList: policies.results.bindings.map(r => r.policy_id.value), authorizationIds: allAuthorizationIds, policyAuthBind: policyAuthBind};
// };
