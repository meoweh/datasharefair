const SPARQLQueryDispatcher = require('../utils/SPARQLQueryDispatcher');
const constants = require('../../config/constants');


exports.createFairCatalog = async function (catalogData) {
    const queryCatalog = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX dcterms: <http://purl.org/dc/terms/>
                PREFIX fdp: <http://rdf.biosemantics.org/ontologies/fdp-o#>
                PREFIX lang: <http://id.loc.gov/vocabulary/iso639-1/>
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                
                INSERT DATA {
                    <${catalogData.catalog_id}> a dcat:Catalog;
                  dcterms:accessRights <${catalogData.accessRights}>;
                  dcterms:conformsTo <${catalogData.conformsTo}>;
                  dcterms:description "${catalogData.description}";
                  dcterms:hasVersion "${catalogData.hasVersion}";
                  dcterms:isPartOf <${constants.fdp_address}>;
                  dcterms:language ${catalogData.language};
                  dcterms:license <${catalogData.license}>;
                  dcterms:publisher <${catalogData.publisher}>;
                  dcterms:title "${catalogData.title}";
                  fdp:metadataIdentifier <${catalogData.catalog_id}>;
                  fdp:metadataIssued "${catalogData.metadataIssued}"^^xsd:dateTime;
                  fdp:metadataModified "${catalogData.metadataModified}"^^xsd:dateTime;
                  rdfs:label "${catalogData.title}";
                  dcterms:identifier "${catalogData.catalog_keyword}";
                  dcat:themeTaxonomy ${catalogData.themeTaxonomy} .                
                  
                <${catalogData.accessRights}> a dcterms:RightsStatement;
                  dcterms:description "This resource has access restriction" .
}`;

    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiCatalogUpdateEndpoint);
    await queryDispatcher.query(queryCatalog);
};

exports.deleteFairCatalog = async function (catalogId){
    const query = `DELETE {<${catalogId}> ?p ?o}
                   WHERE {<${catalogId}> ?p ?o}`;
    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiCatalogUpdateEndpoint);
    await queryDispatcher.query(query);
};


exports.getFairCatalogsForUser = async function (targetedAccessRights, authData) {
    const regexData = targetedAccessRights.join('|');


    const queryCatalog = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
                        PREFIX dcterms: <http://purl.org/dc/terms/>
                                    
                        SELECT ?catalog ?property ?property_value
                        WHERE {
                           ?catalog a dcat:Catalog ;
                           dcterms:accessRights ?accessRights ;
                           FILTER(regex(str(?accessRights), "^.*(${regexData})(, (.)*)*$" ) ) .
                           ?catalog ?property ?property_value .
                        }`;
    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiCatalogQueryEndpoint);

    const fusekiData = await queryDispatcher.query(queryCatalog);

    let fusekiGroupedData = {};

    fusekiData.results.bindings.forEach((propValue) => {
        if (!fusekiGroupedData.hasOwnProperty(propValue.catalog.value)) {
            fusekiGroupedData[propValue.catalog.value] = {};
            fusekiGroupedData[propValue.catalog.value]['catalog_id'] = propValue.catalog.value;
        }
        if (propValue.property.value === 'http://purl.org/dc/terms/accessRights') {
            fusekiGroupedData[propValue.catalog.value][propValue.property.value] = authData.filter(el => el.policy_id === propValue.property_value.value);
        } else if (propValue.property.value === 'http://www.w3.org/ns/dcat#dataset' || propValue.property.value === 'http://www.w3.org/ns/dcat#themeTaxonomy') {
            if(!fusekiGroupedData[propValue.catalog.value][propValue.property.value]){
                fusekiGroupedData[propValue.catalog.value][propValue.property.value] = [];
            }
            fusekiGroupedData[propValue.catalog.value][propValue.property.value].push(propValue.property_value.value);
        } else fusekiGroupedData[propValue.catalog.value][propValue.property.value] = propValue.property_value.value;
    });

    return Object.values(fusekiGroupedData);
};

exports.updateCatalogWithDataset = async (datasetData) => {

    const getCatalogDatasetsQuery = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX fdp: <http://rdf.biosemantics.org/ontologies/fdp-o#>
            PREFIX lang: <http://id.loc.gov/vocabulary/iso639-1/>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

            SELECT ?datasets
            WHERE {
               ?catalog a dcat:Catalog ;
               dcat:dataset ?datasets.
            }`;

    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiCatalogQueryEndpoint);
    const fusekiData = await queryDispatcher.query(getCatalogDatasetsQuery);

    let dataset = '';
    fusekiData.results.bindings.forEach((item) => {
        dataset += `<${item.dataset.value}>, `;
    });

    const updateQuery = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX fdp: <http://rdf.biosemantics.org/ontologies/fdp-o#>
            PREFIX lang: <http://id.loc.gov/vocabulary/iso639-1/>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            INSERT DATA
            {
               <${datasetData.catalog}> a dcat:Catalog ;
               dcat:dataset ${dataset}<${datasetData.dataset_id}> .
            }`;

    const updateDispatcher = new SPARQLQueryDispatcher(constants.fusekiCatalogUpdateEndpoint);
    await updateDispatcher.query(updateQuery);
};

exports.getFairCatalogsNameId = async () => {
    const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            SELECT *
            WHERE {
               ?catalog a dcat:Catalog ;
               dcterms:title ?title.
            }`;
    const updateDispatcher = new SPARQLQueryDispatcher(constants.fusekiCatalogQueryEndpoint);
    const result = await updateDispatcher.query(query);
    return result.results.bindings.map((el) =>{
       return {
           catalog_id: el.catalog.value,
           title: el.title.value
       }
    });
};
