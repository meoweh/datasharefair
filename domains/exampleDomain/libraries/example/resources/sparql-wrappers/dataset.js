const SPARQLQueryDispatcher = require('../utils/SPARQLQueryDispatcher');
const constants = require('../../config/constants');

exports.createFairDataset = async (datasetData) => {

    const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX fdp: <http://rdf.biosemantics.org/ontologies/fdp-o#>
            PREFIX lang: <http://id.loc.gov/vocabulary/iso639-1/>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                            
            INSERT DATA {
                  <${datasetData.dataset_id}> a dcat:Dataset;
              dcterms:accessRights <${datasetData.accessRights}>;
              dcterms:conformsTo <${datasetData.conformsTo}>;
              dcterms:description "${datasetData.description}";
              dcterms:hasVersion "${datasetData.hasVersion}";
              dcterms:isPartOf <${datasetData.catalog}>;
              dcterms:language ${datasetData.language};
              dcterms:license <${datasetData.license}>;
              dcterms:publisher <${datasetData.publisher}>;
              dcterms:title "${datasetData.title}";
              fdp:metadataIdentifier <${datasetData.metadataIdentifier}>;
              fdp:metadataIssued "${datasetData.metadataIssued}"^^xsd:dateTime;
              fdp:metadataModified "${datasetData.metadataModified}"^^xsd:dateTime;
              rdfs:label "${datasetData.label}";
              dcat:keyword ${datasetData.keyword};
              dcat:theme ${datasetData.themeTaxonomy} .            
            }`;

    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiDatasetUpdateEndpoint);
    await queryDispatcher.query(query);
};

exports.deleteFairDataset = async function (datasetId) {
    const query = `DELETE {<${datasetId}> ?p ?o}
                   WHERE {<${datasetId}> ?p ?o}`;
    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiDatasetUpdateEndpoint);
    await queryDispatcher.query(query);
};

exports.getFairDatasetsForUser = async (targetedAccessRights, authData) => {
    const regexData = targetedAccessRights.join('|');


    const queryCatalog = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
                        PREFIX dcterms: <http://purl.org/dc/terms/>
                                    
                        SELECT ?dataset ?property ?property_value
                        WHERE {
                           ?dataset a dcat:Dataset ;
                           dcterms:accessRights ?accessRights ;
                           FILTER(regex(str(?accessRights), "^.*(${regexData})(, (.)*)*$" ) ) .
                           ?dataset ?property ?property_value .
                        }`;
    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiDatasetQueryEndpoint);

    const fusekiData = await queryDispatcher.query(queryCatalog);

    let fusekiGroupedData = {};

    fusekiData.results.bindings.forEach((propValue) => {
        if (!fusekiGroupedData.hasOwnProperty(propValue.dataset.value)) {
            fusekiGroupedData[propValue.dataset.value] = {};
            fusekiGroupedData[propValue.dataset.value]['dataset_id'] = propValue.dataset.value;
        }
        if (propValue.property.value === 'http://purl.org/dc/terms/accessRights') {
            fusekiGroupedData[propValue.dataset.value][propValue.property.value] = authData.filter(el => el.policy_id === propValue.property_value.value);
        } else if (propValue.property.value === 'http://www.w3.org/ns/dcat#keyword' || propValue.property.value === 'http://www.w3.org/ns/dcat#theme' || propValue.property.value === 'http://www.w3.org/ns/dcat#distribution') {
            if (!fusekiGroupedData[propValue.dataset.value][propValue.property.value]) {
                fusekiGroupedData[propValue.dataset.value][propValue.property.value] = [];
            }
            fusekiGroupedData[propValue.dataset.value][propValue.property.value].push(propValue.property_value.value);
        } else fusekiGroupedData[propValue.dataset.value][propValue.property.value] = propValue.property_value.value;
    });

    return Object.values(fusekiGroupedData);
};

exports.getFairDatasets = async () => {
    const queryCatalog = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
                        PREFIX dcterms: <http://purl.org/dc/terms/>
                                    
                        SELECT ?dataset ?property ?property_value
                        WHERE {
                           ?dataset a dcat:Dataset .
                           ?dataset ?property ?property_value .
                        }`;
    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiDatasetQueryEndpoint);

    const fusekiData = await queryDispatcher.query(queryCatalog);

    let fusekiGroupedData = {};
    let authorizationIds = [];

    fusekiData.results.bindings.forEach((propValue) => {
        if (!fusekiGroupedData.hasOwnProperty(propValue.dataset.value)) {
            fusekiGroupedData[propValue.dataset.value] = {};
            fusekiGroupedData[propValue.dataset.value]['dataset_id'] = propValue.dataset.value;
        }
        if (propValue.property.value === 'http://purl.org/dc/terms/accessRights') {
            fusekiGroupedData[propValue.dataset.value][propValue.property.value] = propValue.property_value.value;
            authorizationIds.push(propValue.property_value.value);
        } else if (propValue.property.value === 'http://www.w3.org/ns/dcat#keyword' || propValue.property.value === 'http://www.w3.org/ns/dcat#theme') {
            if (!fusekiGroupedData[propValue.dataset.value][propValue.property.value]) {
                fusekiGroupedData[propValue.dataset.value][propValue.property.value] = [];
            }
            fusekiGroupedData[propValue.dataset.value][propValue.property.value].push(propValue.property_value.value);
        } else fusekiGroupedData[propValue.dataset.value][propValue.property.value] = propValue.property_value.value;
    });

    return {fairDatasets: Object.values(fusekiGroupedData), authorizationIds};
};

exports.getFairCategories = async () => {
    const query = `PREFIX dcat: <http://www.w3.org/ns/dcat#>                                         
            SELECT * WHERE {                    
              ?dataset_id a dcat:Dataset;
              dcat:theme ?themes .
            }`;

    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiDatasetQueryEndpoint);
    const fusekiData = await queryDispatcher.query(query);

    return fusekiData.results.bindings.map((item) => item.themes.value);
};

exports.updateDatasetWithDistribution = async (distributionData) => {
    const getDatasetDistributionsQuery = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
            SELECT ?distribution
            WHERE {
                <${distributionData.dataset}> <http://www.w3.org/ns/dcat#distribution> ?distribution;
                dcat:distribution ?distribution.
            }`;

    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiDatasetQueryEndpoint);
    const currentDistributions = (await queryDispatcher.queryJSON(getDatasetDistributionsQuery)).map(d => d.distribution);
    const newDistributions = [...currentDistributions, distributionData.distribution_id]
        .map(d => `<${d}>`).join(', ');

    const updateQuery = `PREFIX dcat: <http://www.w3.org/ns/dcat#>
            INSERT DATA
            {
               <${distributionData.dataset}> a dcat:Dataset ;
               dcat:distribution ${newDistributions} .
            }`;

    const updateDispatcher = new SPARQLQueryDispatcher(constants.fusekiDatasetUpdateEndpoint);
    await updateDispatcher.query(updateQuery);
};

exports.getFairCatalogIdForDataset = async (dataset_id) => {

    const query = `PREFIX dcterms: <http://purl.org/dc/terms/>
                                                
            SELECT ?catalog
            WHERE {
                   <${dataset_id}> dcterms:isPartOf ?catalog
            }`;
    const queryDispatcher = new SPARQLQueryDispatcher(constants.fusekiDatasetQueryEndpoint);
    return (await queryDispatcher.queryJSON(query))[0].catalog;

};
