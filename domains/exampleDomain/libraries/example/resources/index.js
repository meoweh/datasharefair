module.exports = $$.library(function(){
    require("../assets/Account.js");
    require("../assets/AccessPolicy.js");
    require("../assets/Authorization.js");
    require("../assets/Session.js");
    require("../assets/Group.js");
    require("../assets/Catalog.js");
    require("../assets/Dataset.js");
    require("../assets/Distribution.js");

    require("./transactions/account.js");
    require("./transactions/session.js");
    require("./transactions/group.js");
    require("./transactions/catalog.js");
    require("./transactions/dataset.js");
    require("./transactions/distribution.js");
    require("./transactions/getFairResources.js");
});
