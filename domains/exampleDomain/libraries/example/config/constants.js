const myIp = `${process.env.HOST_ADDR}`;

const jenaIP = `${process.env.HOST_ADDR}`;

console.log(process.env.HOST_ADDR);


module.exports = {
    fdp_address: `http://${myIp}:8087/fdp/`,
    wikidataEndpoint: `https://query.wikidata.org/sparql`,

    fusekiCatalogUpdateEndpoint: `http://${jenaIP}:3030/CatalogMetadata/update`,
    fusekiCatalogQueryEndpoint: `http://${jenaIP}:3030/CatalogMetadata/sparql`,
    fusekiCatalogEndpoint: `http://${jenaIP}:3030/CatalogMetadata`,

    fusekiDatasetQueryEndpoint: `http://${jenaIP}:3030/DatasetMetadata/sparql`,
    fusekiDatasetUpdateEndpoint: `http://${jenaIP}:3030/DatasetMetadata/update`,

    fusekiDistributionQueryEndpoint: `http://${jenaIP}:3030/DistributionMetadata/sparql`,
    fusekiDistributionUpdateEndpoint: `http://${jenaIP}:3030/DistributionMetadata/update`,

    fusekiOrganizationQueryEndpoint: `http://${jenaIP}:3030/Organizations/sparql`,
    fusekiOrganizationUpdateEndpoint: `http://${jenaIP}:3030/Organizations/update`,

    fusekiAuthorizationsQueryEndpoint: `http://${jenaIP}:3030/Authorizations/sparql`,
    fusekiAuthorizationsUpdateEndpoint: `http://${jenaIP}:3030/Authorizations/update`,

    fusekiAccessPoliciesQueryEndpoint: `http://${jenaIP}:3030/AccessPolicies/sparql`,
    fusekiAccessPoliciesUpdateEndpoint: `http://${jenaIP}:3030/AccessPolicies/update`,
    fusekiAccessPoliciesEndpoint: `http://${jenaIP}:3030/AccessPolicies`
};
