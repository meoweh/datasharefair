// $$.swarm.describe("Echo", {
//    say: function(message){
//        this.return(null, "Echo:"+message);
//    }
// });
if (typeof $$ !== "undefined" && typeof $$.blockchain === "undefined") {
    const pskDB = require("pskdb");
    pskDB.startDB("./datasharefair_db");
}

let prefix = "..";

//temporary fix
//cwd and point of reference is different when loaded in domain vs agent
if (process.cwd().indexOf("engine") === -1) {
    prefix = "code";
}

$$.loadLibrary("example", require("../example/resources/index.js"));
