
const config = require('../config/constants');

$$.asset.describe('Dataset', {
    public: {
        alias: 'string',
        dataset_id: 'string:alias',
        catalog: 'string',
        categories: 'array', // aka themes
        created: 'string',
        description: 'string',
        keywords: 'string',
        language: 'string',
        license: 'string',
        modified: 'string',
        publisher: 'string',
        title: 'string',
        version: 'string',
        accessRights: 'string' //se va crea un policy unde se pun defapt toate entitatile care vin in ACL
    },
    create: function (req) {
        this.alias = req.dataset_id;
        this.dataset_id = req.dataset_id;
        this.catalog = req.catalog;
        this.themeTaxonomy = req.themeTaxonomy;
        this.metadataIssued = req.metadataIssued;
        this.description = req.description;
        this.keyword = req.keyword;
        this.language = req.language;
        this.license = req.license;
        this.metadataModified = req.metadataModified;
        this.publisher = req.publisher;
        this.title = req.title;
        this.hasVersion = req.hasVersion;
        this.accessRights = req.accessRights;
        return true;
    },
    updateDat: function (req) {
        for (let key of Object.keys(req)) {
            console.log(`Key: ${key}`);
            if (!['dataset_id', 'catalog', 'publisher', 'user_token'].includes(key)) {
                this[key] = req[key];
            }
        }
        return true;
    }
});
