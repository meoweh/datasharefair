const config = require('../config/constants');
const crypto = require('crypto');
const secret = 'mysecretpass';

$$.asset.describe('Account', {
    public: {
        alias: 'string',
        company_id: 'string:alias',
        resource_id: 'string', // url semantic: ip + companyID
        region: 'string', //hash
        name: 'string',
        email: 'string',
        address: 'string',
        category: 'string',
        certificate: 'string'
    },
    create: function (req) {
        this.company_id = req.company_id;
        this.alias = req.company_id;
        this.resource_id = config.fdp_address + req.company_id;
        this.region = req.region;
        this.name = req.name;
        this.email = req.email;
        this.address = req.address;
        this.category = req.category;
        this.certificate = crypto.createHmac('sha256', secret).update(req.certificate).digest('hex');
        return true;
    },
    validateCredentials: function (req) {
        console.log('Found company id: ' + this.company_id);
        if (req.company_id !== this.company_id)
            return false;
        const hash = crypto.createHmac('sha256', secret).update(req.certificate).digest('hex');
        console.log('hashed pass: ' + hash);
        if (hash !== this.certificate)
            return false;
        return true;
    },
    updateAcc: function (req) {
        for(let key of Object.keys(req)){
            console.log(`Key: ${key}`);
            if(!['company_id', 'alias', 'resource_id', 'user_token'].includes(key)){
                this[key] = req[key];
            }
        }
        return {
            company_id: this.company_id,
            resource_id: this.resource_id,
            region: this.region,
            name: this.name,
            email: this.email,
            address: this.address,
            category: this.category
        };
    },
    deleteAcc: function () {
        this.region = '';
        this.name = '';
        this.email = '';
        this.address = '';
        this.category = '';
        this.certificate = '';
        return true;
    },
    get: function () {
        return {
            company_id: this.company_id,
            resource_id: this.resource_id,
            region: this.region,
            name: this.name,
            email: this.email,
            address: this.address,
            category: this.category
        }
    }
});
