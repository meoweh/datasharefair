const config = require('../config/constants');

$$.asset.describe('Group', {
    public: {
        alias: 'string',
        group_id: 'string:alias',
        resource_id: 'string', // url semantic: ip + groupID
        group_name: 'string',
        description: 'string',
        creator: 'string',
        members: 'array'
    },
    create: function (req) {
        this.group_id = req.group_id;
        this.alias = req.group_id;
        this.resource_id = config.fdp_address + req.group_id;
        this.group_name = req.group_name;
        this.description = req.description;
        this.creator = req.creator;
        this.members = req.members;
        return true;
    },
    addMember: function (company_id) {
        this.members.push(company_id);
        return true;
    },
    removeMember: function (company_id) {
        const memberIdx = this.members.indexOf(company_id);
        if(memberIdx > -1){
            this.members.splice(memberIdx, 1);
        }
        return true;
    },
    deleteGroup: function () {
        this.group_name = '';
        this.description = '';
        this.creator = '';
        this.members = [];
        return true;
    },
    get: function () {
        return {
            group_id: this.group_id,
            resource_id: this.resource_id,
            group_name: this.group_name,
            description: this.description,
            creator: this.creator,
            members: this.members
        }
    }
});
