$$.asset.describe('AccessPolicy', {
    public: {
        alias: 'string',
        policy_id: 'string:alias',
        description: 'string',
        isPartOf: 'string',
        removed: 'boolean',
    },
    create: function (req) {
        this.policy_id = req.policy_id;
        this.alias = req.policy_id;
        this.description = req.description;
        this.isPartOf = req.isPartOf;
        return true;
    },
    updateAccessPolicy: function (req) {
        for (let key of Object.keys(req)) {
            console.log(`Key: ${key}`);
            if (!['policy_id', 'alias'].includes(key)) {
                this[key] = req[key];
            }
        }
        return true;
    },
    remove: function () {
        this.removed = true;
        return true;
    }
});
