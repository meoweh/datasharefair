$$.asset.describe('Catalog', {
    public: {
        alias: 'string',
        catalog_id: 'string:alias',
        conformsTo: 'string',
        description: 'string',
        hasVersion: 'string',
        isPartOf: 'string',
        language: 'string',
        license: 'string',
        publisher: 'string',
        title: 'string',
        metadataIdentifier: 'string',
        metadataIssued: 'string',
        metadataModified: 'string',
        label: 'string',
        themeTaxonomy: 'string',
        accessRights: 'string', //se va crea un policy unde se pun defapt toate entitatile care vin in ACL
        identifier: 'string'
    },
    create: function (req) {
        this.catalog_id = req.catalog_id;
        this.alias = req.company_id;
        this.conformsTo = req.conformsTo;
        this.description = req.description;
        this.hasVersion = req.hasVersion;
        this.description = req.description;
        this.isPartOf = req.isPartOf;
        this.language = req.language;
        this.license = req.license;
        this.publisher = req.publisher;
        this.title = req.title;
        this.metadataIdentifier = req.metadataIdentifier;
        this.metadataIssued = req.metadataIssued;
        this.metadataModified = req.metadataModified;
        this.label = req.label;
        this.themeTaxonomy = req.themeTaxonomy;
        this.accessRights = req.accessRights;
        this.identifier = req.identifier;
        return true;
    },
    updateCat: function (req) {
        for (let key of Object.keys(req)) {
            console.log(`Key: ${key}`);
            if (!['catalog_id', 'alias', 'conformsTo', 'isPartOf', 'publisher', 'user_token', 'metadataIssued', 'identifier'].includes(key)) {
                this[key] = req[key];
            }
        }
        return true;
    }
});
