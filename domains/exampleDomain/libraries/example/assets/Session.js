const moment = require('moment');

$$.asset.describe("Session", {
    public: {
        alias: "string",
        company_id: "string",
        user_token: "string:alias",
        expiry: "number"
    },
    add: function (company_id, user_token) {
        this.company_id = company_id;
        this.alias = user_token;
        this.user_token = user_token;
        this.expiry = moment().add(2, 'h').unix();
        return true;
    },
    isValid: function () {
        if (this.expiry < moment().unix())
            return false;
        return true;
    },
    remove: function () {
        this.expiry = moment().unix();
        return true;
    },
    get: function () {
        return {
            company_id: this.company_id,
            user_token: this.user_token,
            expiry: this.expiry
        }
    }
});
