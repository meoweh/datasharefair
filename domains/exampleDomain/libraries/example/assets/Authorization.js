$$.asset.describe('Authorization', {
    public: {
        alias: 'string',
        authorization_id: 'string:alias', //id-ul se pune la isPartOf din AccessPolicy
        mode: 'string', //read, write, control
        agent: 'string', // cine primeste dreptul, un resource_id de account
        removed: 'boolean',
        agent_type: 'string',
        status: 'string',
        distribution_id: 'string'
    },
    create: function (req) {
        this.authorization_id = req.authorization_id;
        this.alias = req.authorization_id;
        this.mode = req.mode;
        this.agent = req.agent;
        this.status = req.status;
        this.removed = false;
        this.distribution_id = '';
        if(req.distribution_id){
            this.distribution_id = req.distribution_id;
        }
        if(req.agent_type){
            this.agent_type = req.agent_type;
        }
        return true;
    },
    updateAut: function(data){
        this.status = 'approved';
        return true;
    },
    remove: function () {
        this.removed = true;
        return true;
    },
    get: function () {
        let obj =  {
            authorization_id: this.authorization_id,
            mode: this.mode,
            agent: this.agent,
            status: this.status,
            removed: this.removed,
            distribution_id: this.distribution_id
        };

        if(this.agent_type){
            obj.agent_type = this.agent_type;
        }

        return obj;

    }
});
