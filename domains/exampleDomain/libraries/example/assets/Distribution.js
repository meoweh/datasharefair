const config = require('../config/constants');

$$.asset.describe('Distribution', {
    public: {
        alias: 'string',
        distribution_id: 'string:alias',
        accessRights: 'string', //se va crea un policy unde se pun defapt toate entitatile care vin in ACL
        description: 'string',
        version: 'string',
        dataset: 'string',
        language: 'string',
        license: 'string',
        publisher: 'string',
        title: 'string',
        label: 'string',
        url: 'string',
        mediaType: 'string',
        created: 'string',
        modified: 'string',
        removed: 'boolean',
        encryptionKey: 'string'
    },
    create: function (req) {
        this.distribution_id = req.distribution_id;
        this.alias = req.distribution_id;
        this.accessRights = req.accessRights;
        this.description = req.description;
        this.hasVersion = req.hasVersion;
        this.dataset = req.isPartOf;
        this.language = req.language;
        this.publisher = req.publisher;
        this.license = req.license;
        this.title = req.title;
        this.label = req.label;
        this.downloadURL = req.downloadURL;
        this.mediaType = req.mediaType;
        this.metadataIssued = req.metadataIssued;
        this.metadataModified = req.metadataModified;
        return true;
    },
    updateDist: function (req) {
        for (let key of Object.keys(req)) {
            console.log(`Key: ${key}`);
            if (!['distribution_id', 'publisher', 'mediaType', 'downloadURL'].includes(key)) {
                this[key] = req[key];
            }
        }
        return true;
    },
    addEncryptionKey: function (key){
      this.encryptionKey = key;
    },
    getEncryptionKey: function (){
      return this.encryptionKey;
    },
    get: function () {
        return {
            distribution_id: this.distribution_id,
            accessRights: this.accessRights,
            description: this.description,
            hasVersion: this.hasVersion,
            dataset: this.dataset,
            language: this.language,
            publisher: this.publisher,
            license: this.license,
            title: this.title,
            label: this.label,
            downloadURL: this.downloadURL,
            mediaType: this.mediaType,
            metadataIssued: this.metadataIssued,
            metadataModified: this.metadataModified
        }
    },
    remove: function () {
        this.removed = true;
        return true;
    }
});
