#!/bin/bash

# Start the first process
node /p2p_fair_point/bin/www &
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start my_first_process: $status"
  exit $status
fi

# Start the second process
cd /psk
node ./psknode/bin/scripts/psknode.js &
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start my_second_process: $status"
  exit $status
fi

#going back to root folder
cd ..

# Start the third process
cd /web
npx ng serve --host=0.0.0.0 --disableHostCheck
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start my_second_process: $status"
  exit $status
fi

#going back to root folder
cd ..
