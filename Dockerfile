FROM node:12.9.0-slim

#install compile dependencies
RUN apt update -y && apt install -y iproute2
RUN apt-get install -y build-essential
RUN apt-get install -y python2.7 python-pip
RUN apt-get install gettext-base

# =================================
#p2p fair point install
RUN mkdir /p2p_fair_point
WORKDIR /p2p_fair_point

COPY ./p2p_fair_point/package*.json ./

# install needed packages
RUN npm install --only=production

# copying source files
COPY ./p2p_fair_point .


# =================================
# psk install
RUN mkdir /psk
WORKDIR /psk

COPY ./package*.json ./

# install needed packages
RUN npm install

# copying source files
COPY . .

# run build of PSK
RUN node scripts/builder.js


# =================================
# web interface install
RUN mkdir /web
WORKDIR /web

COPY ./web/package*.json ./
# install needed packages
RUN npm install
# copying source files
COPY ./web .
COPY ./web/angular-dockerfile.json angular.json

# =================================
#copy start script
WORKDIR /
COPY start-app.sh start-app.sh


EXPOSE 8087 8080 4200

RUN chmod +x ./start-app.sh

#start p2p fair point
CMD ["/bin/sh",  "-c",  "envsubst < /web/src/assets/env.template.js > /web/src/assets/env.js && ./start-app.sh"]
