const express = require('express');
const router = express.Router();
const JSZip = require("jszip");
const zip = new JSZip();
const fs = require('fs');
const uuid = require('uuid');
const multer = require('multer');
const {encrypt} = require("../utils/modules");
const upload = multer();

router.post('/', upload.any(), async function (req, res) {
    if (!req.files || !req.files.length) {
        console.error('No files');
        await res.json({'status': 1, 'message': 'Files missing from upload'});
        res.end();
        return;
    }

    req.files.forEach(file => zip.file(file.originalname, file.buffer));

    let content = null;
    // this true, so we get uint8arr
    if (JSZip.support.uint8array) {
        content = await zip.generateAsync({type: "uint8array"});
    } else {
        content = await zip.generateAsync({type: "string"});
    }

    let {result, key} = encrypt(content);
    const archiveId = uuid.v4();
    try{
        fs.writeFileSync(`/p2p_fair_point/uploads/${archiveId}.zip`, result);
        await res.json({'status': 0, 'url': `http://${process.env.HOST_ADDR}:8087/uploads/${archiveId}.zip`, key: key});
        res.end();
    }catch(err){
        console.error(err);
        await res.json({'status': 1, 'message': 'Cannot save archive'});
        res.end();
    }
});

router.delete('/:zip', async function (req, res) {
    if(!req.params || !req.params.zip){
        console.error('Missing parameters');
        await res.json({'status': 1, 'message': 'No distribution provided for delete'});
        res.end();
        return;
    }

    try {
        fs.unlinkSync(`/p2p_fair_point/uploads/${req.params.zip}`);
        await res.json({'status': 0});
        res.end();
    } catch(err) {
        console.error(err);
        await res.json({'status': 1, 'message': 'Cannot remove archive'});
        res.end();
    }

});

module.exports = router;
