const express = require('express');
const router = express.Router();
const config = require('../config.js');
const axios = require('axios');


router.get('/fdp/:resourceType/:resourceName', async function (req, res, next) {
    if (!req.params || !req.params.resourceType || !req.params.resourceName) {
        res.render("error", {
            'message': 'Missing resource type or name',
            'error': {
                'status': 400,
                'stack': 'Bad Request'
            }
        });
    }

    console.log(process.env.HOST_ADDR);
    const sparqlQuery = `DESCRIBE <http://${process.env.HOST_ADDR}:8087${req.url}>`;
    let fullUrl = config[req.params.resourceType];
    let method = 'GET';
    fullUrl += '?query=' + encodeURIComponent(sparqlQuery);
    const headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'};
    //get data from jena
    let response = '';
    try {
        response = await axios({
            url: fullUrl,
            method: method,
            headers: headers
        })
    } catch (err) {
        console.error(err);
        throw err;
    }

    // skip first element cause it has title
    const initialSplit = response.data.split(';').slice(1);
    // lines start with \n
    const splitByNewLine = initialSplit.map(l => l.split('\n').slice(1).map(d => d.trim()));
    const prettyValues =  splitByNewLine.map(([key, value], index) => {
        if(index === splitByNewLine.length - 1){
            // last line has .
            value = value.replace(/\./g, '');
        }
        const replacedKey = key.replace(/[<>]/g, '');
        // time
        if(value.includes('^^')){
            const stringDate = value.split('^^')[0].replace(/"/g, '');
            return [replacedKey, stringDate];
        }
        // string value
        if(value.startsWith('"') && value.endsWith('"')){
            return [replacedKey, value.replace(/"/g, '')];
        }
        // array values
        if(value.includes(',')){
            const splitValues = value.split(',').map(d => d.trim().replace(/[<>.\s]/g, ''));
            return [replacedKey, splitValues];
        }
        // single jena id with <>
        return [replacedKey, value.replace(/[<>]/g, '')];
    });

    return res.render('index', {
        fairValues: prettyValues,
        title:
            (prettyValues.find(([key]) => key ==='http://purl.org/dc/terms/title') || [null, 'DataShareFair'])[1]
    });
});

module.exports = router;
