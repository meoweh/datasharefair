const crypto = require('crypto');
const algorithm = 'aes-256-ctr';
const uuid = require('uuid');
let encrKey = uuid.v4();
const fs = require('fs');

exports.encrypt = (buffer) => {
    const key = crypto.createHash('sha256').update(String(encrKey)).digest('base64').substr(0, 32);
    // Create an initialization vector
    const iv = crypto.randomBytes(16);
    // Create a new cipher using the algorithm, key, and iv
    const cipher = crypto.createCipheriv(algorithm, key, iv);

    // Create the new (encrypted) buffer
    const result = Buffer.concat([iv, cipher.update(buffer), cipher.final()]);
    return {result, key};
};


const decrypt = (encrypted, key) => {
    // Get the iv: the first 16 bytes
    const buf = new Buffer(encrypted, 'base64');
    const iv = buf.slice(0, 16);
    console.log(iv);
    // Get the rest
    const encr = buf.toString('base64', 16);
    // Create a decipher
    const decipher = crypto.createDecipheriv(algorithm, key, iv);
    // Actually decrypt it
    return Buffer.concat([decipher.update(encr, 'base64'), decipher.final()]);
};

// fs.writeFileSync('./uploads/decrypt.zip', decrypt(fs.readFileSync('./utils/test.txt'), 'rFTet4PIQ6dA1HDBlSYfcyqk1Gd4C4e7'));
