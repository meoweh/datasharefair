const jenaIP = process.env.HOST_ADDR;

module.exports = {
     "catalogs": `http://${jenaIP}:3030/CatalogMetadata/sparql`,
     "datasets": `http://${jenaIP}:3030/DatasetMetadata/sparql`,
     "distributions": `http://${jenaIP}:3030/DistributionMetadata/sparql`,
     "organizations": `http://${jenaIP}:3030/Organizations/sparql`,
     "authorizations": `http://${jenaIP}:3030/Authorizations/sparql`,
     "accessPolicies": `http://${jenaIP}:3030/AccessPolicies/sparql`
};
