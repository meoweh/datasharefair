export interface AccessPolicy {
  entityId: string;
  entityName: string;
  accessLevel: 'Read' | 'Write' | 'Control';
  status: 'pending' | 'approved' | 'denied';
}

export interface Catalog {
  catalog_id: string;
  title: string;
  publisher: string;
  description: string;
  version: string;
  license: string;
  created: string;
  modified: string;
  language: string;
  datasets: Array<string>;
  accessRights: Array<AccessPolicy>;
  categories: Array<string>; // aka themes
}

export interface Dataset {
  catalog: string;
  categories: Array<string>; // aka themes
  created: string;
  dataset_id: string;
  description: string;
  keywords: Array<string>;
  language: string;
  license: string;
  modified: string;
  publisher: string;
  title: string;
  version: string;
  accessRights: Array<AccessPolicy>;
}

export interface Distribution {
  distribution_id: string;
  accessRights: Array<AccessPolicy>;
  description: string;
  version: string;
  dataset: string;
  language: string;
  license: string;
  publisher: string;
  title: string;
  label: string;
  url: string;
  mediaType: string;
  created: string;
  modified: string;
}

export interface GroupDescription {
  resource_id: string;
  group_id: string;
  group_name: string;
  description: string;
  creator: string;
  members: Array<MemberDescription>;
}

export interface MemberDescription {
  resource_id: string;
  company_id: string;
  name: string;
  category: string;
  region: string;
  email?: string;
  address?: string;
}

export interface DatasetWithDistributionsUserAccess extends Dataset {
  distributions: Array<Distribution>;
}

export interface LimitedMemberDesc {
  resource_id: string;
  company_id: string;
  name: string;
}

export interface NewAccessRequestCreation {
  accessType: 'Group' | 'User';
  accessEntity: string;
  accessByField: 'company_id' | 'group_id' | 'name';
  accessLevel: 'Read' | 'Write' | 'Control';
}

export interface NewAccessRequestNotification {
  accessType: 'Group' | 'User';
  accessEntity: string;
  accessEntityId: string;
  accessLevel: 'Read' | 'Write' | 'Control';
  status: 'pending' | 'approved' | 'denied';
  resourceType: 'catalog' | 'dataset' | 'distribution';
  resourceTitle: string;
  resourceId: string;
}

const MOCKED_ACCESS_RIGHTS_APPROVED: Array<AccessPolicy> = [
  {
    entityId: 'http://136.243.4.200:8087/organizations/1234',
    entityName: 'Pingu1234',
    accessLevel: 'Control',
    status: 'approved',
  },
  {
    entityId: 'http://136.243.4.200:8087/organizations/434343',
    entityName: 'Group434343',
    accessLevel: 'Write',
    status: 'approved',
  },
];

const MOCKED_ACCESS_RIGHTS_PENDING: Array<AccessPolicy> = [
  {
    ...MOCKED_ACCESS_RIGHTS_APPROVED[0],
    status: 'pending',
  },
  {
    ...MOCKED_ACCESS_RIGHTS_APPROVED[1],
    status: 'pending',
  },
];

export const MOCKED_CATALOGS: Array<Catalog> = [
  {
    catalog_id: 'http://136.243.4.200:8087/fdp/catalog/textmining',
    title: 'Textmining catalog 1',
    description: 'Catalog for textmining 1',
    version: '0.0.1',
    language: 'en',
    license: 'DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE',
    created: '2018-03-20T10:20:37.08Z',
    modified: '2018-08-20T13:09:55',
    publisher: 'http://biosemantics.org',
    datasets: [
      'http://136.243.4.200:8087/fdp/dataset/gene_disease_association',
    ],
    categories: ['http://dbpedia.org/resource/Text_mining', 'http://edamontology.org/topic_0218'],
    accessRights: [MOCKED_ACCESS_RIGHTS_APPROVED[0], MOCKED_ACCESS_RIGHTS_APPROVED[1]],
  },
  {
    catalog_id: 'http://136.243.4.200:8087/fdp/catalog/textmining_2',
    title: 'Textmining catalog 2',
    description: 'Catalog for textmining 2',
    version: '0.0.2',
    license: 'Unlicense',
    language: 'en',
    created: '2018-03-20T10:20:37.08Z',
    modified: '2018-08-20T13:09:55',
    publisher: 'http://biosemantics.org',
    datasets: [
      'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_2',
    ],
    categories: ['http://dbpedia.org/resource/Text_mining', 'http://edamontology.org/topic_0218'],
    accessRights: [MOCKED_ACCESS_RIGHTS_APPROVED[0], MOCKED_ACCESS_RIGHTS_APPROVED[1]],
  },
  {
    catalog_id: 'http://136.243.4.200:8087/fdp/catalog/textmining_3',
    title: 'Textmining catalog 3',
    description: 'Catalog for textmining 3',
    version: '0.0.3',
    language: 'en',
    license: 'Unlicense',
    created: '2018-03-20T10:20:37.08Z',
    modified: '2018-08-20T13:09:55',
    publisher: 'http://biosemantics.org',
    datasets: [
      'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_3',
    ],
    categories: ['http://dbpedia.org/resource/Text_mining', 'http://edamontology.org/topic_0218'],
    accessRights: [MOCKED_ACCESS_RIGHTS_APPROVED[0], MOCKED_ACCESS_RIGHTS_APPROVED[1]],
  },
  {
    catalog_id: 'http://136.243.4.200:8087/fdp/catalog/textmining_4',
    title: 'Textmining catalog 4',
    language: 'en',
    description: 'Catalog for textmining 4 very long such wow much amazing description',
    version: '0.0.4',
    license: 'Unlicense',
    created: '2018-03-20T10:20:37.08Z',
    modified: '2018-08-20T13:09:55',
    publisher: 'http://biosemantics.org',
    datasets: [
      'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_4',
    ],
    categories: ['http://dbpedia.org/resource/Text_mining', 'http://edamontology.org/topic_0218'],
    accessRights: [MOCKED_ACCESS_RIGHTS_APPROVED[0], MOCKED_ACCESS_RIGHTS_APPROVED[1]],
  },
];
Object.freeze(MOCKED_CATALOGS);

export const MOCKED_DATASETS_NO_DISTRIBUTIONS: Array<Dataset> = [
  {
    catalog: MOCKED_CATALOGS[0].catalog_id,
    categories: ['<http://dbpedia.org/resource/Text_mining>', '<http://semanticscience.org/resource/statistical-association>'],
    created: '2018-03-20T10:30:18.662Z',
    dataset_id: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association',
    description: 'High-throughput experimental methods such as medical sequencing and genome-wide association studies (GWAS) identify increasingly large numbers of potential relations between genetic variants and diseases. Both biological complexity (millions of potential gene-disease associations) and the accelerating rate of data production necessitate computational approaches to prioritize and rationalize potential gene-disease relations. Here, we use concept profile technology to expose from the biomedical literature both explicitly stated gene-disease relations (the explicitome) and a much larger set of implied gene-disease associations (the implicitome). Implicit relations are largely unknown to, or are even unintended by the original authors, but they vastly extend the reach of existing biomedical knowledge for identification and interpretation of gene-disease associations. The implicitome can be used in conjunction with experimental data resources to rationalize both known and novel associations. We demonstrate the usefulness of the implicitome by rationalizing known and novel gene-disease associations, including those from GWAS. To facilitate the re-use of implicit gene-disease associations, we publish our data in compliance with FAIR Data Publishing recommendations [https://www.force11.org/group/fairgroup] using nanopublications. An online tool (http://knowledge.bio) is available to explore established and potential gene-disease associations in the context of other biomedical relations.',
    keywords: ['GDA', 'Gene disease association (LUMC)', 'LWAS', 'Text mining', 'The Explicitome',
      'The Implicitome'],
    language: 'en',
    modified: '2018-08-20T13:09:55',
    license: '<http://rdflicense.appspot.com/rdflicense/cc-by-nc-nd3.0>',
    publisher: 'Biosemantic group',
    title: 'Gene disease association (LUMC)',
    version: '0.0.1',
    accessRights: [MOCKED_ACCESS_RIGHTS_APPROVED[0], MOCKED_ACCESS_RIGHTS_APPROVED[1]],
  },
  {
    catalog: MOCKED_CATALOGS[1].catalog_id,
    categories: ['<http://dbpedia.org/resource/Text_mining>', '<http://semanticscience.org/resource/statistical-association>'],
    created: '2018-03-20T10:30:18.662Z',
    dataset_id: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_2',
    description: 'High-throughput experimental methods such as medical sequencing and genome-wide association studies (GWAS) identify increasingly large numbers of potential relations between genetic variants and diseases. Both biological complexity (millions of potential gene-disease associations) and the accelerating rate of data production necessitate computational approaches to prioritize and rationalize potential gene-disease relations. Here, we use concept profile technology to expose from the biomedical literature both explicitly stated gene-disease relations (the explicitome) and a much larger set of implied gene-disease associations (the implicitome). Implicit relations are largely unknown to, or are even unintended by the original authors, but they vastly extend the reach of existing biomedical knowledge for identification and interpretation of gene-disease associations. The implicitome can be used in conjunction with experimental data resources to rationalize both known and novel associations. We demonstrate the usefulness of the implicitome by rationalizing known and novel gene-disease associations, including those from GWAS. To facilitate the re-use of implicit gene-disease associations, we publish our data in compliance with FAIR Data Publishing recommendations [https://www.force11.org/group/fairgroup] using nanopublications. An online tool (http://knowledge.bio) is available to explore established and potential gene-disease associations in the context of other biomedical relations.',
    keywords: ['GDA', 'Gene disease association (LUMC)', 'LWAS', 'Text mining', 'The Explicitome',
      'The Implicitome'],
    language: 'en',
    modified: '2018-08-20T13:09:55',
    license: '<http://rdflicense.appspot.com/rdflicense/cc-by-nc-nd3.0>',
    publisher: 'Biosemantic group',
    title: 'Gene disease association (LUMC)',
    version: '0.0.2',
    accessRights: [MOCKED_ACCESS_RIGHTS_APPROVED[0], MOCKED_ACCESS_RIGHTS_APPROVED[1]],
  },
  {
    catalog: MOCKED_CATALOGS[2].catalog_id,
    categories: ['eyelash', 'eye', 'pretty'],
    created: '2018-03-20T10:30:18.662Z',
    dataset_id: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_3',
    description: 'Gene disease stuff; much important, very wow, pay attention!',
    keywords: ['gene', 'disease', 'wow'],
    language: 'en',
    modified: '2018-08-20T13:09:55',
    license: 'DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE',
    publisher: 'Biosemantic group',
    title: 'Gene disease association (LUMC) 3',
    version: '0.0.3',
    accessRights: [MOCKED_ACCESS_RIGHTS_APPROVED[0], MOCKED_ACCESS_RIGHTS_APPROVED[1]],
  },
  {
    catalog: MOCKED_CATALOGS[3].catalog_id,
    categories: ['skin', 'care'],
    created: '2018-03-20T10:30:18.662Z',
    dataset_id: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_4',
    description: 'Gene disease stuff 2; very much important, mega wow, pay much attention!',
    keywords: ['much', 'mega', 'wow'],
    language: 'eo',
    modified: '2018-08-20T13:09:55',
    license: 'Unlicense',
    publisher: 'Biosemantic group',
    title: 'Gene disease association (LUMC) 4',
    version: '0.0.4',
    accessRights: [
      {
        entityId: '1234',
        accessLevel: 'Control',
        status: 'approved',
        entityName: 'Pingu1234',
      },
      {
        entityId: '434343',
        accessLevel: 'Write',
        status: 'approved',
        entityName: 'Group434343',
      },
    ],
  },
];
Object.freeze(MOCKED_DATASETS_NO_DISTRIBUTIONS);

export const MOCKED_DATASETS_WITH_DISTRIBUTION_USER_ACCESS: Array<DatasetWithDistributionsUserAccess> = [
  {
    ...MOCKED_DATASETS_NO_DISTRIBUTIONS[0],
    distributions: [
      {
        url: 'http://136.243.4.200:3000/gene_disease_association_csv.gzip',
        title: 'Gene disease association (LUMC) nquads as CSV distribution',
        distribution_id: 'http://136.243.4.200:8087/fdp/distribution/gene_disease_association_csv_gzip',
        description: 'The complete set of all ~204 million associations (explicit and implicit) as nanopublications. Each nanopublication asserts an association between a gene and a disease concept and the percentile rank of the match score.',
        created: '2018-03-20T10:20:37.08Z',
        modified: '2018-08-20T13:09:55',
        accessRights: [MOCKED_ACCESS_RIGHTS_PENDING[0], MOCKED_ACCESS_RIGHTS_PENDING[1]],
        version: '1.0.1',
        dataset: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_3',
        language: 'en',
        license: 'http://rdflicense.appspot.com/rdflicense/cc-by-nc-nd3.0',
        publisher: 'http://biosemantics.org>',
        label: 'Gene disease association (LUMC) nquads as gzip distribution',
        mediaType: 'application/gzip',
      },
      {
        url: 'http://136.243.4.200:3000/gene_disease_association_csv.gzip',
        title: 'Gene disease association (LUMC) nquads as HTML distribution',
        distribution_id: 'http://136.243.4.200:8087/fdp/distribution/gene_disease_association_csv_gzip',
        description: 'The complete set of all ~204 million associations (explicit and implicit) as nanopublications. Each nanopublication asserts an association between a gene and a disease concept and the percentile rank of the match score.',
        created: '2018-03-20T10:20:37.08Z',
        modified: '2018-08-20T13:09:55',
        accessRights: [MOCKED_ACCESS_RIGHTS_PENDING[0], MOCKED_ACCESS_RIGHTS_PENDING[1]],
        version: '1.0.1',
        dataset: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_3',
        language: 'en',
        license: 'http://rdflicense.appspot.com/rdflicense/cc-by-nc-nd3.0',
        publisher: 'http://biosemantics.org>',
        label: 'Gene disease association (LUMC) nquads as gzip distribution',
        mediaType: 'application/html',
      }, {
        url: 'http://136.243.4.200:3000/gene_disease_association_csv.gzip',
        title: 'Gene disease association (LUMC) nquads as CSV distribution',
        distribution_id: 'http://136.243.4.200:8087/fdp/distribution/gene_disease_association_csv_gzip',
        description: 'The complete set of all ~204 million associations (explicit and implicit) as nanopublications. Each nanopublication asserts an association between a gene and a disease concept and the percentile rank of the match score.',
        created: '2018-03-20T10:20:37.08Z',
        modified: '2018-08-20T13:09:55',
        accessRights: [MOCKED_ACCESS_RIGHTS_PENDING[0], MOCKED_ACCESS_RIGHTS_PENDING[1]],
        version: '1.0.1',
        dataset: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_3',
        language: 'en',
        license: 'http://rdflicense.appspot.com/rdflicense/cc-by-nc-nd3.0',
        publisher: 'http://biosemantics.org>',
        label: 'Gene disease association (LUMC) nquads as gzip distribution',
        mediaType: 'application/csv',
      },
    ],
  },
  {
    ...MOCKED_DATASETS_NO_DISTRIBUTIONS[1],
    distributions: [
      {
        url: 'http://136.243.4.200:3000/gene_disease_association_csv.gzip',
        title: 'Gene disease association (LUMC) nquads as CSV distribution',
        distribution_id: 'http://136.243.4.200:8087/fdp/distribution/gene_disease_association_csv_gzip',
        description: 'The complete set of all ~204 million associations (explicit and implicit) as nanopublications. Each nanopublication asserts an association between a gene and a disease concept and the percentile rank of the match score.',
        created: '2018-03-20T10:20:37.08Z',
        modified: '2018-08-20T13:09:55',
        accessRights: [MOCKED_ACCESS_RIGHTS_PENDING[0], MOCKED_ACCESS_RIGHTS_PENDING[1]],
        version: '1.0.1',
        dataset: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_3',
        language: 'en',
        license: 'http://rdflicense.appspot.com/rdflicense/cc-by-nc-nd3.0',
        publisher: 'http://biosemantics.org>',
        label: 'Gene disease association (LUMC) nquads as gzip distribution',
        mediaType: 'application/gzip',
      },
      {
        url: 'http://136.243.4.200:3000/gene_disease_association_csv.gzip',
        title: 'Gene disease association (LUMC) nquads as HTML distribution',
        distribution_id: 'http://136.243.4.200:8087/fdp/distribution/gene_disease_association_csv_gzip',
        description: 'The complete set of all ~204 million associations (explicit and implicit) as nanopublications. Each nanopublication asserts an association between a gene and a disease concept and the percentile rank of the match score.',
        created: '2018-03-20T10:20:37.08Z',
        modified: '2018-08-20T13:09:55',
        accessRights: [MOCKED_ACCESS_RIGHTS_PENDING[0], MOCKED_ACCESS_RIGHTS_PENDING[1]],
        version: '1.0.1',
        dataset: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_3',
        language: 'en',
        license: 'http://rdflicense.appspot.com/rdflicense/cc-by-nc-nd3.0',
        publisher: 'http://biosemantics.org>',
        label: 'Gene disease association (LUMC) nquads as gzip distribution',
        mediaType: 'application/html',
      }, {
        url: 'http://136.243.4.200:3000/gene_disease_association_csv.gzip',
        title: 'Gene disease association (LUMC) nquads as CSV distribution',
        distribution_id: 'http://136.243.4.200:8087/fdp/distribution/gene_disease_association_csv_gzip',
        description: 'The complete set of all ~204 million associations (explicit and implicit) as nanopublications. Each nanopublication asserts an association between a gene and a disease concept and the percentile rank of the match score.',
        created: '2018-03-20T10:20:37.08Z',
        modified: '2018-08-20T13:09:55',
        accessRights: [MOCKED_ACCESS_RIGHTS_PENDING[0], MOCKED_ACCESS_RIGHTS_PENDING[1]],
        version: '1.0.1',
        dataset: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_3',
        language: 'en',
        license: 'http://rdflicense.appspot.com/rdflicense/cc-by-nc-nd3.0',
        publisher: 'http://biosemantics.org>',
        label: 'Gene disease association (LUMC) nquads as gzip distribution',
        mediaType: 'application/csv',
      },
    ],
  },
  {
    ...MOCKED_DATASETS_NO_DISTRIBUTIONS[2],
    distributions: [],
  },
  {
    ...MOCKED_DATASETS_NO_DISTRIBUTIONS[3],
    distributions: [],
  },
];

export const MOCKED_DISTRIBUTIONS: Array<Distribution> = [
  {
    url: 'http://136.243.4.200:3000/gene_disease_association_csv.gzip',
    title: 'Gene disease association (LUMC) nquads as CSV distribution',
    distribution_id: 'http://136.243.4.200:8087/fdp/distribution/gene_disease_association_csv_gzip',
    description: 'The complete set of all ~204 million associations (explicit and implicit) as nanopublications. Each nanopublication asserts an association between a gene and a disease concept and the percentile rank of the match score.',
    created: '2018-03-20T10:20:37.08Z',
    modified: '2018-08-20T13:09:55',
    accessRights: [MOCKED_ACCESS_RIGHTS_PENDING[0], MOCKED_ACCESS_RIGHTS_PENDING[1]],
    version: '1.0.1',
    dataset: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_3',
    language: 'en',
    license: 'http://rdflicense.appspot.com/rdflicense/cc-by-nc-nd3.0',
    publisher: 'http://biosemantics.org>',
    label: 'Gene disease association (LUMC) nquads as gzip distribution',
    mediaType: 'application/gzip',
  },
  {
    url: 'http://136.243.4.200:3000/gene_disease_association_csv.gzip',
    title: 'Gene disease association (LUMC) nquads as HTML distribution',
    distribution_id: 'http://136.243.4.200:8087/fdp/distribution/gene_disease_association_csv_gzip',
    description: 'The complete set of all ~204 million associations (explicit and implicit) as nanopublications. Each nanopublication asserts an association between a gene and a disease concept and the percentile rank of the match score.',
    created: '2018-03-20T10:20:37.08Z',
    modified: '2018-08-20T13:09:55',
    accessRights: [MOCKED_ACCESS_RIGHTS_PENDING[0], MOCKED_ACCESS_RIGHTS_PENDING[1]],
    version: '1.0.1',
    dataset: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_3',
    language: 'en',
    license: 'http://rdflicense.appspot.com/rdflicense/cc-by-nc-nd3.0',
    publisher: 'http://biosemantics.org>',
    label: 'Gene disease association (LUMC) nquads as gzip distribution',
    mediaType: 'application/gzip',
  }, {
    url: 'http://136.243.4.200:3000/gene_disease_association_csv.gzip',
    title: 'Gene disease association (LUMC) nquads as GZIP distribution',
    distribution_id: 'http://136.243.4.200:8087/fdp/distribution/gene_disease_association_csv_gzip',
    description: 'The complete set of all ~204 million associations (explicit and implicit) as nanopublications. Each nanopublication asserts an association between a gene and a disease concept and the percentile rank of the match score.',
    created: '2018-03-20T10:20:37.08Z',
    modified: '2018-08-20T13:09:55',
    accessRights: [MOCKED_ACCESS_RIGHTS_PENDING[0], MOCKED_ACCESS_RIGHTS_PENDING[1]],
    version: '1.0.1',
    dataset: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_3',
    language: 'en',
    license: 'http://rdflicense.appspot.com/rdflicense/cc-by-nc-nd3.0',
    publisher: 'http://biosemantics.org>',
    label: 'Gene disease association (LUMC) nquads as gzip distribution',
    mediaType: 'application/gzip',
  },
];



export const MOCKED_GROUPS: Array<GroupDescription> = [{
  resource_id: 'http://entities.com/group434343',
  group_id: 'group434343',
  group_name: 'Penguin Group',
  creator: '1234',
  description: 'The Pingu Group',
  members: [
    {
      resource_id: 'http://entities.com/pingu1234',
      company_id: '123456789101123',
      category: 'Educational Institution',
      name: 'Pinguinescool Enterprise',
      region: 'EU',
    },
    {
      resource_id: 'http://entities.com/pingu5678',
      company_id: '123456789101123',
      category: 'Educational Institution',
      name: 'Pinguinescool Store',
      region: 'EU',
    },
  ],
},
  {
    resource_id: 'http://entities.com/group5454545',
    group_id: 'group5454545',
    creator: '5678',
    group_name: 'Penguin Group 2',
    description: 'The Second Pingu Group',
    members: [
      {
        resource_id: 'http://entities.com/pingu1234',
        company_id: '123456789101123',
        category: 'Educational Institution',
        name: 'Pinguinescool Enterprise',
        region: 'EU',
      },
      {
        resource_id: 'http://entities.com/pingu5678',
        company_id: '123456789101123',
        category: 'Educational Institution',
        name: 'Pinguinescool Store',
        region: 'EU',
      },
    ],
  }];
Object.freeze(MOCKED_GROUPS);



export const MOCKED_ALL_MEMBERS: Array<LimitedMemberDesc> = [
  {
    resource_id: 'http://entities.com/pingu1234',
    company_id: '123456789101123',
    name: 'Pinguinescool Enterprise',
  },
  {
    resource_id: 'http://entities.com/pingu5678',
    company_id: '123456789101124',
    name: 'Pinguinescool Store',
  },
];
Object.freeze(MOCKED_DATASETS_WITH_DISTRIBUTION_USER_ACCESS);

export const MOCKED_CURRENT_USER_DATA: MemberDescription = {
  resource_id: 'http://entities.com/pingu1234',
  company_id: '1234',
  name: 'Pinguinescool Enterprise',
  email: 'pingu@pingu.com',
  address: 'Pingu Street 123',
  category: 'Educational Institution',
  region: 'EU',
};
Object.freeze(MOCKED_CURRENT_USER_DATA);



export const MOCKED_ACCESS_NOTIFICATIONS: Array<NewAccessRequestNotification> = [{
  accessType: 'User',
  accessEntity: 'Pinguinescool Enterprise',
  accessEntityId: '1234',
  accessLevel: 'Read',
  resourceType: 'dataset',
  resourceTitle: 'Gene disease association (LUMC)',
  resourceId: 'http://136.243.4.200:8087/fdp/dataset/gene_disease_association_2',
  status: 'pending',
},
  {
    accessType: 'Group',
    accessEntity: 'The Pingu Group',
    accessEntityId: '434343',
    accessLevel: 'Write',
    status: 'pending',
    resourceType: 'distribution',
    resourceTitle: 'Gene disease association (LUMC) nquads as HTML distribution',
    resourceId: 'http://136.243.4.200:8087/fdp/distribution/gene_disease_association_html',
  }];
