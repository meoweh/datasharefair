export const licenseMap = {
  'aal.txt': {
    aliases: [],
    name: 'Attribution Assurance License',
    version: null,
    url: 'https://opensource.org/licenses/AAL',
    abbreviation: 'AAL',
    approved: true,
    title: 'ATTRIBUTION ASSURANCE LICENSE'
  },
  'agpl-3.txt': {
    aliases: [],
    name: 'GNU Affero General Public License',
    version: 'v3',
    url: 'https://opensource.org/licenses/AGPL-3.0',
    abbreviation: 'AGPL-3.0',
    approved: true,
    title: 'GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007'
  },
  'apsl-2.0.txt': {
    aliases: [],
    name: 'Apple Public Source License',
    version: '2.0',
    url: 'https://opensource.org/licenses/APSL-2.0',
    abbreviation: 'APSL-2.0',
    approved: true,
    title: 'APPLE PUBLIC SOURCE LICENSE Version 2.0 -  August 6, 2003',
  },
  'artistic-2_0.txt': {
    aliases: [],
    name: 'Artistic License',
    version: '2.0',
    url: 'https://opensource.org/licenses/Artistic-2.0',
    abbreviation: 'Artistic-2.0',
    approved: true,
    title: 'The Artistic License 2.0'
  },
  'asl-1.0.txt': {
    aliases: [],
    name: 'Apache Software License',
    version: '1.0',
    url: 'https://www.apache.org/licenses/LICENSE-1.0',
    approved: false
  },
  'asl-1.1.txt': {
    aliases: [],
    name: 'Apache Software License',
    version: '1.1',
    url: 'https://www.apache.org/licenses/LICENSE-1.1',
    approved: false,
    title: 'The Apache Software License, Version 1.1'
  },
  'asl-2.0.txt': {
    aliases: [],
    name: 'Apache License',
    version: '2.0',
    url: 'https://opensource.org/licenses/Apache-2.0',
    abbreviation: 'Apache-2.0',
    approved: true,
    title: 'Apache License Version 2.0, January 2004',
  },
  'bsd-2clause.txt': {
    aliases: ['FreeBSD License'],
    name: 'BSD 2-Clause License',
    version: null,
    url: 'https://opensource.org/licenses/BSD-2-Clause',
    abbreviation: 'BSD-2-Clause',
    approved: true
  },
  'bsd-3clause.txt': {
    aliases: ['BSD New', 'BSD Simplified'],
    name: 'BSD 3-Clause License',
    version: null,
    url: 'https://opensource.org/licenses/BSD-3-Clause',
    abbreviation: 'BSD-3-Clause',
    approved: true
  },
  'bsd-4clause.txt': {
    aliases: [],
    name: 'BSD 4-Clause License',
    url: 'https://spdx.org/licenses/BSD-4-Clause.html',
    version: null,
    approved: false
  },
  'bsl-1.0.txt': {
    aliases: [],
    name: 'Boost Software License',
    version: '1.0',
    url: 'https://opensource.org/licenses/BSL-1.0',
    abbreviation: 'BSL-1.0',
    approved: true,
    title: 'Boost Software License - Version 1.0 - August 17th, 2003',
  },
  'cc0-1.0.txt': {
    aliases: [],
    name: 'CC0 1.0 Universal',
    url: 'https://creativecommons.org/publicdomain/zero/1.0/',
    version: '1.0',
    approved: false,
    title: 'CC0 1.0 Universal'
  },
  'ccby-3.0.txt': {
    aliases: [],
    name: 'Creative Commons Attribution',
    version: '3.0 Unported',
    url: 'https://creativecommons.org/licenses/by/3.0/',
    approved: false,
    title: 'Attribution 3.0 Unported'
  },
  'ccbync-3.0.txt': {
    aliases: [],
    name: 'Creative Commons Attribution-NonCommercial',
    url: 'https://creativecommons.org/licenses/by-nc/3.0/',
    version: '3.0 Unported',
    approved: false,
    title: 'Attribution-NonCommercial 3.0 Unported'
  },
  'ccbyncnd-3.0.txt': {
    aliases: [],
    name: 'Creative Commons Attribution-NonCommercial-NoDerivs',
    version: '3.0 Unported',
    url: 'https://creativecommons.org/licenses/by-nc-nd/2.0/',
    approved: false,
    title: 'Attribution-NonCommercial-NoDerivs 3.0 Unported'
  },
  'ccbyncsa-3.0.txt': {
    aliases: [],
    name: 'Creative Commons Attribution-NonCommercial-ShareAlike',
    url: 'https://creativecommons.org/licenses/by-nc-sa/3.0/ro/',
    version: '3.0 Unported',
    approved: false,
    title: 'Attribution-NonCommercial-ShareAlike 3.0 Unported'
  },
  'ccbynd-3.0.txt': {
    aliases: [],
    name: 'Creative Commons Attribution-NoDerivs',
    version: '3.0 Unported',
    url: 'https://en.wikisource.org/wiki/Creative_Commons_Attribution-NoDerivs_3.0_Unported',
    approved: false,
    title: 'Attribution-NoDerivs 3.0 Unported'
  },
  'ccbysa-3.0.txt': {
    aliases: [],
    name: 'Creative Commons Attribution-ShareAlike',
    url: 'https://creativecommons.org/licenses/by-sa/3.0/',
    version: '3.0 Unported',
    approved: false,
    title: 'Attribution-ShareAlike 3.0 Unported'
  },
  'cddl-1.0.txt': {
    aliases: [],
    name: 'COMMON DEVELOPMENT AND DISTRIBUTION LICENSE',
    version: '1.0',
    url: 'https://opensource.org/licenses/CDDL-1.0',
    abbreviation: 'CDDL-1.0',
    approved: true,
    title: 'COMMON DEVELOPMENT AND DISTRIBUTION LICENSE Version 1.0 (CDDL-1.0)'
  },
  'cpl-1.0.txt': {
    aliases: [],
    name: 'Common Public License',
    version: '1.0',
    url: 'https://opensource.org/licenses/cpl1.0.php',
    approved: false,
    title: 'Common Public License Version 1.0'
  },
  'epl-1.0.txt': {
    aliases: [],
    name: 'Eclipse Public License',
    version: '1.0',
    url: 'https://opensource.org/licenses/EPL-1.0',
    abbreviation: 'EPL-1.0',
    approved: true,
    title: 'Eclipse Public License, Version 1.0 (EPL-1.0)'
  },
  'fdl-1.1.txt': {
    aliases: [],
    name: 'GNU Free Documentation License',
    version: '1.1',
    approved: false,
    url: 'https://www.gnu.org/licenses/fdl-1.1.html',
    title: 'GNU Free Documentation License Version 1.1, March 2000'
  },
  'fdl-1.2.txt': {
    aliases: [],
    name: 'GNU Free Documentation License',
    version: '1.2',
    approved: false,
    url: 'https://www.gnu.org/licenses/fdl-1.2.html',
    title: 'GNU Free Documentation License Version 1.2, November 2002'
  },
  'fdl-1.3.txt': {
    aliases: [],
    name: 'GNU Free Documentation License',
    version: '1.3',
    approved: false,
    url: 'https://www.gnu.org/licenses/fdl-1.3.html',
    title: 'GNU Free Documentation License Version 1.3, 3 November 2008'
  },
  'gpl-1.txt': {
    aliases: [],
    name: 'GNU General Public License',
    version: '1',
    url: 'https://www.gnu.org/licenses/gpl-1.0.html',
    approved: false,
    title: 'GNU GENERAL PUBLIC LICENSE Version 1, February 1989'
  },
  'gpl-2.txt': {
    aliases: [],
    name: 'GNU General Public License',
    version: '2',
    url: 'https://opensource.org/licenses/GPL-2.0',
    abbreviation: 'GPL-2.0',
    approved: true,
    title: 'GNU GENERAL PUBLIC LICENSE Version 2, June 1991'
  },
  'gpl-3.txt': {
    aliases: [],
    name: 'GNU General Public License',
    version: '3',
    url: 'https://opensource.org/licenses/GPL-3.0',
    abbreviation: 'GPL-3.0',
    approved: true,
    title: 'GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007'
  },
  'lgpl-2.txt': {
    aliases: [],
    name: 'GNU Library General Public License',
    version: '2',
    url: 'https://www.gnu.org/licenses/old-licenses/lgpl-2.0.html',
    approved: false,
    title: 'GNU LIBRARY GENERAL PUBLIC LICENSE Version 2, June 1991'
  },
  'lgpl-2.1.txt': {
    aliases: [],
    name: 'GNU Lesser General Public License',
    version: '2.1',
    url: 'https://opensource.org/licenses/LGPL-2.1',
    abbreviation: 'LGPL-2.1',
    approved: true,
    title: 'GNU LESSER GENERAL PUBLIC LICENSE Version 2.1, February 1999'
  },
  'lgpl-3.txt': {
    aliases: [],
    name: 'GNU Lesser General Public License',
    version: '3',
    url: 'https://opensource.org/licenses/LGPL-3.0',
    abbreviation: 'LGPL-3.0',
    approved: true,
    title: 'GNU LESSER GENERAL PUBLIC LICENSE Version 3, 29 June 2007'
  },
  'mit.txt': {
    aliases: [],
    name: 'MIT License',
    title: 'The MIT License',
    version: null,
    url: 'https://opensource.org/licenses/MIT',
    abbreviation: 'MIT',
    approved: true
  },
  'mpl-1.0.txt': {
    aliases: [],
    name: 'Mozilla Public License',
    version: '1.0',
    approved: false,
    title: 'MOZILLA PUBLIC LICENSE Version 1.0',
    url: 'http://www.mozilla.org/MPL/'
  },
  'mpl-2.0.txt': {
    aliases: [],
    name: 'Mozilla Public License',
    version: '2.0',
    url: 'https://opensource.org/licenses/MPL-2.0',
    abbreviation: 'MPL-2.0',
    approved: true,
    title: 'Mozilla Public License Version 2.0',
  },
  'ofl-1.0.txt': {
    aliases: [],
    name: 'SIL Open Font License',
    version: '1.0',
    approved: false,
    url: 'http://scripts.sil.org/OFL',
    title: 'SIL OPEN FONT LICENSE Version 1.0 - 22 November 2005'
  },
  'ofl-1.1.txt': {
    aliases: [],
    name: 'SIL Open Font License',
    version: '1.1',
    url: 'https://opensource.org/licenses/OFL-1.1',
    abbreviation: 'OFL-1.1',
    title: 'SIL OPEN FONT LICENSE Version 1.1 - 26 February 2007',
    approved: true
  },
  'unlicense.txt': {
    aliases: [],
    name: 'Unlicense',
    version: null,
    url: 'http://unlicense.org/',
    approved: false
  }
};
