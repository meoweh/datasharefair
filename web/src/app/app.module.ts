import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CovalentFileModule } from '@covalent/core/file';
import { NgxJsonLdModule } from '@ngx-lite/json-ld';
import { FileUploadModule } from 'ng2-file-upload';
import { CookieService } from 'ngx-cookie-service';
import { DashAccountComponent } from './account/dash-account.component';
import { DashGroupDialogComponent } from './account/dash-group-dialog.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashHomeComponent } from './dash-home.component';
import { DashLoginComponent } from './dash-login.component';
import { DashMainComponent } from './dash-main.component';
import { DashRegisterComponent } from './dash-register.component';
import { DashDistributionComponent } from './distribution/dash-distribution.component';
import { DistributionAccessComponent } from './distribution/distribution-access.component';
import { DistributionMetadataComponent } from './distribution/distribution-metadata.component';
import { DistributionUploadComponent } from './distribution/distribution-upload.component';
import { DashAccessRightsEditComponent } from './management/dash-access-rights-edit.component';
import { DashCatalogEditComponent } from './management/dash-catalog-edit.component';
import { DashDatasetEditComponent } from './management/dash-dataset-edit.component';
import { DashDatasetsManagementComponent } from './management/dash-datasets-management.component';
import { DashDistributionEditComponent } from './management/dash-distribution-edit.component';
import { DashAccessRequestDialogComponent } from './marketplace/dash-access-request-dialog.component';
import { DashDatasetItemComponent } from './marketplace/dash-dataset-item.component';
import { DashDistributionTabComponent } from './marketplace/dash-distribution-tab.component';
import { DashMarketplaceComponent } from './marketplace/dash-marketplace.component';
import { FilterByPropertyPipe } from './pipes/filter-by-property.pipe';
import { FilterUsedMemberGroupsPipe } from './pipes/filter-used-member-groups.pipe';
import { ObjectKeysPipe } from './pipes/object-keys.pipe';
import { PaginationPipe } from './pipes/pagination.pipe';
import { UrlToStatePipe } from './pipes/url-to-state.pipe';
import { DashThemesFormComponent } from './shared/dash-themes-form.component';
import { LinkIfNeededComponent } from './shared/link-if-needed.component';

@NgModule({
  declarations: [
    AppComponent,
    DashRegisterComponent,
    DashLoginComponent,
    DashAccountComponent,
    DashHomeComponent,
    DashMainComponent,
    DashDatasetsManagementComponent,
    DashMarketplaceComponent,
    DashDistributionComponent,
    DashMainComponent,
    UrlToStatePipe,
    PaginationPipe,
    DashDatasetItemComponent,
    ObjectKeysPipe,
    DashGroupDialogComponent,
    FilterByPropertyPipe,
    DistributionMetadataComponent,
    DistributionUploadComponent,
    DistributionAccessComponent,
    FilterUsedMemberGroupsPipe,
    DashAccessRequestDialogComponent,
    DashDistributionTabComponent,
    DashCatalogEditComponent,
    DashDatasetEditComponent,
    DashDistributionEditComponent,
    DashAccessRightsEditComponent,
    DashThemesFormComponent,
    LinkIfNeededComponent,
  ],
  imports: [
    CovalentFileModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    HttpClientModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatRadioModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatListModule,
    MatSelectModule,
    MatExpansionModule,
    MatStepperModule,
    NgxJsonLdModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatChipsModule,
    MatCheckboxModule,
    MatTableModule,
    MatTabsModule,
    MatAutocompleteModule,
    MatDialogModule,
    FileUploadModule,
    MatProgressBarModule,
    MatMenuModule,
    MatBadgeModule,
  ],
  entryComponents: [
    DashGroupDialogComponent,
    DashAccessRequestDialogComponent,
    DashCatalogEditComponent,
    DashDatasetEditComponent,
    DashDistributionEditComponent,
  ],
  providers: [
    CookieService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
