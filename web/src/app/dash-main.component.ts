import { Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { NewAccessRequestNotification } from '../assets/mocked-data';
import { DashAccountService } from './services/dash-account.service';
import { DashDataConvertorService } from './services/dash-data-convertor.service';
import { DashLoginService } from './services/dash-login.service';
import { DashUtilsService } from './services/dash-utils.service';

@Component({
  selector: 'dash-main',
  templateUrl: './dash-main.component.html',
  styleUrls: ['./dash-main.component.scss'],
})
export class DashMainComponent {

  @ViewChild('sidenav', {static: false}) sidenav: MatSidenav;
  public notifications: Array<NewAccessRequestNotification> = [];

  constructor(public $router: Router, private UtilsService: DashUtilsService, public $cookies: CookieService, public SessionService: DashLoginService, public AccountService: DashAccountService, public convertorService: DashDataConvertorService) {
    this.getAccessNotifications();
  }

  close() {
    this.sidenav.close().then();
  }

  getAccessNotifications() {
    this.AccountService.getAccessRightsNotificationsForUser((err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('Retrieving access requests failed');
        return;
      }
      this.notifications = this.convertorService.covertMyData(res.data, 'notification');
      console.log(this.notifications);
    });
  }

  logOut() {
    const req = {
      user_token: this.$cookies.get('user_token'),
    };

    this.SessionService.logout(req, (err, res) => {
      if (err) {
        console.error(err);
        this.UtilsService.openSnackBar('Log out failed. Try again later.');
        return;
      }
      this.$cookies.delete('user_token');
      this.$router.navigate(['/login'])
        .then();
      console.log('Logged Out');
    });

  }

  approveAccessRight($event, notification) {
    $event.preventDefault();
    notification.status = 'approved';
    this.AccountService.editAccessRightsForUser(notification, (err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('There was an issue changing the access rights');
        return;
      }
      this.UtilsService.openSnackBar('Access right approved!');
      this.getAccessNotifications();
    });
  }

  denyAccessRight($event, notification) {
    $event.preventDefault();
    notification.status = 'denied';
    this.AccountService.editAccessRightsForUser(notification, (err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('There was an issue changing the access rights');
        return;
      }
      this.UtilsService.openSnackBar('Access right denied!');
      this.getAccessNotifications();
    });
  }
}

