import { Component, OnInit } from '@angular/core';
import { MatDialog, MatOptionSelectionChange } from '@angular/material';
import { countriesMap } from '../../assets/countries';
import { licenseMap } from '../../assets/licenses';
import { Catalog, Dataset } from '../../assets/mocked-data';
import { DashCatalogEditComponent, NewCatalogAPI } from '../management/dash-catalog-edit.component';
import { DashDatasetEditComponent, NewDatasetAPI } from '../management/dash-dataset-edit.component';
import { DashDatasetService } from '../services/dash-dataset.service';
import { DashUtilsService } from '../services/dash-utils.service';

@Component({
  selector: 'distribution-metadata',
  templateUrl: './distribution-metadata.component.html',
  styleUrls: ['./distribution-metadata.component.scss'],
})
export class DistributionMetadataComponent implements OnInit {

  public userDatasets: Array<Dataset>;
  public userCatalogs: Array<Catalog>;
  public filteredCatalogs: Array<Catalog>;
  public filteredDatasets: Array<Dataset>;

  public countries = countriesMap;
  public licenses = Object.values(licenseMap);

  public distributionFormModel = {
    title: '',
    description: '',
    version: '',
    language: this.countries[0].alpha2,
    license: this.licenses[0].url,
    dataset: '',
    catalog: '',
  };

  constructor(
    private $dashDatasetSvc: DashDatasetService,
    private $dashUtilsSvc: DashUtilsService,
    private $dialog: MatDialog,
  ) {
    this.getUserCatalogs();
  }

  ngOnInit() {
  }

  /*pentru prelucrarea noii distributii */
  getUserDatasets() {

    this.$dashDatasetSvc.getDatasetsForUser().then((result: Array<Dataset>) => {
      if (!result) {
        this.$dashUtilsSvc.openSnackBar('Retrieving user datasets failed');
        return;
      }
      this.userDatasets = result;
      if (!this.userDatasets.length) {
        this.distributionFormModel.dataset = '';
        this.distributionFormModel.catalog = '';
        return;
      }
      this.distributionFormModel.dataset = this.userDatasets[0].dataset_id;
      this.filteredDatasets = [...this.userDatasets];
      this.chooseCatalog(this.userDatasets[0]);
    });
  }

  chooseCatalog(dataset: Dataset, $event?: MatOptionSelectionChange) {
    if ($event && !$event.source.selected) {
      return;
    }
    this.filteredCatalogs = this.userCatalogs.filter((catalog) => {
      if (catalog.datasets && catalog.datasets.includes(dataset.dataset_id)) {
        return catalog;
      }
    });
    if(this.filteredCatalogs &&  this.filteredCatalogs.length){
      this.distributionFormModel.catalog = this.filteredCatalogs[0].catalog_id;
    } else {
      this.distributionFormModel.catalog = '';
    }
  }

  // get all users catalogs
  getUserCatalogs() {
    this.$dashDatasetSvc.getCatalogsForUser().then((result: Array<Catalog>) => {
      if (!result) {
        this.$dashUtilsSvc.openSnackBar('Retrieving user catalogs failed');
        return;
      }
      this.userCatalogs = result;
      // get datasets for those catalogs
      this.getUserDatasets();
    });

  }

  /*create dataset*/
  openDataset() {
    const dialogRef = this.$dialog.open(DashDatasetEditComponent, {
      width: '500px',
      height: '850px',
      data: {
        catalogs: this.userCatalogs,
      },
    });

    dialogRef.afterClosed().subscribe((data: NewDatasetAPI) => {
      this.$dashDatasetSvc.createDataset(data)
        .then((result: { status: number }) => {
          if (result.status === 0) {
            this.$dashUtilsSvc.openSnackBar('Dataset created successfully!');
            this.getUserCatalogs();
            return;
          }
          this.$dashUtilsSvc.openSnackBar('There was an error creating the dataset');
        });
    });
  }

  /*create catalog*/
  openCatalog() {
    const dialogRef = this.$dialog.open(DashCatalogEditComponent, {
      width: '500px',
      height: '650px',
    });

    dialogRef.afterClosed().subscribe((data: NewCatalogAPI) => {
      console.log(data);
      this.$dashDatasetSvc.createCatalog(data)
        .then((result: { status: number }) => {
          if (result.status === 0) {
            this.getUserCatalogs();
            this.$dashUtilsSvc.openSnackBar('Catalog created successfully!');
            return;
          }
          this.$dashUtilsSvc.openSnackBar('There was an issue creating the catalog!');
        });
    });
  }

}
