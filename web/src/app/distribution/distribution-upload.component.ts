import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { FileUploaderCustom } from '../shared/file-uploader-custom';


@Component({
  selector: 'distribution-upload',
  templateUrl: './distribution-upload.component.html',
  styleUrls: ['./distribution-upload.component.scss'],
})
export class DistributionUploadComponent implements OnInit {

  public displayedColumns: string[] = ['name', 'size', 'type', 'actions'];
  public hasBaseDropZoneOver = false;
  public uploader: FileUploaderCustom;

  constructor() {

    this.uploader = new FileUploaderCustom({
      url: `http://${environment.hostIp}:8087/uploader`,
      disableMultipart: false, // 'DisableMultipart' must be 'true' for formatDataFunction to be called.
    });

  }

  ngOnInit() {
  }

  public fileOverBase(e: boolean): void {
    this.hasBaseDropZoneOver = e;
  }

}
