import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {DashAccountService} from '../services/dash-account.service';
import {DashUtilsService} from '../services/dash-utils.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import _ from 'lodash';
import {MatTableDataSource} from '@angular/material';

export interface MemberGroup {
  entity_id: string;
  resource_id: string;
  name: string;
}

interface MemberProfile { entity_id: string; name: string; access: string; not_removable?: boolean }


@Component({
  selector: 'distribution-access',
  templateUrl: './distribution-access.component.html',
  styleUrls: ['./distribution-access.component.scss']
})
export class DistributionAccessComponent implements OnInit {
  // variable used just to trigger the pipe again (for filter values in selects)
  public triggerPipeUpdate = false;
  public memberGroupOptions: Array<MemberGroup> = [];
  public members: Array<MemberProfile> = [];
  public membersDS = new MatTableDataSource<MemberProfile>(this.members);
  public accessForm: FormGroup;
  public searchCrit = [
    {
      viewValue: 'Identifier (VAT/TIN_ID or Group ID)',
      value: 'entity_id'
    },
    {
      viewValue: 'Member/Group Name',
      value: 'name'
    }
  ];

  public searchOption: keyof MemberGroup = 'entity_id';
  public accessRights = ['Read', 'Write', 'Control'];
  public accessOption = this.accessRights[0];

  public displayedColumnsAccessPolicy = ['name', 'company_id', 'access', 'actions'];

  constructor(
    private $dashAccountService: DashAccountService,
    private $dashUtilsSvc: DashUtilsService,
    private $formBuilder: FormBuilder
  ) {
    this.getAccountInfo();
    this.getAllUsersAndGroups();

    this.accessForm = this.$formBuilder.group({
      groupCtrl: ['', Validators.nullValidator]
    });

  }

  ngOnInit() {
  }

  /*access policy component */
  getAccountInfo() {
    this.$dashAccountService.get((err, res: { status: number, data: { resource_id: string, name: string } }) => {
      if (err) {
        this.$dashUtilsSvc.openSnackBar('Retrieving account data failed.. Try again later');
        return;
      }
      this.members.push({
        entity_id: res.data.resource_id,
        name: res.data.name,
        access: 'Control',
        not_removable: true
      });
      this.membersDS = new MatTableDataSource<MemberProfile>(this.members);
      console.log('Members: ' + JSON.stringify(this.members));
    });
  }

  addNewPolicyMember() {
    const groupValue = this.accessForm.get('groupCtrl').value;
    const foundMemberGroup = this.memberGroupOptions.find(mG => mG[this.searchOption] === groupValue);
    if (!foundMemberGroup) {
      return;
    }

    this.members.push({
      entity_id: foundMemberGroup.entity_id,
      name: foundMemberGroup.name,
      access: this.accessOption
    });
    // scoate-l din lista de sugestii ca e deja adaugat
    this.membersDS = new MatTableDataSource<MemberProfile>(this.members);

    this.accessForm.get('groupCtrl').reset();
    this.triggerPipeUpdate = !this.triggerPipeUpdate;
  }

  removePolicyMember(element: any) {
    const idx = this.members.findIndex(el => _.isEqual(el, element));
    this.members.splice(idx, 1);
    this.membersDS = new MatTableDataSource<MemberProfile>(this.members);
    this.triggerPipeUpdate = !this.triggerPipeUpdate;
  }

  getAllUsersAndGroups() {
    this.$dashAccountService.getAll((err, res) => {
      if (err) {
        this.$dashUtilsSvc.openSnackBar('There was an error retrieving available users.. Try again later');
        return;
      }
      this.memberGroupOptions = res.data.map((el) => {
        return {entity_id: el.resource_id, name: el.name};
      });
      this.$dashAccountService.getGroups((err2, res2) => {
        if (err2) {
          this.$dashUtilsSvc.openSnackBar('Retrieving groups for user failed.. Try again later');
          return;
        }
        this.memberGroupOptions = this.memberGroupOptions.concat(res2.data.map((el) => {
          return {entity_id: el.resource_id, name: el.group_name};
        }));
      });

    });
  }


}
