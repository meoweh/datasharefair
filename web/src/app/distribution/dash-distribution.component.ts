import { Component } from '@angular/core';
import { DashDistributionService } from '../services/dash-distribution.service';
import { DashUtilsService } from '../services/dash-utils.service';
import { FileUploaderCustom } from '../shared/file-uploader-custom';

@Component({
  selector: 'dash-distribution',
  templateUrl: './dash-distribution.component.html',
  styleUrls: ['./dash-distribution.component.scss'],
})
export class DashDistributionComponent {

  constructor(
    private $dashUtilsSvc: DashUtilsService,
    private $dashDistributionSvc: DashDistributionService,
  ) {
  }

  createDistribution({uploader, metadata, access}: { uploader: FileUploaderCustom, metadata: any, access: any }) {
    const notUploadedItems = uploader.getNotUploadedItems();
    if (!uploader.getNotUploadedItems().length) {
      this.$dashUtilsSvc.openSnackBar('You need to choose minimum 1 file for your distribution!');
      return;
    }
    // upload all files using CSB
    const data = {...metadata};
    data.accessRights = access.map((item) => {
      return {
        entityId: item.entity_id,
        accessLevel: item.access,
      };
    });

    uploader.uploadAllFiles();


    uploader.response.subscribe((respRaw) => {
      let resp;
      try {
        resp = JSON.parse(respRaw);
      } catch (e) {
        this.$dashUtilsSvc.openSnackBar('There was an error creating the distribution archive!');
        return;
      }
      if (resp.status === 1) {
        this.$dashUtilsSvc.openSnackBar('There was an error creating the distribution archive!');
        return;
      }
      data.downloadURL = resp.url;
      this.$dashDistributionSvc.createDistribution(data).then((result: { status: number, distribution_id: string }) => {
        if (result.status) {
          this.$dashUtilsSvc.openSnackBar('There was a problem creating the distribution');
          return;
        }
        this.$dashDistributionSvc.saveEncryptionKey({key: resp.key, distribution_id: result.distribution_id}).then((result: { status: number }) => {
          if (result.status) {
            this.$dashUtilsSvc.openSnackBar('There was a problem saving the encryption key');
            return;
          }
        });
        this.$dashUtilsSvc.openSnackBar('Distribution created successfully');
      });
    });


  }

}
