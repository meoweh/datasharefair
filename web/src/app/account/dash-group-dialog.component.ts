import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material';

export interface NewGroupData {
  groupName: string;
  groupDescription: string;
}

@Component({
  selector: 'dash-group-dialog',
  templateUrl: './dash-group-dialog.component.html',
  styleUrls: ['./dash-group-dialog.component.scss']
})
export class DashGroupDialogComponent {
  public data: NewGroupData = {
    groupName: '',
    groupDescription: ''
  };

  constructor(
    public dialogRef: MatDialogRef<DashGroupDialogComponent>) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
