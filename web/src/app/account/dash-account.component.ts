import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {GroupDescription, LimitedMemberDesc, MemberDescription} from '../../assets/mocked-data';
import {DashUtilsService} from '../services/dash-utils.service';
import {DashAccountService} from '../services/dash-account.service';
import {CookieService} from 'ngx-cookie-service';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {MatDialog} from '@angular/material';
import {DashGroupDialogComponent} from './dash-group-dialog.component';



@Component({
  selector: 'dash-account',
  templateUrl: './dash-account.component.html',
  styleUrls: ['./dash-account.component.scss']
})
export class DashAccountComponent implements OnInit {


  emailForm: FormGroup;
  accountForm: FormGroup;
  nameForm: FormGroup;
  addressForm: FormGroup;
  idForm: FormGroup;
  regionForm: FormGroup;
  certificate: string;
  public groupsData: Array<GroupDescription>;
  displayedColumns: string[] = ['company_id', 'name', 'category', 'region', 'actions'];
  groupForm: FormGroup;
  memberGroupOptions: Array<LimitedMemberDesc>;
  memberNamesOptions: Observable<string[]>;
  memberIDOptions: Observable<string[]>;
  memberNames: string[];
  memberID: string[];
  searchCrit: any = [{
    viewValue: 'VAT/TIN_ID',
    value: 'company_id'
  },
    {
      viewValue: 'Company Name',
      value: 'name'
    }];
  searchOption = 'company_id';
  groupDescription: string;
  groupName: string;
  private reqBody: { name: FormGroup; email: FormGroup, address: FormGroup, certificate?: string };
  private categoryForm: FormGroup;

  // tslint:disable-next-line:variable-name
  private accountInfo: MemberDescription;
  constructor(private _formBuilder: FormBuilder, public dialog: MatDialog, private $cookies: CookieService, private $router: Router, private UtilsService: DashUtilsService, private AccountService: DashAccountService) {
  }

  ngOnInit() {
    this.groupForm = this._formBuilder.group({
      groupCtrl: ['', Validators.nullValidator]
    });
    this.nameForm = this._formBuilder.group({
      nameCtrl: ['', Validators.nullValidator]
    });
    this.emailForm = this._formBuilder.group({
      emailCtrl: ['', Validators.nullValidator]
    });
    this.addressForm = this._formBuilder.group({
      addressCtrl: ['', Validators.nullValidator]
    });
    this.categoryForm = this._formBuilder.group({
      categoryCtrl: [{
        value: 'AAAAA',
        disabled: true
      }, Validators.nullValidator]
    });
    this.idForm = this._formBuilder.group({
      idCtrl: [{
        value: 'AAAAA',
        disabled: true
      }, Validators.nullValidator]
    });
    this.regionForm = this._formBuilder.group({
      regionCtrl: [{
        value: 'AAAAA',
        disabled: true
      }, Validators.nullValidator]
    });

    this.getAccountInfo();
    this.getGroupsForUser();
    this.getAllUsers();

    // tslint:disable-next-line:no-non-null-assertion
    this.memberNamesOptions = this.groupForm.get('groupCtrl')!.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterGroup(value))
      );
    // tslint:disable-next-line:no-non-null-assertion
    this.memberIDOptions = this.groupForm.get('groupCtrl')!.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterGroup(value))
      );
  }

  onKeyFileChange($event: File | FileList) {
    this.UtilsService.readFileAsBase64($event)
      .then((value: string) => {
        this.certificate = value;
      });
  }

  clearKeyFile() {
    this.certificate = '';
  }

  getAccountInfo() {
    this.AccountService.get((err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('Retrieving account data failed.. Try again later');
        return;
      }
      this.accountInfo = res.data;
      this.idForm.get('idCtrl').patchValue(res.data.company_id);
      this.nameForm.get('nameCtrl').patchValue(res.data.name);
      this.emailForm.get('emailCtrl').patchValue(res.data.email);
      this.addressForm.get('addressCtrl').patchValue(res.data.address);
      this.categoryForm.get('categoryCtrl').patchValue(res.data.category);
      this.regionForm.get('regionCtrl').patchValue(res.data.region);
    });
  }

  updateAccount() {

    this.reqBody = {
      name: this.nameForm.value.nameCtrl,
      email: this.emailForm.value.emailCtrl,
      address: this.addressForm.value.addressCtrl
    };

    if (this.certificate) {
      this.reqBody.certificate = this.certificate;
    }

    this.AccountService.update(this.reqBody, (err) => {
      if (err) {
        this.UtilsService.openSnackBar('Updating account failed.. Try again later');
        return;
      }
      this.UtilsService.openSnackBar('Updating account successfully');
    });
  }

  deleteAccount() {
    this.AccountService.delete((err) => {
      if (err) {
        this.UtilsService.openSnackBar('Deleting account failed.. Try again later');
        return;
      }
      this.$cookies.delete('user_token');
      this.$router.navigate(['/login']).then();
    });
  }

  getGroupsForUser() {
    this.AccountService.getGroups((err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('Retrieving groups for user failed.. Try again later');
        return;
      }
      this.groupsData = res.data;
    });
  }

  getAllUsers() {
    this.AccountService.getAll((err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('There was an error retrieving available users.. Try again later');
        return;
      }
      this.memberGroupOptions = res.data;
      this.memberGroupOptions = this.memberGroupOptions.filter(member => member.company_id !== this.accountInfo.company_id);
      this.memberNames = this.memberGroupOptions.map(el => el.name);
      this.memberID = this.memberGroupOptions.map(el => el.company_id);
    });
  }

  deleteGroup(group) {
    this.AccountService.deleteGroup({group_id: group.group_id}, (err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('Removing group failed.. Try again later');
        return;
      }
      this.UtilsService.openSnackBar('Removed group successfully');
      this.getGroupsForUser();
    });

  }

  updateGroup(group: GroupDescription, element: any, operation: string) {
    if (operation === 'delete') {
      const req = {
        group_id: group.group_id,
        company_id: element.company_id
      };
      console.log(req);
      this.AccountService.deleteGroupMember(req, (err, res) => {
        if (err) {
          this.UtilsService.openSnackBar('Removing group member failed.. Try again later');
          return;
        }
        this.UtilsService.openSnackBar('Removed group member successfully');
        this.getGroupsForUser();
      });
    } else {
      let myEl;
      if (!this.searchOption) {
        this.UtilsService.openSnackBar('Please select a search criteria');
        return;
      }
      if (this.searchOption === 'name') {
        myEl = this.memberGroupOptions.filter(el => el.name === this.groupForm.value.groupCtrl);
      } else {
        myEl = this.memberGroupOptions.filter(el => el.company_id === this.groupForm.value.groupCtrl);
      }
      if (!myEl || !myEl.length) {
        this.UtilsService.openSnackBar('No user found to match your search');
        return;
      }
      const req = {
        group_id: group.group_id,
        company_id: myEl[0].company_id
      };
      console.log(req);
      this.AccountService.addGroupMember(req, (err, res) => {
        if (err) {
          this.UtilsService.openSnackBar('Adding group member failed.. Try again later');
          return;
        }
        this.UtilsService.openSnackBar('Added group member successfully');
        this.getGroupsForUser();
      });
    }
  }

  openDialog() {
    const dialogRef = this.dialog.open(DashGroupDialogComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(data => {
      console.log('The dialog was closed');
      if (data && data.hasOwnProperty('groupName') && data.hasOwnProperty('groupDescription') && data.groupName !== '' && data.groupDescription !== '') {
        this.AccountService.createGroup({
          group_name: data.groupName,
          description: data.groupDescription,
          creator: this.accountInfo.company_id,
          members: [this.accountInfo.company_id]
        }, (err: any, res: any) => {
          if (err) {
            this.UtilsService.openSnackBar('There was an error creating a group..Try again later');
            return;
          }
          this.UtilsService.openSnackBar('Creating group finished');
          this.getGroupsForUser();
        });

      }
    });
  }

  private _filterGroup(value: string): string[] {
    if (this.searchOption === 'name') {
      if (value) {
        return DashUtilsService.filter(this.memberNames, value);
      }
      return this.memberNames;
    } else {
      if (value) {
        return DashUtilsService.filter(this.memberID, value);
      }

      return this.memberID;
    }
  }
}

