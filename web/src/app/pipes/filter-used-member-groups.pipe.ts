import {Pipe, PipeTransform} from '@angular/core';
import {MemberGroup} from '../distribution/distribution-access.component';
import {AccessPolicy} from '../../assets/mocked-data';

function isMemberGroup(value): value is MemberGroup {
  return !!value.entity_id;
}

function isAccessPolicy(value): value is AccessPolicy {
  return !!value.entityId;
}

@Pipe({
  name: 'filterUsedMemberGroups'
})
export class FilterUsedMemberGroupsPipe implements PipeTransform {

  transform(groups: Array<MemberGroup>, used: Array<MemberGroup | AccessPolicy>, triggerUpdate?: boolean): Array<MemberGroup | AccessPolicy> {
    return groups.filter(g => {
      // get group members which aren't found in used array
      return !used.find(uG => {
        if (isMemberGroup(uG)) {
          return uG.entity_id === g.entity_id && uG.name === g.name;
        }
        if (isAccessPolicy(uG)) {
          return uG.entityId === g.entity_id && uG.entityName === g.name;
        }
      });
    });
  }

}
