import { Pipe, PipeTransform } from '@angular/core';
import { MemberGroup } from '../distribution/distribution-access.component';

@Pipe({
  name: 'filterByProperty',
})
export class FilterByPropertyPipe implements PipeTransform {

  transform(values: Array<MemberGroup>, valueToFilter: string, propToFilter: keyof MemberGroup): Array<MemberGroup> {
    const filterValue = (valueToFilter || '').toLowerCase();
    return values.filter(item => this.getMemberProp(item, propToFilter).toLowerCase().startsWith(filterValue));
  }

  private getMemberProp(item: MemberGroup, prop: keyof MemberGroup): string {
    const value = item[prop];
    if (prop !== 'entity_id') {
      return value;
    }
    if (value.startsWith('<http')) {
      // split by /, take last item, remove last character >
      return value.split('/').slice(-1)[0].slice(0, -1);
    }
    // some initial member groups were without IRI
    return value;
  }

}
