import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Chart} from 'chart.js';
import {DashUtilsService} from './services/dash-utils.service';
import {DatasetWithDistributionsUserAccess} from '../assets/mocked-data';
import {DashHomeService} from './services/dash-home.service';

interface MyChartOptions {
  datasets: { backgroundColor: (string)[]; borderColor: string; data: number[]; borderWidth: number }[];
  labels: any[];
}

@Component({
  selector: 'dash-home',
  templateUrl: './dash-home.component.html',
  styleUrls: ['./dash-home.component.scss']
})
export class DashHomeComponent implements OnInit {
  @ViewChild('categoryCanvas', {static: true}) el: ElementRef<HTMLCanvasElement>;
  @ViewChild('licenseCanvas', {static: true}) elEnv: ElementRef<HTMLCanvasElement>;
  public popularItems: Array<DatasetWithDistributionsUserAccess>;
  private showProperties: boolean;
  private chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
  };
  private licenseData: MyChartOptions = {
    labels: [],
    datasets: [{
      backgroundColor: [
        this.chartColors.red,
        this.chartColors.green,
        this.chartColors.blue,
        this.chartColors.purple,
        this.chartColors.orange
      ],
      borderColor: this.chartColors.grey,
      borderWidth: 2,
      data: []
    }],
  };
  private categoryData: MyChartOptions = {
    labels: [],
    datasets: [{
      data: [],
      backgroundColor: [
        '#C13FAC',
        '#F72C25',
        '#ABD8AC',
        '#FEC925',
        this.chartColors.purple,
        this.chartColors.orange,
        this.chartColors.red,
        this.chartColors.green,
        this.chartColors.blue,
      ],
      borderWidth: 2,
      borderColor: this.chartColors.grey
    }],
  };
  private categoryChart: any;
  private licenseChart: any;

  constructor(private HomeService: DashHomeService, private UtilsService: DashUtilsService) {
  }

  renderCharts() {

    this.licenseChart = new Chart(this.el.nativeElement, {
      data: this.licenseData,
      type: 'bar',
      options: {
        legend: {
          display: false
        },
        title: {
          fontSize: 14,
          padding: 20,
          display: true,
          text: 'Number of distributions by sharing license agreement'
        },
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [
            {
              ticks: {
                suggestedMin: 0
              }
            }
          ]
        }
      },
    });

    this.categoryChart = new Chart(this.elEnv.nativeElement, {
      data: this.categoryData,
      type: 'pie',
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          position: 'right'
        },
        title: {
          fontSize: 14,
          padding: 20,
          display: true,
          text: 'Number of distributions by targeted category'
        },
      }
    });

    this.HomeService.retrieveStats((err, res) => {
      if (err) {
        console.error(err);
        this.UtilsService.openSnackBar('Retrieving chart data failed. Try again later.');
        return;
      }
      this.categoryData.labels = res.categoryData.labels;
      this.categoryData.datasets[0].data = res.categoryData.data;

      this.licenseData.labels = res.licenseData.labels;
      this.licenseData.datasets[0].data = res.licenseData.data;
      this.categoryChart.update();
      this.licenseChart.update();
    });

  }

  renderPopular() {
    this.HomeService.renderPopular((err, res) => {
      if (err) {
        console.error(err);
        this.UtilsService.openSnackBar('Retrieving popular datasets failed. Try again later.');
        return;
      }
      this.popularItems = res.datasets;
      // this.dataSource = res.datasets.map((dataset) => {
      //   return {
      //     url: dataset.url,
      //     access: dataset.user_access,
      //     title: dataset.title
      //   };
      // });
    });
  }

  objectkeys(obj) {
    const keys = Object.keys(obj);
    return keys.filter((key) => key !== 'description');
  }

  ngOnInit() {
    this.showProperties = false;
    this.renderCharts();
    this.renderPopular();
  }


}
