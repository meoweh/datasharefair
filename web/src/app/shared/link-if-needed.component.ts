import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'link-if-needed',
  templateUrl: './link-if-needed.component.html',
  styleUrls: ['./link-if-needed.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LinkIfNeededComponent implements OnInit {

  @Input() propertyValue: string;

  constructor() {
  }

  ngOnInit() {
  }

  public isLink() {
    if (typeof this.propertyValue !== 'string') {
      console.log(this.propertyValue);
      return;
    }
    return this.propertyValue.trim().startsWith('http');
  }

}
