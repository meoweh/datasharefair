import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl } from '@angular/forms';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Observable, of } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { debounceTime, startWith, switchMap } from 'rxjs/operators';
import { DashDatasetService, SearchThemeResult } from '../services/dash-dataset.service';

@Component({
  selector: 'dash-themes-form',
  templateUrl: './dash-themes-form.component.html',
  styleUrls: ['./dash-themes-form.component.scss'],
})
export class DashThemesFormComponent implements OnInit {
  @Input() themesFA: FormArray;

  public themeSearchInputFC = new FormControl();
  public searchedThemes$: Observable<Array<SearchThemeResult>>;

  @ViewChild('themeSearchInput', {static: true}) themeSearchInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: true}) matAutocomplete: MatAutocomplete;

  constructor(
    private $dashDatasetService: DashDatasetService,
  ) {
    this.searchedThemes$ = this.themeSearchInputFC.valueChanges.pipe(
      startWith(null),
      debounceTime(300),
      switchMap((themeSearch: string | null) => {
        console.log('search');
        if (themeSearch) {
          return fromPromise(this.$dashDatasetService.searchTheme(themeSearch).catch(e => {
            console.error(e);
            return [];
          }));
        }
        return of([] as Array<SearchThemeResult>);
      }),
    );
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.themesFA.push(new FormControl(event.option.value));
    this.themeSearchInput.nativeElement.value = '';
    this.themeSearchInputFC.setValue(null);
  }

  ngOnInit(): void {
  }

  removeTheme(themeIndex: number) {
    this.themesFA.removeAt(themeIndex);
  }
}
