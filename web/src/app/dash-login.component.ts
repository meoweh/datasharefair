import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {DashLoginService} from './services/dash-login.service';
import {DashUtilsService} from './services/dash-utils.service';
import {environment} from '../environments/environment'

@Component({
  selector: 'dash-login',
  templateUrl: './dash-login.component.html',
  styleUrls: ['./dash-login.component.scss']
})
export class DashLoginComponent {


  authFormModel = {
    CIF: '',
    certificate: ''
  };
  private req: { certificate: string; company_id: string };

  constructor(private $http: HttpClient, private $cookies: CookieService, private $router: Router, private SessionService: DashLoginService, private UtilsService: DashUtilsService) {
  }

  goSignUp() {
    this.$router.navigate(['/register'])
      .then();
  }

  uploadCertificate(certificate) {
    return certificate;
  }

  goHome() {

    const password = this.uploadCertificate(this.authFormModel.certificate);
    this.req = {
      company_id: this.authFormModel.CIF,
      certificate: password
    };

    console.log(this.req);

    // call login service
    this.SessionService.login(this.req, (err, res) => {
      if (err) {
        console.error(err);
        this.UtilsService.openSnackBar('Login failed. Try again later.');
        return;
      }
      const dateNow = new Date();
      dateNow.setHours(dateNow.getHours() + 2);
      this.$cookies.set('user_token', res.user_token, dateNow, '/', environment.hostIp, false, 'Lax');
      // if there's time for home, change here
      this.$router.navigate(['/marketplace']).then(console.log).catch(console.error);
    });
  }

  onKeyFileChange($event: File | FileList) {
    this.UtilsService.readFileAsBase64($event)
      .then((value: string) => {
        this.authFormModel.certificate = value;
      });
  }

  clearKeyFile() {
    this.authFormModel.certificate = '';
  }
}
