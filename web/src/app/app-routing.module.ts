import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashLoginComponent} from './dash-login.component';
import {DashRegisterComponent} from './dash-register.component';
import {DashMainComponent} from './dash-main.component';
import {DashHomeComponent} from './dash-home.component';
import {DashAccountComponent} from './account/dash-account.component';
import {DashDatasetsManagementComponent} from './management/dash-datasets-management.component';
import {DashDistributionComponent} from './distribution/dash-distribution.component';
import {DashMarketplaceComponent} from './marketplace/dash-marketplace.component';
import {IsLoggedOutGuard} from './guards/is-logged-out.guard';
import {IsLoggedInGuard} from './guards/is-logged-in.guard';


const routes: Routes = [
  {
    path: 'login',
    pathMatch: 'full',
    component: DashLoginComponent,
    canActivate: [IsLoggedOutGuard]
  },
  {
    path: 'register',
    pathMatch: 'full',
    component: DashRegisterComponent,
    canActivate: [IsLoggedOutGuard]
  },
  {
    path: '',
    component: DashMainComponent,
    canActivate: [IsLoggedInGuard],
    children: [
      // {
      //   path: 'home',
      //   component: DashHomeComponent
      // },
      {
        path: 'account',
        component: DashAccountComponent
      },
      {
        path: 'datasets',
        component: DashDatasetsManagementComponent
      },
      {
        path: 'distribution',
        component: DashDistributionComponent
      },
      {
        path: 'marketplace',
        component: DashMarketplaceComponent
      },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
