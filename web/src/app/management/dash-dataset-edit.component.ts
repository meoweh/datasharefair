import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, Inject } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { countriesMap } from '../../assets/countries';
import { licenseMap } from '../../assets/licenses';
import { AccessPolicy, Catalog, Dataset } from '../../assets/mocked-data';
import { DashAccountService } from '../services/dash-account.service';
import { DashUtilsService } from '../services/dash-utils.service';

export interface NewDatasetAPI {
  datasetCatalog: string;
  datasetName: string;
  datasetDescription: string;
  datasetVersion: string;
  datasetLanguage: string;
  datasetLicense: string;
  datasetKeywords: Array<string>;
  datasetTheme: Array<string>;
  accessRights: Array<AccessPolicy>;
}

export interface DatasetFormValue {
  catalog: string;
  title: string;
  description: string;
  version: string;
  language: string;
  license: string;
  keywords: Array<string>;
  categories: Array<string>;
}

@Component({
  selector: 'dash-dataset-edit',
  templateUrl: './dash-dataset-edit.component.html',
  styleUrls: ['./dash-dataset-edit.component.scss'],
})
export class DashDatasetEditComponent {
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  public requiredErrorMessage = 'This field is required!';
  public countries = countriesMap;
  public licenses = Object.values(licenseMap);

  public datasetFG: FormGroup;
  private userData: any;

  constructor(private $accountSvc: DashAccountService,
              private $formBuilder: FormBuilder,
              private $dialogRef: MatDialogRef<DashDatasetEditComponent>,
              @Inject(MAT_DIALOG_DATA) public dataO: { data?: Dataset, catalogs?: Array<Catalog> }) {
    this.$accountSvc.get((err, res) => {
      this.userData = res.data;
    });
    if (!this.dataO.data) {
      this.datasetFG = this.$formBuilder.group({
        catalog: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
        title: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
        description: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
        version: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
        language: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
        license: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
        keywords: this.$formBuilder.array([], Validators.required),
        categories: this.$formBuilder.array([], Validators.required),
      });
      return;
    }

    this.datasetFG = this.$formBuilder.group({
      dataset_id: this.$formBuilder.control(this.dataO.data.dataset_id, Validators.required),
      title: this.$formBuilder.control(this.dataO.data.title, Validators.required), //
      publisher: this.$formBuilder.control({value: this.dataO.data.publisher, disabled: true}, Validators.required),
      description: this.$formBuilder.control(this.dataO.data.description, Validators.required), //
      version: this.$formBuilder.control(this.dataO.data.version, Validators.required), //
      language: this.$formBuilder.control(this.dataO.data.language, Validators.required), //
      license: this.$formBuilder.control(this.dataO.data.license, Validators.required), //
      created: this.$formBuilder.control({value: this.dataO.data.created, disabled: true}, Validators.required),
      modified: this.$formBuilder.control({value: this.dataO.data.modified, disabled: true}, Validators.required),
      accessRights: this.$formBuilder.array(this.dataO.data.accessRights, Validators.required),
      catalog: this.$formBuilder.control({value: this.dataO.data.catalog, disabled: true}, Validators.required),
      categories: this.$formBuilder.array([], Validators.required), // aka themes
      keywords: this.$formBuilder.array([], Validators.required), // aka themes
    });

    this.dataO.data.categories.forEach(category => this.addTheme({input: {value: category}, value: category}));
    this.dataO.data.keywords.forEach(keyword => this.addKeyword({input: {value: keyword}, value: keyword}));

  }

  onNoClick(): void {
    this.$dialogRef.close();
  }

  removeTheme(themeIndex: number) {
    (this.datasetFG.get('categories') as FormArray).removeAt(themeIndex);
  }

  addTheme(event: { input: { value: string }, value: string }): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      (this.datasetFG.get('categories') as FormArray).push(this.$formBuilder.control(value));
    }

    if (input) {
      input.value = '';
    }
  }

  removeKeyword(keywordIndex: number) {
    (this.datasetFG.get('keywords') as FormArray).removeAt(keywordIndex);
  }

  addKeyword(event: { input: { value: string }, value: string }) {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      (this.datasetFG.get('keywords') as FormArray).push(this.$formBuilder.control(value));
    }

    if (input) {
      input.value = '';
    }
  }

  toAPI(): NewDatasetAPI {
    if (this.dataO.data) {
      return this.datasetFG.getRawValue();
    }
    const formValue: DatasetFormValue = this.datasetFG.value;
    return {
      datasetCatalog: formValue.catalog,
      datasetName: formValue.title,
      datasetDescription: formValue.description,
      datasetVersion: formValue.version,
      datasetLanguage: formValue.language,
      datasetLicense: formValue.license,
      datasetTheme: formValue.categories,
      datasetKeywords: formValue.keywords,
      accessRights: [
        {
          entityId: this.userData && this.userData.resource_id ? this.userData.resource_id : '',
          entityName: this.userData && this.userData.name ? this.userData.name : '',
          accessLevel: 'Control',
          status: 'approved',
        },
      ],
    };
  }
}
