import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Catalog, Dataset, Distribution, MemberDescription } from '../../assets/mocked-data';
import { DashAccountService } from '../services/dash-account.service';
import { DashDatasetService } from '../services/dash-dataset.service';
import { DashDistributionService } from '../services/dash-distribution.service';
import { DashUtilsService } from '../services/dash-utils.service';
import { DashCatalogEditComponent } from './dash-catalog-edit.component';
import { DashDatasetEditComponent } from './dash-dataset-edit.component';
import { DashDistributionEditComponent, DistributionEdit } from './dash-distribution-edit.component';

@Component({
  selector: 'dash-datasets',
  templateUrl: './dash-datasets-management.component.html',
  styleUrls: ['./dash-datasets-management.component.scss'],
})
export class DashDatasetsManagementComponent implements OnInit {

  public catalogs: Array<Catalog>;
  public datasets: Array<Dataset> = [];
  public distributions: Array<Distribution> = [];
  public displayedColumns: Array<string> = ['name', 'url', 'mediaType', 'actions'];
  private userData: MemberDescription;

  constructor(
    private $dashDatasetService: DashDatasetService, private $dashAccountService: DashAccountService, private $dashUtilsService: DashUtilsService,
    private $dashDistributionService: DashDistributionService, private $dialog: MatDialog,
  ) {
    this.updateCatalogs();

    this.$dashAccountService.get((err, res) => {
      if (err) {
        this.$dashUtilsService.openSnackBar('Cannot retrieve user data');
        return;
      }
      this.userData = res.data;
    });
  }

  ngOnInit() {
  }

  catalogPanelExpanded(catalog: Catalog) {
    this.updateDatasets(catalog);
  }

  datasetPanelExpanded(dataset: Dataset) {
    this.distributions = [];
    this.updateDistributions(dataset.dataset_id);
  }

  editDistribution(element: Distribution) {
    const dialogRef = this.$dialog.open(DashDistributionEditComponent, {
      width: '900px',
      height: '900px',
      data: element,
    });

    dialogRef.afterClosed().subscribe((data: DistributionEdit) => {
      console.log(data);
      if (!data) {
        return;
      }
      this.$dashDistributionService.editDistribution({
        ...data,
        distribution_id: element.distribution_id,
        dataset: element.dataset
      })
        .then((result: { status: number }) => {
          if (result.status === 0) {
            this.$dashUtilsService.openSnackBar('Distribution edited successfully!');
            this.updateDistributions(element.dataset);
            return;
          }
          this.$dashUtilsService.openSnackBar('There was an issue editing the distribution!');
        });
    });
  }

  deleteDistribution(element: Distribution) {
    this.$dashDistributionService.deleteDistribution(element).then((res1) => {
      this.$dashUtilsService.openSnackBar('Distribution deleted successfully!');
      this.updateDistributions(element.dataset);
    }).catch((err) => {
      this.$dashUtilsService.openSnackBar('Deleting distribution failed');
    });
  }

  editCatalog(catalog: Catalog) {
    const dialogRef = this.$dialog.open(DashCatalogEditComponent, {
      width: '900px',
      height: '900px',
      data: catalog,
    });

    dialogRef.afterClosed().subscribe((data: Catalog) => {
      console.log(data);
      if (!data) {
        return;
      }
      this.$dashDatasetService.editCatalog(data)
        .then((result: { status: number }) => {
          if (result.status === 0) {
            this.$dashUtilsService.openSnackBar('Catalog edited successfully!');
            this.updateCatalogs();
            return;
          }
          this.$dashUtilsService.openSnackBar('There was an issue editing the catalog!');
        });
    });
  }

  editDataset(catalog: Catalog, dataset: Dataset) {
    console.log('Editing dataset');
    const dialogRef = this.$dialog.open(DashDatasetEditComponent, {
      width: '900px',
      height: '900px',
      data: {
        data: dataset,
      },
    });

    dialogRef.afterClosed().subscribe((data: Dataset) => {
      console.log(data);
      if (!data) {
        return;
      }
      this.$dashDatasetService.editDataset(data)
        .then((result: { status: number }) => {
          if (result.status === 0) {
            this.$dashUtilsService.openSnackBar('Dataset edited successfully!');
            this.updateDatasets(catalog);
            return;
          }
          this.$dashUtilsService.openSnackBar('There was an issue editing the dataset!');
        });
    });
  }

  private updateDatasets(catalog: Catalog) {
    this.datasets = [];
    this.$dashDatasetService.getDatasetsForCatalog(catalog.catalog_id)
      .then(res => this.datasets = res);
  }

  private updateCatalogs() {
    this.$dashDatasetService.getCatalogsForUser()
      .then(res => this.catalogs = res);
  }

  private updateDistributions(dataset_id: string) {
    this.$dashDatasetService.getDistributionForDataset(dataset_id)
      .then((res) => {
        this.distributions = res;
        console.log(this.distributions);
      });
  }
}
