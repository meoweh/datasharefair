import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { countriesMap } from '../../assets/countries';
import { licenseMap } from '../../assets/licenses';
import { Distribution } from '../../assets/mocked-data';

export interface DistributionEdit {
  title: string;
  publisher: string;
  description: string;
  version: string;
  language: string;
  license: string;
  url: string;
  mediaType: string;
  label: string;
  created: string;
  modified: string;
  accessRights: Array<any>; // need to implement this interface
}

@Component({
  selector: 'dash-distribution-edit',
  templateUrl: './dash-distribution-edit.component.html',
  styleUrls: ['./dash-distribution-edit.component.scss'],
})
export class DashDistributionEditComponent {

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  public requiredErrorMessage = 'This field is required!';
  public countries = countriesMap;
  public licenses = Object.values(licenseMap);

  public distributionFG: FormGroup;

  constructor(
    private $formBuilder: FormBuilder,
    private $dialogRef: MatDialogRef<DashDistributionEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Distribution,
  ) {

    this.distributionFG = this.$formBuilder.group({
      title: this.$formBuilder.control(this.data.title, Validators.required),
      publisher: this.$formBuilder.control({value: this.data.publisher, disabled: true}, Validators.required),
      description: this.$formBuilder.control(this.data.description, Validators.required),
      version: this.$formBuilder.control(this.data.version, Validators.required),
      language: this.$formBuilder.control(this.data.language, Validators.required),
      license: this.$formBuilder.control(this.data.license, Validators.required),
      url: this.$formBuilder.control({value: this.data.url, disabled: true}, Validators.required),
      mediaType: this.$formBuilder.control({value: this.data.mediaType, disabled: true}, Validators.required),
      label: this.$formBuilder.control(this.data.label, Validators.required),
      created: this.$formBuilder.control({value: this.data.created, disabled: true}, Validators.required),
      modified: this.$formBuilder.control({value: this.data.modified, disabled: true}, Validators.required),
      accessRights: this.$formBuilder.array(this.data.accessRights, Validators.required),
    });

    console.log(this.data);

  }

  onNoClick(): void {
    this.$dialogRef.close();
  }

}
