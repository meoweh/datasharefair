import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, Inject } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { countriesMap } from '../../assets/countries';
import { licenseMap } from '../../assets/licenses';
import { AccessPolicy, Catalog, MemberDescription } from '../../assets/mocked-data';
import { DashAccountService } from '../services/dash-account.service';
import { DashUtilsService } from '../services/dash-utils.service';

export interface NewCatalogAPI {
  catalogName: string;
  catalogDescription: string;
  catalogVersion: string;
  catalogLanguage: string;
  catalogLicense: string;
  catalogTheme: Array<string>;
  accessRights: Array<AccessPolicy>;
}

export interface CatalogFormValue {
  title: string;
  description: string;
  version: string;
  language: string;
  license: string;
  categories: Array<string>;
}

@Component({
  selector: 'dash-catalog-edit',
  templateUrl: './dash-catalog-edit.component.html',
  styleUrls: ['./dash-catalog-edit.component.scss'],
})
export class DashCatalogEditComponent {

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  public requiredErrorMessage = 'This field is required!';
  public countries = countriesMap;
  public licenses = Object.values(licenseMap);

  public catalogFG: FormGroup;
  private userData: MemberDescription;

  constructor(
    private $formBuilder: FormBuilder,
    private $accountSvc: DashAccountService,
    private $dialogRef: MatDialogRef<DashCatalogEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Catalog,
  ) {
    this.$accountSvc.get((err, res) => {
      this.userData = res.data;
    });

    if (!data) {
      this.catalogFG = this.$formBuilder.group({
        title: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
        description: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
        version: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
        language: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
        license: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
        categories: this.$formBuilder.array([], Validators.required),
      });
      return;
    }

    this.catalogFG = this.$formBuilder.group({
      catalog_id: this.$formBuilder.control(this.data.catalog_id, Validators.required),
      title: this.$formBuilder.control(this.data.title, Validators.required), //
      publisher: this.$formBuilder.control({value: this.data.publisher, disabled: true}, Validators.required),
      description: this.$formBuilder.control(this.data.description, Validators.required), //
      version: this.$formBuilder.control(this.data.version, Validators.required), //
      language: this.$formBuilder.control(this.data.language, Validators.required), //
      license: this.$formBuilder.control(this.data.license, Validators.required), //
      created: this.$formBuilder.control({value: this.data.created, disabled: true}, Validators.required),
      modified: this.$formBuilder.control({value: this.data.modified, disabled: true}, Validators.required),
      accessRights: this.$formBuilder.array(this.data.accessRights, Validators.required),
      datasets: this.$formBuilder.control(this.data.datasets), // not required because initial catalog has no datasets
      categories: this.$formBuilder.array([], Validators.required), // aka themes
    });

    this.data.categories.forEach(category => this.addTheme({input: {value: category}, value: category}));
  }

  onNoClick(): void {
    this.$dialogRef.close();
  }

  removeTheme(themeIndex: number) {
    (this.catalogFG.get('categories') as FormArray).removeAt(themeIndex);
  }

  addTheme(event: { input: { value: string }, value: string }): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      (this.catalogFG.get('categories') as FormArray).push(this.$formBuilder.control(value));
    }

    if (input) {
      input.value = '';
    }
  }

  toAPI(): NewCatalogAPI {
    if (this.data) {
      return this.catalogFG.getRawValue();
    }
    const formValue: CatalogFormValue = this.catalogFG.value;
    return {
      catalogName: formValue.title,
      catalogDescription: formValue.description,
      catalogVersion: formValue.version,
      catalogLanguage: formValue.language,
      catalogLicense: formValue.license,
      catalogTheme: formValue.categories,
      accessRights: [
        {
          entityId: this.userData && this.userData.resource_id ? this.userData.resource_id : '',
          entityName: this.userData && this.userData.name ? this.userData.name : '',
          accessLevel: 'Control',
          status: 'approved'
        }
      ]
    };
  }
}
