import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MemberGroup } from '../distribution/distribution-access.component';
import { DashAccountService } from '../services/dash-account.service';
import { DashUtilsService } from '../services/dash-utils.service';

@Component({
  selector: 'dash-access-rights-edit',
  templateUrl: './dash-access-rights-edit.component.html',
  styleUrls: ['./dash-access-rights-edit.component.scss'],
})
export class DashAccessRightsEditComponent implements OnInit {
  displayedColumns = ['entity', 'access', 'status', 'actions'];
  @Input() item: FormArray;
  public triggerPipeUpdate = false;
  public memberGroupOptions: Array<MemberGroup> = [];
  public accessForm: FormGroup;
  public searchCrit = [
    {
      viewValue: 'Identifier (VAT/TIN_ID or Group ID)',
      value: 'entity_id',
    },
    {
      viewValue: 'Member/Group Name',
      value: 'name',
    },
  ];
  public searchOption: keyof MemberGroup = 'entity_id';
  public accessRights = ['Read', 'Write', 'Control'];
  public accessOption = this.accessRights[0];
  public userData: { company_id: string; name: string; resource_id: string; };

  constructor(private $dashAccountService: DashAccountService,
              private $dashUtilsSvc: DashUtilsService,
              private $formBuilder: FormBuilder) {
    this.accessForm = this.$formBuilder.group({
      groupCtrl: ['', Validators.nullValidator],
    });

    this.getAccountInfo();
    this.getAllUsersAndGroups();
  }

  ngOnInit() {
  }

  removeAccess(idx: number) {
    const toBeRemoved = this.item.at(idx);
    if (!this.userData) {
      return;
    }
    if (toBeRemoved.value.entityId === this.userData.resource_id) {
      return;
    }
    this.item.removeAt(idx);
    this.triggerPipeUpdate = !this.triggerPipeUpdate;
  }

  /*access policy component */
  getAccountInfo() {
    this.$dashAccountService.get((err, res: { status: number, data: { company_id: string, name: string; resource_id: string; } }) => {
      if (err) {
        this.$dashUtilsSvc.openSnackBar('Retrieving account data failed.. Try again later');
        return;
      }
      this.userData = res.data;
    });
  }

  addNewPolicyMemberComp() {
    const groupValue = this.accessForm.get('groupCtrl').value;
    const foundMemberGroup = this.memberGroupOptions.find(mG => mG[this.searchOption] === groupValue);
    if (!foundMemberGroup) {
      return;
    }

    if (foundMemberGroup.entity_id === this.userData.company_id) {
      this.$dashUtilsSvc.openSnackBar('You cannot modify your own rights');
      return;
    }

    this.item.push(this.$formBuilder.control({
      entityId: foundMemberGroup.resource_id,
      entityName: foundMemberGroup.name,
      accessLevel: this.accessOption,
      status: 'Approved',
    }));
    // scoate-l din lista de sugestii ca e deja adaugat

    this.accessForm.get('groupCtrl').reset();
    this.triggerPipeUpdate = !this.triggerPipeUpdate;
  }

  getAllUsersAndGroups() {
    this.$dashAccountService.getAll((err, res) => {
      if (err) {
        this.$dashUtilsSvc.openSnackBar('There was an error retrieving available users.. Try again later');
        return;
      }
      this.memberGroupOptions = res.data.map((el) => {
        return {entity_id: el.company_id, name: el.name, resource_id: el.resource_id};
      });
      this.$dashAccountService.getGroups((err2, res2) => {
        if (err2) {
          this.$dashUtilsSvc.openSnackBar('Retrieving groups for user failed.. Try again later');
          return;
        }
        console.log(res2.data);
        this.memberGroupOptions = this.memberGroupOptions.concat(res2.data.map((el) => {
          return {entity_id: el.group_id, name: el.group_name, resource_id: el.resource_id};
        }));
      });

    });
  }

}
