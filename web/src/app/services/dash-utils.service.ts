import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import * as _ from 'lodash';
import {FormBuilder, Validators} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class DashUtilsService {

  durationInSeconds = 5;

  constructor(private snackBar: MatSnackBar) {
  }

  public static getEmptyStringRequiredControl(formBuilderCtrl: FormBuilder['control']) {
    return formBuilderCtrl('', Validators.required);
  }

  public static filter = (opt: string[], value: string): string[] => {
    const filterValue = value.toLowerCase();

    return opt.filter(item => item.toLowerCase().startsWith(filterValue));
  };

  openSnackBar(message: string) {
    this.snackBar.open(
      message,
      null, {
        duration: this.durationInSeconds * 1000,
        horizontalPosition: 'right',
      });
  }

  readFileAsBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      if (file instanceof File) {
        reader.readAsDataURL(file);
        reader.onload = () => {
          console.log(reader.result);
          resolve(_.get(_.split(reader.result, ','), 1));
        };
        reader.onerror = () => {
          reject(reader.error);
        };
      }
    });
  }
}
