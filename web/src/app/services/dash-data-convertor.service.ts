import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DashDataConvertorService {

  constructor() {
  }

  modifyKey(key) {
    const idxofhashtag = key.lastIndexOf('#');
    let myNewKey = key;
    if (idxofhashtag === -1) {
      const idxofslash = key.lastIndexOf('/');
      if (idxofslash !== -1) {
        myNewKey = key.slice(idxofslash + 1);
      }
    } else {
      myNewKey = key.slice(idxofhashtag + 1);
    }
    return myNewKey;
  }

  keyMapping(key, dataType) {
    const mapping = {
      catalog: {
        hasVersion: 'version',
        metadataIssued: 'created',
        metadataModified: 'modified',
        dataset: 'datasets',
        themeTaxonomy: 'categories',
      },
      dataset: {
        hasVersion: 'version',
        metadataIssued: 'created',
        metadataModified: 'modified',
        keyword: 'keywords',
        theme: 'categories',
        isPartOf: 'catalog',
        distribution: 'distributions'
      },
      acl: {
        agent: 'entityId',
        entityName: 'entityName',
        mode: 'accessLevel',
      },
      notification: {
        mode: 'accessLevel',
        agent: 'accessEntityId',
        distribution_id: 'resourceId',
        agent_type: 'accessType',
        resource_type: 'resourceType',
        entity_name: 'accessEntity',
        access_type: 'accessType'
      },
      distribution: {
        hasVersion: 'version',
        isPartOf: 'dataset',
        metadataIssued: 'created',
        metadataModified: 'modified',
        downloadURL: 'url'
      }
    };
    if (!mapping[dataType].hasOwnProperty(key)) {
      return key;
    }
    return mapping[dataType][key];
  }

  covertMyData(swarmResponse, dataType) {
    const newCats = [];
    swarmResponse.forEach((swarmRes) => {
      const frontendCat = {};
      for (const key of Object.keys(swarmRes)) {
        const myNewKey = this.keyMapping(this.modifyKey(key), dataType);
        frontendCat[myNewKey] = swarmRes[key];
        switch (myNewKey) {
          case 'accessRights':
            frontendCat[myNewKey] = [];
            swarmRes[key].forEach((auth) => {
              const authObj = {};
              for (const subkey of Object.keys(auth)) {
                const myNewSubkey = this.keyMapping(this.modifyKey(subkey), 'acl');
                if (myNewSubkey === 'accessLevel') {
                  const idx = auth[subkey].lastIndexOf('#');
                  authObj[myNewSubkey] = auth[subkey].slice(idx + 1);
                } else {
                  authObj[myNewSubkey] = auth[subkey];
                }
              }
              frontendCat[myNewKey].push(authObj);
            });
            break;
          case 'language':
            // http://id.loc.gov/vocabulary/iso639-1/en
            frontendCat[myNewKey] = swarmRes[key].split('/').slice(-1)[0];
            break;
          case 'distributions':
            if(!Array.isArray(swarmRes['http://www.w3.org/ns/dcat#distribution'])){
              frontendCat[myNewKey] = this.covertMyData(swarmRes['http://www.w3.org/ns/dcat#distribution'], 'distribution');
            }
        }
      }
      newCats.push(frontendCat);
    });
    console.log(newCats);
    return newCats;
  }

}
