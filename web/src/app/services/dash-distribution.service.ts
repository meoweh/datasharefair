import { Injectable } from '@angular/core';
import axios from 'axios';
import { CookieService } from 'ngx-cookie-service';
import { zip } from 'rxjs';
import { debug } from "util";
import { Distribution } from '../../assets/mocked-data';
import { environment } from '../../environments/environment';
import { DistributionEdit } from '../management/dash-distribution-edit.component';
import { DashSwarmConnectorService } from './dash-swarm-connector.service';

interface DistributionEditAPI extends DistributionEdit {
  distribution_id: string;
  dataset: string;
}

@Injectable({
  providedIn: 'root',
})
export class DashDistributionService {

  constructor(private $cookieService: CookieService, private $swarmService: DashSwarmConnectorService) {
  }

  requestAccess(distribution, requestAccessParams) {
    return new Promise((resolve, reject) => {
      const ris = this.$swarmService.get();
      const req = {
        user_token: this.$cookieService.get('user_token'),
        distribution,
        accessRights: [requestAccessParams],
      };
      ris.startSwarm('distribution', 'requestAccess', req).onReturn((err, res) => {
        if (err) {
          return reject(err);
        }
        console.log(res);
        return resolve({status: 0});
      });
    });
  }

  download(req) {
    return new Promise((resolve, reject) => {
      const ris = this.$swarmService.get();
      req['user_token'] = this.$cookieService.get('user_token');
      ris.startSwarm('distribution', 'getDecryptedDistribution', req).onReturn((err, res) => {
        debugger
        if (err) {
          return reject(err);
        }
        return resolve(res.file);
      });
    });
  }

  deleteDistribution(req: Distribution) {
    return new Promise((resolve, reject) => {
      const ris = this.$swarmService.get();
      req['user_token'] = this.$cookieService.get('user_token');
      ris.startSwarm('distribution', 'deleteDist', req).onReturn((err, res) => {
        if (err) {
          return reject(err);
        }
        const idx = req.url.lastIndexOf('/');
        const zipName = req.url.slice(idx+1);
        debugger
        axios.delete(`http://${environment.hostIp}:8087/uploader` + `/${zipName}`)
          .then((response) => {
            debugger
            return resolve({status: 0});
          });
      });
    });
  }

  editDistribution(distributionData: DistributionEditAPI) {
    return new Promise((resolve, reject) => {
      distributionData['user_token'] = this.$cookieService.get('user_token');
      const ris = this.$swarmService.get();
      ris.startSwarm('distribution', 'updateDist', distributionData).onReturn((err, res) => {
        if (err) {
          reject(err);
        }
        console.log(res);
        return resolve({status: 0});
      });
    });
  }

  createDistribution(req) {
    return new Promise((resolve, reject) => {
      const ris = this.$swarmService.get();
      req.user_token = this.$cookieService.get('user_token');
      ris.startSwarm('distribution', 'createDistribution', req).onReturn((err, res) => {
        if (err) {
          return reject(err);
        }
        console.log(res);
        return resolve(res);
      });
    });
  }

  saveEncryptionKey(req){
    return new Promise((resolve, reject) => {
      const ris = this.$swarmService.get();
      req.user_token = this.$cookieService.get('user_token');
      ris.startSwarm('distribution', 'saveEncryptionKeySwarm', req).onReturn((err, res) => {
        debugger
        if (err) {
          return reject(err);
        }
        console.log(res);
        return resolve({status: 0});
      });
    });
  }
}
