import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {DashSwarmConnectorService} from './dash-swarm-connector.service';
import {DatasetWithDistributionsUserAccess, MOCKED_DATASETS_WITH_DISTRIBUTION_USER_ACCESS} from '../../assets/mocked-data';

@Injectable({
  providedIn: 'root'
})
export class DashHomeService {

  constructor(private $cookieService: CookieService, private $swarmService: DashSwarmConnectorService) {
  }

  retrieveStats(cb) {
    const req = {
      user_token: this.$cookieService.get('user_token')
    };

// ris.startSwarm('dataset', 'getStats', req).onReturn( (err, res) => {
    //   if (err) {
    //     return cb(err);
    //   }
    //   console.log(`Retrieved Stats"`);
    //   return cb(null, res);
    // });

    cb(null, {
      categoryData: {
        labels: ['Law', 'Engineering', 'Biology', 'Medical', 'Tourism'],
        data: [90, 70, 60, 50, 40, 30]
      },
      licenseData: {
        labels: ['OpenGPL', 'Apache', 'C-by-nc-nd3'],
        data: [10, 20, 30]
      }
    });
  }

  renderPopular(cb: (err: any, res: { status: number; datasets: DatasetWithDistributionsUserAccess[]; }) => void) {
    const req = {
      user_token: this.$cookieService.get('user_token')
    };

    // ris.startSwarm('dataset', 'getPopular', req).onReturn( (err, res) => {
    //   if (err) {
    //     return cb(err);
    //   }
    //   console.log(`Retrieved Popular Items"`);
    //   return cb(null, res);
    // });

    cb(null, {
      status: 0,
      datasets: MOCKED_DATASETS_WITH_DISTRIBUTION_USER_ACCESS
    });
  }
}
