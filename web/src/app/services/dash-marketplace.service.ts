import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import { DashDataConvertorService } from './dash-data-convertor.service';
import {DashSwarmConnectorService} from './dash-swarm-connector.service';
import {DatasetWithDistributionsUserAccess, MOCKED_DATASETS_WITH_DISTRIBUTION_USER_ACCESS} from '../../assets/mocked-data';

@Injectable({
  providedIn: 'root'
})
export class DashMarketplaceService {
  private readonly singleItem: Array<DatasetWithDistributionsUserAccess>;
  private readonly multipleItems: Array<DatasetWithDistributionsUserAccess>;

  constructor(private $cookieService: CookieService, private $swarmService: DashSwarmConnectorService, private $convertorService: DashDataConvertorService) {
    this.singleItem = [
      MOCKED_DATASETS_WITH_DISTRIBUTION_USER_ACCESS[0]
    ];
    this.multipleItems = MOCKED_DATASETS_WITH_DISTRIBUTION_USER_ACCESS;
  }

  getMarketplaceData(cb) {
    const req = {
      user_token: this.$cookieService.get('user_token')
    };
    const ris = this.$swarmService.get();
    ris.startSwarm('dataset', 'getAllDatasets', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      res.user_token = this.$cookieService.get('user_token');
      ris.startSwarm('distribution', 'getDistForDatasets', res).onReturn( (err2, res2) => {
        if (err2) {
          return cb(err2);
        }
        console.log(`Retrieved all marketplace`);
        const frontendData = this.$convertorService.covertMyData(res2.datasets, 'dataset');
        return cb(null, {
          status: 0,
          datasets: frontendData
        });
      });
    });
  }


  getFilteredMarketplaceData(searches: {name: string}[], catalogs: { checked: boolean; value: string; show: string; }[], categories: { checked: boolean; value: string; show: string; }[], marketplaceItems: DatasetWithDistributionsUserAccess[], cb: { (err: any, res: any): void; (err: any, res: any): void; (arg0: any, arg1: { status: number; datasets: any[]; }): void; }) {

    let filteredItems = [...marketplaceItems];
    const filteredCatalogs = catalogs.filter(v => v.checked);
    const filteredCategories = categories.filter(v => v.checked);

    if (filteredCatalogs.length) {
      const catalogValues = filteredCatalogs.map(cat => cat.value);
      filteredItems = filteredItems.filter(item => catalogValues.includes(item.catalog));
    }
    if(filteredCategories.length){
      const categoriesValues = filteredCategories.map(cat => cat.value);
      filteredItems = filteredItems.filter(item => item.categories.some(cat => categoriesValues.includes(cat)));
    }

    if(searches.length){
      filteredItems = filteredItems.filter(item => searches.some(search => item.title.toLowerCase().includes(search.name.toLowerCase()) || item.description.toLowerCase().includes(search.name.toLowerCase()) || item.keywords.includes(search.name.toLowerCase())));
    }

    return cb(null, {
      status: 0,
      datasets: filteredItems
    });

  }

  getCatalogs(cb) {
    const req = {
      user_token: this.$cookieService.get('user_token')
    };
    const ris = this.$swarmService.get();
    ris.startSwarm('catalog', 'getAllCatalogsNameId', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      console.log(`Retrieved all catalogs`);
      return cb(null, res.catalogs);
    });
  }

  getCategories(cb) {
    const req = {
      user_token: this.$cookieService.get('user_token')
    };

    const ris = this.$swarmService.get();
    ris.startSwarm('dataset', 'getCategoriesForDats', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }

      console.log(`Retrieved all dataset categories ${JSON.stringify(res)}`);
      return cb(null, res);
    });
  }

}
