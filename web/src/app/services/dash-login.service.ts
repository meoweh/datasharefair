import {Injectable} from '@angular/core';
import {DashSwarmConnectorService} from './dash-swarm-connector.service';

@Injectable({
  providedIn: 'root'
})
export class DashLoginService {

  constructor(private $swarmService: DashSwarmConnectorService) {
  }

  login(req, cb) {
    const ris = this.$swarmService.get();
    ris.startSwarm('session', 'login', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      console.log(`User token "${res.user_token}"`);
      return cb(null, {status: 0, user_token: res.user_token});
    });
  }

  logout(req, cb) {
    const ris = this.$swarmService.get();
    ris.startSwarm('session', 'logout', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      console.log(`Logged out user`);
      return cb(null, {status: 0});
    });

  }
}
