import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {DashSwarmConnectorService} from './dash-swarm-connector.service';

@Injectable({
  providedIn: 'root'
})
export class DashAccountService {

  constructor(private $cookieService: CookieService, private $swarmService: DashSwarmConnectorService) {
  }

  // user
  create(req, cb) {
    const ris = this.$swarmService.get();
    ris.startSwarm('account', 'create', req).onReturn((err, res) => {
      if (err) {
        return cb(err);
      }
      console.log(res);
      return cb(null, {status: 0});
    });
  }

  delete(cb) {

    const req = {
      user_token: this.$cookieService.get('user_token')
    };
    const ris = this.$swarmService.get();
    ris.startSwarm('account', 'delete', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      console.log(`Deleted account`);
      return cb(null, {status: 0});
    });
  }

  update(req, cb) {
    req.user_token = this.$cookieService.get('user_token');
    const ris = this.$swarmService.get();
    ris.startSwarm('account', 'update', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      console.log(`Updated account`);
      return cb(null, {status: 0});
    });
  }

  get(cb) {
    const req = {
      user_token :  this.$cookieService.get('user_token')
    };
    const ris = this.$swarmService.get();
    ris.startSwarm('account', 'get', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      console.log(`Get account info success`);
      return cb(null, {status: 0, data: res.data});
    });
  }

  getAccessRightsNotificationsForUser(cb) {
    const req = {
      user_token: this.$cookieService.get('user_token')
    };
    const ris = this.$swarmService.get();
    ris.startSwarm('distribution', 'retrieveAccessToApproveForUser', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      return cb(null, {status: 0, data: res});
    });
  }

  getAll(cb) {
    const req = {
      user_token :  this.$cookieService.get('user_token')
    };
    const ris = this.$swarmService.get();
    ris.startSwarm('account', 'getAll', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      console.log(res.data);
      return cb(null, {status: 0, data: res.data});
    });
  }

  // groups
  getGroups(cb) {
    const req = {
      user_token :  this.$cookieService.get('user_token')
    };
    const ris = this.$swarmService.get();
    ris.startSwarm('group', 'getAll', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      console.log(`Retrieve groups success ${JSON.stringify(res.data)}`);
      return cb(null, {status: 0, data: res.data});
    });
  }

  editAccessRightsForUser(req, cb) {
    req.user_token = this.$cookieService.get('user_token');
    const ris = this.$swarmService.get();
    ris.startSwarm('distribution', 'approveAccess', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      return cb(null, {status: 0});
    });
  }

  addGroupMember(req, cb) {
    req.user_token = this.$cookieService.get('user_token');
    const ris = this.$swarmService.get();
    ris.startSwarm('group', 'addGroupMember', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      console.log(`Update group success`);
      return cb(null, {status: 0});
    });
  }

  deleteGroupMember(req, cb) {
    req.user_token = this.$cookieService.get('user_token');
    const ris = this.$swarmService.get();
    ris.startSwarm('group', 'removeGroupMember', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      console.log(`Update group success`);
      return cb(null, {status: 0});
    });
  }

  createGroup(req, cb) {
    req.user_token = this.$cookieService.get('user_token');
    const ris = this.$swarmService.get();
    ris.startSwarm('group', 'create', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      return cb(null, {status: 0});
    });
  }

  deleteGroup(req, cb) {
    req.user_token = this.$cookieService.get('user_token');
    const ris = this.$swarmService.get();
    ris.startSwarm('group', 'delete', req).onReturn( (err, res) => {
      if (err) {
        return cb(err);
      }
      console.log(`Deleted group successfully`);
      return cb(null, {status: 0});
    });
  }
}
