import { flatten } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { uniqBy } from 'lodash-es';
import { CookieService } from 'ngx-cookie-service';
import { Catalog, Dataset, Distribution } from '../../assets/mocked-data';
import { DashDataConvertorService } from './dash-data-convertor.service';
import { DashSwarmConnectorService } from './dash-swarm-connector.service';

interface DBPediaResult {
  uri: string;
  label: string;
  description: string;
  refCount: number;
  classes: Array<any>;
  categories: Array<{
    uri: string;
    label: string;
  }>;
  templates: Array<any>;
  redirects: Array<any>;
}

interface WikiDataResult {
  item: {
    type: 'uri',
    value: string
  };
  itemLabel: {
    'xml:lang': 'en',
    type: 'literal',
    value: string;
  };
  itemDescription: {
    'xml:lang': 'en',
    type: 'literal',
    value: string;
  };
}

export interface SearchThemeResult {
  uri: string;
  label: string;
}

@Injectable({
  providedIn: 'root',
})
export class DashDatasetService {

  constructor(private $cookieService: CookieService, private $swarmService: DashSwarmConnectorService, private $convertorService: DashDataConvertorService) {
  }

  createDataset(req) {
    return new Promise((resolve, reject) => {
      req.user_token = this.$cookieService.get('user_token');
      const ris = this.$swarmService.get();
      ris.startSwarm('dataset', 'createDataset', req).onReturn((err, res) => {
        if (err) {
          return reject(err);
        }
        console.log(`Retrieved all dataset categories ${JSON.stringify(res)}`);
        return resolve(res);
      });
    });
  }

  createCatalog(req) {
    return new Promise((resolve, reject) => {
      req.user_token = this.$cookieService.get('user_token');
      const ris = this.$swarmService.get();
      ris.startSwarm('catalog', 'createCat', req).onReturn((err, res) => {
        if (err) {
          reject(err);
        }
        console.log(`Created catalog success`);
        return resolve({status: 0});
      });
    });
  }

  getCatalogsForUser(): Promise<Array<Catalog>> {
    return new Promise((resolve, reject) => {
      const req = {user_token: this.$cookieService.get('user_token')};
      const ris = this.$swarmService.get();
      ris.startSwarm('catalog', 'getCatsForUser', req).onReturn((err, res) => {
        if (err) {
          reject(err);
        }
        console.log(`Get catalog success ${JSON.stringify(res)}`);
        const frontendCatalogs = this.$convertorService.covertMyData(res.catalogs, 'catalog');
        console.log(`Frontend catalog ${JSON.stringify(frontendCatalogs)}`);
        return resolve(frontendCatalogs);
      });
    });
  }

  getDatasetsForUser(): Promise<Array<Dataset>> {
    return new Promise((resolve, reject) => {
      const req = {
        user_token: this.$cookieService.get('user_token'),
      };
      const ris = this.$swarmService.get();
      ris.startSwarm('dataset', 'getDatsForUser', req).onReturn((err, res) => {
        if (err) {
          reject(err);
        }
        console.log(`Get dataset success ${JSON.stringify(res)}`);
        const frontendDatasets = this.$convertorService.covertMyData(res.datasets, 'dataset');
        console.log(`Frontend datasets ${JSON.stringify(frontendDatasets)}`);
        return resolve(frontendDatasets);
      });
    });
  }

  getDatasetsForCatalog(catalogId: string): Promise<Array<Dataset>> {
    return new Promise((resolve, reject) => {
      this.getDatasetsForUser().then((res) => {
        return resolve(res.filter(d => d.catalog === catalogId));
      }).catch((err) => {
        reject(err);
      });
    });
  }

  getDistributionForDataset(dataset_id: string): Promise<Array<Distribution>> {
    return new Promise((resolve, reject) => {
      const ris = this.$swarmService.get();
      const req = {
        user_token: this.$cookieService.get('user_token'),
        datasets: [{dataset_id}],
      };
      ris.startSwarm('distribution', 'getDistForDatasets', req).onReturn((err2, res2) => {
        if (err2) {
          return reject(err2);
        }
        console.log(`Retrieved all marketplace`);
        let dist = res2.datasets.map(i => i['http://www.w3.org/ns/dcat#distribution']);
        dist = dist.flat();
        const frontendData = this.$convertorService.covertMyData(dist, 'distribution');
        resolve( frontendData);
      });
    });
  }

  editCatalog(catalogData: Catalog) {
    return new Promise((resolve, reject) => {
      catalogData['user_token'] = this.$cookieService.get('user_token');
      const ris = this.$swarmService.get();
      ris.startSwarm('catalog', 'updateCat', catalogData).onReturn((err, res) => {
        if (err) {
          reject(err);
        }
        console.log(`Updated catalog success`);
        return resolve({status: 0});
      });
    });
  }

  editDataset(data: Dataset) {
    return new Promise((resolve, reject) => {
      data['user_token'] = this.$cookieService.get('user_token');
      const ris = this.$swarmService.get();
      ris.startSwarm('dataset', 'updateDat', data).onReturn((err, res) => {
        if (err) {
          reject(err);
        }
        console.log(`Updated dataset success`);
        return resolve({status: 0});
      });
    });
  }

  searchTheme(keyword: string): Promise<Array<SearchThemeResult>> {
    return new Promise((resolve, reject) => {
      const req = {
        category: keyword,
        user_token: this.$cookieService.get('user_token'),
      };
      const ris = this.$swarmService.get();
      ris.startSwarm('getFairResources', 'getSemanticCategories', req).onReturn((err, res: Array<DBPediaResult> | Array<WikiDataResult>) => {
        if (err) {
          reject(err);
        }

        console.log(res);

        if (!res.length) {
          return resolve([]);
        }

        if (this.isDBPediaArray(res)) {
          return resolve(
            uniqBy(
              flatten(
                res.map(r => r.categories.map(
                  rC => ({
                    uri: rC.uri,
                    label: rC.label,
                  }),
                  ),
                ),
              ), 'uri'),
          );
        }

        return resolve(res.map(r => ({
          uri: r.item.value,
          label: r.itemLabel.value,
        })));

      });
    });
  }

  private isDBPediaResult(r: DBPediaResult | WikiDataResult): r is DBPediaResult {
    return 'uri' in r ? !!r.uri : false;
  }

  private isDBPediaArray(res: Array<DBPediaResult> | Array<WikiDataResult>): res is Array<DBPediaResult> {
    return this.isDBPediaResult(res[0]);
  }
}
