import {Injectable} from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashSwarmConnectorService {

  private swarmConnection: any;

  constructor() {
    this.init();
  }

  init() {
    // @ts-ignore
    const interact = pskclientRequire('interact');
    interact.enableRemoteInteractions();
    this.swarmConnection = interact.createRemoteInteractionSpace('testRemote', `http://${environment.hostIp}:8080`, 'local/agent/example');
  }

  get() {
    if (!this.swarmConnection) {
      this.init();
    }
    return this.swarmConnection;
  }
}
