import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog, MatExpansionPanel } from '@angular/material';
import {
  DatasetWithDistributionsUserAccess,
  GroupDescription,
  MemberDescription,
  NewAccessRequestCreation,
  Distribution
} from '../../assets/mocked-data';
import { DashAccountService } from '../services/dash-account.service';
import { DashDistributionService } from '../services/dash-distribution.service';
import { DashUtilsService } from '../services/dash-utils.service';
import { DashAccessRequestDialogComponent } from './dash-access-request-dialog.component';



@Component({
  selector: 'dash-dataset-item',
  templateUrl: './dash-dataset-item.component.html',
  styleUrls: ['./dash-dataset-item.component.scss'],
})
export class DashDatasetItemComponent implements OnInit {

  @Input() item: DatasetWithDistributionsUserAccess;
  @Input() expanded = false;
  @Output() collapsedItem = new EventEmitter<void>();
  public filteredProperties = ['distributions', 'description', 'accessRights'];

  @ViewChild(MatExpansionPanel, {static: false}) panel: MatExpansionPanel;
  private user: Array<MemberDescription>;
  private groups: Array<GroupDescription>;

  constructor(private DistributionService: DashDistributionService, private UtilsService: DashUtilsService, private $dialog: MatDialog, private AccountService: DashAccountService) {
    this.AccountService.get((err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('Retrieving user data failed');
        return;
      }
      this.user = [res.data];
    });

    this.AccountService.getGroups((err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('Retrieving groups data failed');
        return;
      }
      this.groups = res.data;
    });
  }

  ngOnInit() {
  }


  collapsePanel() {
    this.panel.close();
  }
}
