import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { GroupDescription, MemberDescription } from '../../assets/mocked-data';
import { DashUtilsService } from '../services/dash-utils.service';



@Component({
  selector: 'dash-access-request-dialog',
  templateUrl: './dash-access-request-dialog.component.html',
  styleUrls: ['./dash-access-request-dialog.component.scss']
})
export class DashAccessRequestDialogComponent {

  public requiredErrorMessage = 'This field is required!';

  public accessFG: FormGroup;

  constructor(
    private $formBuilder: FormBuilder,
    private $dialogRef: MatDialogRef<DashAccessRequestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public userGroupsData: {'groups': Array<GroupDescription>, 'user': Array<MemberDescription>}
  ) {
    this.accessFG = this.$formBuilder.group({
      accessType: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
      entityId: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
      accessByField: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control),
      accessLevel: DashUtilsService.getEmptyStringRequiredControl(this.$formBuilder.control)
    });

  }

  onNoClick(): void {
    this.$dialogRef.close();
  }

}
