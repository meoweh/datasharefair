import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import axios from 'axios';
import * as _ from 'lodash';
import { Distribution, GroupDescription, MemberDescription, NewAccessRequestCreation } from '../../assets/mocked-data';
import { DashAccountService } from '../services/dash-account.service';
import { DashDataConvertorService } from '../services/dash-data-convertor.service';
import { DashDistributionService } from '../services/dash-distribution.service';
import { DashUtilsService } from '../services/dash-utils.service';
import { DashAccessRequestDialogComponent } from './dash-access-request-dialog.component';

@Component({
  selector: 'dash-distribution-tab',
  templateUrl: './dash-distribution-tab.component.html',
  styleUrls: ['./dash-distribution-tab.component.scss'],
})
export class DashDistributionTabComponent implements OnInit {

  @Input() item: Array<Distribution>;
  private user: Array<MemberDescription>;
  private groups: Array<GroupDescription>;

  constructor(private UtilsService: DashUtilsService, private DistributionService: DashDistributionService, private $dialog: MatDialog, private AccountService: DashAccountService, private ConvertorService: DashDataConvertorService) {
    this.AccountService.get((err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('Retrieving user data failed');
        return;
      }
      this.user = [res.data];
    });

    this.AccountService.getGroups((err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('Retrieving groups data failed');
        return;
      }
      this.groups = res.data;
    });
  }

  ngOnInit() {
    this.item = this.ConvertorService.covertMyData(this.item, 'distribution');
  }

  checkAccess(distribution: Distribution) {
    if (!distribution.accessRights) {
      console.log('No access rights detected');
      return false;
    }
    let found = false;
    distribution.accessRights.forEach((right) => {
      if (right.entityId === this.user[0].resource_id) {
        if (right.accessLevel === 'Control' || right.accessLevel === 'Write') {
          found = true;
        }
      }
    });
    // cauta in access rights daca userul curent are drepturi
    return found;
  }

  downloadDistribution(distribution: Distribution) {
    const urlO = new URL(distribution.url);
    urlO.port = '8087'; // hack to redirect to uploader server port
    if (!distribution.url.endsWith('.zip')) {
      urlO.pathname += '.zip'; // hack to redirect to .zip
    }

    axios.get(urlO.toString(), {
      responseType: 'blob',
    })
      .then((response) => {
        const reader = new FileReader();
        reader.addEventListener('loadend', () => {
          // reader.result contains the contents of blob as a typed array
          this.DistributionService.download({
            distribution_id: distribution.distribution_id,
            file: _.get(_.split(reader.result, ','), 1),
          }).then((res: string) => {
            this.UtilsService.openSnackBar('Download initiated successfully.');
            const u8arr = this.convertDataBase64ToBinary(res);
            const blob = new Blob([u8arr.buffer], {type: 'octet/stream'});
            const urlBlob = window.URL.createObjectURL(blob);

            const element = document.createElement('a');
            element.href = urlBlob;
            element.download = distribution.distribution_id + '.zip';
            element.style.display = 'none';
            document.body.appendChild(element);
            element.click();
            document.body.removeChild(element);
          });
        });
        reader.readAsDataURL(response.data);
      });

  }

  requestDistributionAccess(distribution) {
    const dialogRef = this.$dialog.open(DashAccessRequestDialogComponent, {
      width: '500px',
      height: '500px',
      data: {
        groups: this.groups,
        user: this.user,
      },
    });

    dialogRef.afterClosed().subscribe((requestAccessParams: NewAccessRequestCreation) => {
      if (!requestAccessParams) {
        return;
      }
      console.log(requestAccessParams);

      this.DistributionService.requestAccess(distribution, requestAccessParams).then((res) => {
        this.UtilsService.openSnackBar('Request access sent. You will be able to download the distribution as soon as the owner approves the request');
      }).catch((err) => {
        this.UtilsService.openSnackBar('Requesting access to distribution failed.. Try again later.');
        return;
      });
    });
  }

  private convertDataBase64ToBinary(base64String: string) {
    const raw = window.atob(base64String);
    const rawLength = raw.length;
    const array = new Uint8Array(new ArrayBuffer(rawLength));

    for (let i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  }

}
