import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ViewChild } from '@angular/core';
import { MatChipInputEvent } from '@angular/material';
import { DatasetWithDistributionsUserAccess } from '../../assets/mocked-data';
import { DashMarketplaceService } from '../services/dash-marketplace.service';
import { DashUtilsService } from '../services/dash-utils.service';
import { DashDatasetItemComponent } from './dash-dataset-item.component';

@Component({
  selector: 'dash-marketplace',
  templateUrl: './dash-marketplace.component.html',
  styleUrls: ['./dash-marketplace.component.scss'],
})
export class DashMarketplaceComponent {

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  addOnBlur = true;
  selectable = true;
  removable = true;
  marketPlaceItems: Array<DatasetWithDistributionsUserAccess>;
  @ViewChild(DashDatasetItemComponent, {static: false}) datasetItemComp: DashDatasetItemComponent;
  keywords = [];
  selectedItem: DatasetWithDistributionsUserAccess;
  private tempSelectedItem: DatasetWithDistributionsUserAccess;
  private maketPlaceCategories: string[];
  private marketPlaceCatalogs: Array<{catalog_id: string, title: string}>;
  private Catalogs: { checked: boolean; value: string, show: string }[];
  private Categories: { checked: boolean; value: string, show: string }[];
  private allMarketPlaceData: Array<DatasetWithDistributionsUserAccess>;

  constructor(private MarketplaceService: DashMarketplaceService, private UtilsService: DashUtilsService) {
    this.MarketplaceService.getCategories((err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('Cannot retrieve categories');
        return;
      }
      this.maketPlaceCategories = res;
      this.MarketplaceService.getCatalogs((err2, res2) => {
        if (err2) {
          this.UtilsService.openSnackBar('Cannot retrieve catalogs');
          return;
        }
        this.marketPlaceCatalogs = res2;
        this.Catalogs = this.marketPlaceCatalogs.map((item) => {
          return {
            checked: false,
            value: item.catalog_id,
            show: item.title
          };
        });
        this.Categories = this.maketPlaceCategories.map((item) => {
          return {
            checked: false,
            value: item,
            show: item.slice(item.lastIndexOf('/') + 1),
          };
        });
        this.getMarketplaceData();
      });
    });
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.keywords.push({name: value.trim()});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
    this.filterResults();
  }

  remove({chip}: { chip: any }): void {
    const index = this.keywords.indexOf(chip);

    if (index >= 0) {
      this.keywords.splice(index, 1);
    }
    this.filterResults();
  }

  getMarketplaceData() {
    this.MarketplaceService.getMarketplaceData((err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('Could not retrieve marketplace data. Try again later.');
        return;
      }
      this.allMarketPlaceData = res.datasets;
      this.marketPlaceItems = [...this.allMarketPlaceData];
    });
  }

  selectNewItem(item: DatasetWithDistributionsUserAccess) {
    if (!this.selectedItem) {
      this.selectedItem = item;
      return;
    }

    this.tempSelectedItem = item;
    this.datasetItemComp.collapsePanel();
  }

  itemCollapsed() {
    this.selectedItem = this.tempSelectedItem || null;
    this.tempSelectedItem = null;
  }

  filterResults() {
    if (!this.keywords) {
      this.UtilsService.openSnackBar('Please fill in some keywords for your search!');
      return;
    }
    this.MarketplaceService.getFilteredMarketplaceData(this.keywords, this.Catalogs, this.Categories, this.allMarketPlaceData, (err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('Failed to perform search.. Try again later');
        return;
      }
      this.marketPlaceItems = res.datasets;
    });
  }

  filterEntries() {
    this.MarketplaceService.getFilteredMarketplaceData(this.keywords, this.Catalogs, this.Categories, this.allMarketPlaceData, (err, res) => {
      if (err) {
        this.UtilsService.openSnackBar('Failed to perform search.. Try again later');
        return;
      }
      this.marketPlaceItems = res.datasets;
    });
  }
}
