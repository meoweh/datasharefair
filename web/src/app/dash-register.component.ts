import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {MatSelectChange} from '@angular/material';
import {DashUtilsService} from './services/dash-utils.service';
import {DashAccountService} from './services/dash-account.service';

@Component({
  selector: 'dash-register',
  templateUrl: './dash-register.component.html',
  styleUrls: ['./dash-register.component.scss']
})
export class DashRegisterComponent {


  signFormModel = {
    region: '',
    name: '',
    company_id: '',
    email: '',
    category: '',
    certificate: '',
    address: ''
  };
  Region: any = ['EU', 'US', 'Other'];
  // Choose city using select dropdown
  Category: any = ['Educational Institution', 'Private Business', 'Non-Profit Organization'];
  private req: { company_id: string; name: string; email: string; region: string; category: string; certificate: string; address: string };

  constructor(private $http: HttpClient, private $cookies: CookieService, private $router: Router, private UtilsService: DashUtilsService, private AccountService: DashAccountService) {
  }

  changeRegion(e) {
    console.log(this.signFormModel.region);
  }

  onKeyFileChange($event: File | FileList) {
    this.UtilsService.readFileAsBase64($event)
      .then((value: string) => {
        this.signFormModel.certificate = value;
      });
  }

  clearKeyFile() {
    this.signFormModel.certificate = '';
  }

  goSignUp() {
    if (!this.signFormModel.company_id || !this.signFormModel.name || !this.signFormModel.email || !this.signFormModel.address || !this.signFormModel.certificate || !this.signFormModel.category) {
      this.UtilsService.openSnackBar('Please fill in all the fields.');
      return;
    }

    this.req = {
      company_id: this.signFormModel.company_id,
      name: this.signFormModel.name,
      email: this.signFormModel.email,
      address: this.signFormModel.address,
      certificate: this.signFormModel.certificate,
      category: this.signFormModel.category,
      region: this.signFormModel.region
    };

    this.AccountService.create(this.req, (err, res) => {
      if (err) {
        console.error(err);
        this.UtilsService.openSnackBar('Creating user failed. Try again later.');
        return;
      }
      console.log(res);
      this.UtilsService.openSnackBar('Signed up.. Redirecting to login...');
      setTimeout(() => {
        this.$router.navigate(['/login'])
          .then();
      }, 6000);
    });
  }

  changeCategory($event: MatSelectChange) {
    console.log(this.signFormModel.category);
  }
}
